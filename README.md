# registration-e-document-manager

An online registration and electronic document management platform, that enables an organization to assess submitted documents of applicants and determine their admissions.