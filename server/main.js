import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import '../imports/startup/server';
import { UserApplication } from '../imports/api/users/collections';
import { applicantsDocumentList, membersDocumentList } from '../imports/api/users/methods';

//import createApplicants from './fixtures';

Meteor.startup(async () => {
    console.log('In meteor startup');

    //COMMENT OUT THESE 2 CALLS
    console.log('Updating members');
    //await membersDocumentList();

    console.log('Updating applicants');
    //await applicantsDocumentList();

    //setting MAIL_URL
    //process.env.MAIL_URL = "smtp://postmaster%40app.anan.org.ng:cce805a5fd7a8a2bc0dcf025723157eb@smtp.mailgun.org:587";
    //process.env.MAIL_URL = "smtp://postmaster%40sandbox7d97de394e6249b9a972a6e0130bb811.mailgun.org:3ef720ada23f8cf93840a2cedac11c61@smtp.mailgun.org:587";
    //process.env.MAIL_URL = "smtp://postmaster%40register.anan.org.ng:030fcfb6d9d0373d62d117ce44df4bb1@smtp.mailgun.org:587";
    process.env.MAIL_URL="smtp://anan%40accountant.com:Anan123456@smtp.mail.com:587/";

    if(Meteor.users.find().count() === 0){
        let options = {
            username: 'admin',
            password: Meteor.settings.private.ADMIN_PASSWORD,
            email: Meteor.settings.private.ADMIN_EMAIL,
            bio: {
                lastName: 'Admin',
                firstName: "Admin",
                role: 'Admin',
                phone: Meteor.settings.private.ADMIN_PHONE
            }
        };

        let user = Accounts.createUser(options);
        Roles.addUsersToRoles(user, 'admin');

        //MEMBERSHIP
        options = {
            username: 'membership',
            password: 'ANAN123456',
            email: 'membership@anan.org.ng',
            bio: {
                lastName: 'Membership',
                firstName: "Membership",
                role: 'Membership'
            }
        };

        user = Accounts.createUser(options);
        Roles.addUsersToRoles(user, 'Membership');

        //AUDIT
        options = {
            username: 'audit',
            password: 'ANAN123456',
            email: 'audit@anan.org.ng',
            bio: {
                lastName: 'Audit',
                firstName: "Audit",
                role: 'Audit'
            }
        };

        user = Accounts.createUser(options);
        Roles.addUsersToRoles(user, 'Audit');

        //EXAM
        options = {
            username: 'exams',
            password: 'ANAN123456',
            email: 'exams@anan.org.ng',
            bio: {
                lastName: 'Exams',
                firstName: "Exams",
                role: 'Exams'
            }
        };

        user = Accounts.createUser(options);
        Roles.addUsersToRoles(user, 'Exams');

        //REGISTRY
        options = {
            username: 'registry',
            password: 'ANAN123456',
            email: 'registry@anan.org.ng',
            bio: {
                lastName: 'Registry',
                firstName: "Registry",
                role: 'Registry'
            }
        };

        user = Accounts.createUser(options);
        Roles.addUsersToRoles(user, 'Registry');
    }

    //ADD USER DATA; REMOVE LATER
    // if(Meteor.isDevelopment) {
    //     let user = Meteor.users.findOne({ username: 'user0'});
    //     if(!user) {
    //         //if the demo users have not been created before, create them
    //         createApplicants();
    //     }
    // }

    //CREATE UNIQUE INDEXES HERE
    //--- ALSO CREATE INDEXES ON THE FIELDS USED BY PUBLICATIONS - ANANNUMBER ON BIO
    //UserApplication._ensureIndex({userId: 1}, {unique: 1});
    //UserApplication._ensureIndex({ananNumber: 1}, {unique: 1});
    //PUT AN INDEX ON bio.membershipStatus; userApplications.userId, bio.lastName, bio.firstName, bio.ananNumber
    Meteor.users._ensureIndex({
        "bio.ananNumber": -1,
        "bio.membershipStatus": 1,
        "bio.lastName": 1,
        "bio.firstName": 1
    });

    UserApplication._ensureIndex({
        userId: 1,
        ananNumber: -1
    })
});
