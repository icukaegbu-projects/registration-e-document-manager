/**
 * Created by ikedi on 02/12/2016.
 */
import { UserApplication, UserPayment, UserDocuments, UploadStatistics, ApplicantApprovals,
        ApprovalComments, AuditTrail, LoginAuditTrail, SubmissionAuditTrail } from '../users/collections';
import {check, Match} from 'meteor/check';
import { JoinServer } from 'meteor-publish-join';

Meteor.publish('users.all', function () {
    return Meteor.users.find({}, {
        fields: {
            "username": 1,
            "emails": 1,
            "bio": 1,
            "createdAt": 1
        }
    });
});

Meteor.publish('members.all', function (options) {
    // return Meteor.users.find({
    //     "bio.membershipStatus": {
    //         $in: ["CNA", "FCNA"]
    //     }
    // }, {
    //     fields: {
    //         "username": 1,
    //         "emails": 1,
    //         "bio": 1,
    //         "createdAt": 1
    //     }
    // },{
    //     sort: {
    //         createdAt: -1
    //     },
    //     limit: 10
    // });

    check(options, Match.OneOf(Object, null, undefined));
    //let query = {},
    let query = {
            "bio.membershipStatus": {
                $in: ["CNA", "FCNA"]
            },
            "bio.lastName": { $ne: "DELETED" }  //EXCLUDE ALL NAMES WHO HAVE BEEN DELETED
            // $or: [
            //     {"bio.firstName": { $ne: "DELETED" }},
            //     {"bio.lastName": { $ne: "DELETED" }},
            // ]
        },
        projection = {
            //sort by ananNumber descending
            sort: { "bio.ananNumber": -1, created_at: 1},
            limit: 10,
            fields: {
                "username": 1,
                "emails": 1,
                "bio": 1,
                "createdAt": 1
            }
        },
        lastNameRegex = "", firstNameRegex = "";

    if(!options || options.searchType === "None") {
        // query = {
        //     "bio.membershipStatus": {
        //         $in: ["CNA", "FCNA"]
        //     }
        // }
        return Meteor.users.find(query, projection);
    }else if(options && Object.keys(options).length === 1 && options.numResults) {
        projection.limit = options.numResults;
        console.log(projection);

        return Meteor.users.find(query, projection);
    }else{
        if (options.searchType === "Name") {
            if (options.lastName) {
                lastNameRegex = RegExp(options.lastName, "i");
            }
            if (options.firstName) {
                firstNameRegex = RegExp(options.firstName, "i");
            }
            query = {
                $or: [
                    {"bio.firstName": firstNameRegex},
                    {"bio.lastName": lastNameRegex},
                ],
                ...query  //spread the former query object

                // "bio.membershipStatus": {
                //     $in: ["CNA", "FCNA"]
                // }
            };

            console.log(query)

            projection.limit = parseInt(options.numResults);

            return Meteor.users.find(query, projection);
        }

        if (options.searchType === "Anan#") {
            query = {
                "bio.ananNumber": {
                    $gte: options.ananNumberStart,
                    $lte: options.ananNumberEnd
                },
                ...query  //spread the former query object

                // "bio.membershipStatus": {
                //     $in: ["CNA", "FCNA"]
                // }
            };

            console.log(query)

            projection.limit = parseInt(options.numResults);
            return Meteor.users.find(query, projection);
        }
    }
});

Meteor.publish('applicants.all', function (options) {
    //check(limit, Number);
    //return all users for the specified academic year
    //who belong to a particular role
    //return all applicants, sort by date and return by limits of 100
    // return Meteor.users.find({}, {
    //     fields: {
    //         "username": 1,
    //         "emails": 1,
    //         "bio": 1,
    //         "createdAt": 1
    //     }
    // }, {
    //     sort: {
    //         limit: limit
    //     }
    // });

    // return Meteor.users.find({
    //     "bio.membershipStatus": "APPLICANT"
    // }, {
    //     fields: {
    //         "username": 1,
    //         "emails": 1,
    //         "bio": 1,
    //         "createdAt": 1
    //     }
    // });

    check(options, Match.OneOf(Object, null, undefined));
    let query = {
            "bio.membershipStatus": "APPLICANT"
        },
        projection = {
            sort: { created_at: 1},
            limit: 20,
            fields: {
                "username": 1,
                "emails": 1,
                "bio": 1,
                "createdAt": 1
            }
        },
        lastNameRegex = "", firstNameRegex = "";

    if(!options || options.searchType === "None") {
        return Meteor.users.find(query, projection);
    }else if(options && Object.keys(options).length === 1 && options.numResults) {
        projection.limit = options.numResults;

        return Meteor.users.find(query, projection);
    }else{
        if (options.searchType === "Name") {
            if (options.lastName) {
                lastNameRegex = RegExp(options.lastName, "i");
            }
            if (options.firstName) {
                firstNameRegex = RegExp(options.firstName, "i");
            }
            query = {
                $or: [
                    {"bio.firstName": firstNameRegex},
                    {"bio.lastName": lastNameRegex},
                ],
                ...query
                //"bio.membershipStatus": "APPLICANT"
            };

            projection.limit = parseInt(options.numResults);

            return Meteor.users.find(query, projection);
        }

        if (options.searchType === "Anan#") {
            query = {
                "bio.ananNumber": {
                    $gte: options.ananNumberStart,
                    $lte: options.ananNumberEnd
                },
                ...query
                //"bio.membershipStatus": "APPLICANT"
            };

            projection.limit = parseInt(options.numResults);
            return Meteor.users.find(query, projection);
        }
    }
});

Meteor.publish('users.one', function (id) {
    check(id, Match.OneOf(String, null, undefined));
    if(!id) return;

    return Meteor.users.find({_id: id}, {
        fields: {
            "username": 1,
            "emails": 1,
            "bio": 1,
            "roles": 1
        }
    });
});

//refine this method
Meteor.publish('userApplication.all', function (ids) {
    check(ids, Array);

    return UserApplication.find({
        userId: { $in: ids }
    }, {
        fields: {
            "nextOfKinName": 0,
            "nextOfKinPhone": 0,
            "nextOfKinRelationship": 0,
            "nextOfKinAddress": 0,

            "tertiaryInstitutionAttended": 0,
            "qualificationsWithDates": 0,

            "ananProfessionalExamDate": 0,
            "ananTrainingStartDate": 0,
            "ananTrainingEndDate": 0,

            "presentEmployer": 0,
            "employerAddress": 0,
            "positionHeld": 0,
            "appointmentDate": 0,
            "supportingParticulars": 0,

            "financialMisconduct": 0,
            "financialMisconductDetails": 0,
            "courtConviction": 0,
            "courtConvictionDetails": 0,
            "bankruptcy": 0,
            "bankruptcyDetails": 0,

            "previousAnanApplication": 0,
            "previousAnanApplicationDetails": 0,
        }
    });
});

Meteor.publish('userApplication.one', function (id) {
    check(id, Match.OneOf(String, null, undefined));
    if(!id) return;

    return UserApplication.find({userId: id});
});

Meteor.publish('userPayment.one', function (id) {
    check(id, Match.OneOf(String, null, undefined));
    if(!id) return;

    return UserPayment.find({ custReference: id });
});

Meteor.publish('uploadStatistics.user', function (id) {
    check(id, Match.OneOf(String, null, undefined));
    if(!id) return;

    return UploadStatistics.find({staffId: id}, { sort: { dateUploaded: -1}});
});

Meteor.publish('applicantApproval.user', function (id) {
    check(id, Match.OneOf(String, null, undefined));
    if(!id) return;

    return ApplicantApprovals.find({applicantId: id});
});

Meteor.publish('approval.comments', function (id) {
    check(id, Match.OneOf(String, null, undefined));
    if(!id) return;

    return ApprovalComments.find({applicantId: id});
});

Meteor.publish('audit.trail', function (options) {
    //POSSIBLE SEARCH PARAMETERS: STAFFID, ACTIONTYPE, ACTIONSTATUS, DATEPERFORMED
    check(options, Match.OneOf(Object, null, undefined));
    let query = {},
        projection = {
            sort: { datePerformed: -1},
            limit: 50,
        };

    return AuditTrail.find(query, projection);

    // if(!options || options.searchType === "None") {
    //     return Meteor.users.find(query, projection);
    // }else if(options && Object.keys(options).length === 1 && options.numResults) {
    //     projection.limit = options.numResults;
    //
    //     return Meteor.users.find(query, projection);
    // }else{
    //     if (options.searchType === "Name") {
    //         if (options.lastName) {
    //             lastNameRegex = RegExp(options.lastName, "i");
    //         }
    //         if (options.firstName) {
    //             firstNameRegex = RegExp(options.firstName, "i");
    //         }
    //         query = {
    //             $or: [
    //                 {"bio.firstName": firstNameRegex},
    //                 {"bio.lastName": lastNameRegex},
    //             ],
    //             ...query
    //             //"bio.membershipStatus": "APPLICANT"
    //         };
    //
    //         projection.limit = parseInt(options.numResults);
    //
    //         return Meteor.users.find(query, projection);
    //     }
    //
    //     if (options.searchType === "Anan#") {
    //         query = {
    //             "bio.ananNumber": {
    //                 $gte: options.ananNumberStart,
    //                 $lte: options.ananNumberEnd
    //             },
    //             ...query
    //             //"bio.membershipStatus": "APPLICANT"
    //         };
    //
    //         projection.limit = parseInt(options.numResults);
    //         return Meteor.users.find(query, projection);
    //     }
    // }
});

Meteor.publish('login.audit.trail', function (options) {
    //POSSIBLE SEARCH PARAMETERS: STAFFID, ACTIONTYPE, ACTIONSTATUS, DATEPERFORMED
    check(options, Match.OneOf(Object, null, undefined));
    let query = {},
        projection = {
            sort: { datePerformed: -1 },
            limit: 50,
        };

    return LoginAuditTrail.find(query, projection);

    // if(!options || options.searchType === "None") {
    //     return Meteor.users.find(query, projection);
    // }else if(options && Object.keys(options).length === 1 && options.numResults) {
    //     projection.limit = options.numResults;
    //
    //     return Meteor.users.find(query, projection);
    // }else{
    //     if (options.searchType === "Name") {
    //         if (options.lastName) {
    //             lastNameRegex = RegExp(options.lastName, "i");
    //         }
    //         if (options.firstName) {
    //             firstNameRegex = RegExp(options.firstName, "i");
    //         }
    //         query = {
    //             $or: [
    //                 {"bio.firstName": firstNameRegex},
    //                 {"bio.lastName": lastNameRegex},
    //             ],
    //             ...query
    //             //"bio.membershipStatus": "APPLICANT"
    //         };
    //
    //         projection.limit = parseInt(options.numResults);
    //
    //         return Meteor.users.find(query, projection);
    //     }
    //
    //     if (options.searchType === "Anan#") {
    //         query = {
    //             "bio.ananNumber": {
    //                 $gte: options.ananNumberStart,
    //                 $lte: options.ananNumberEnd
    //             },
    //             ...query
    //             //"bio.membershipStatus": "APPLICANT"
    //         };
    //
    //         projection.limit = parseInt(options.numResults);
    //         return Meteor.users.find(query, projection);
    //     }
    // }
});

Meteor.publish('submission.audit.trail', function (options) {
    //POSSIBLE SEARCH PARAMETERS: STAFFID, ACTIONTYPE, ACTIONSTATUS, DATEPERFORMED
    check(options, Match.OneOf(Object, null, undefined));
    let query = {},
        projection = {
            sort: { datePerformed: -1 },
            limit: 50,
        };

    return SubmissionAuditTrail.find(query, projection);

    // if(!options || options.searchType === "None") {
    //     return Meteor.users.find(query, projection);
    // }else if(options && Object.keys(options).length === 1 && options.numResults) {
    //     projection.limit = options.numResults;
    //
    //     return Meteor.users.find(query, projection);
    // }else{
    //     if (options.searchType === "Name") {
    //         if (options.lastName) {
    //             lastNameRegex = RegExp(options.lastName, "i");
    //         }
    //         if (options.firstName) {
    //             firstNameRegex = RegExp(options.firstName, "i");
    //         }
    //         query = {
    //             $or: [
    //                 {"bio.firstName": firstNameRegex},
    //                 {"bio.lastName": lastNameRegex},
    //             ],
    //             ...query
    //             //"bio.membershipStatus": "APPLICANT"
    //         };
    //
    //         projection.limit = parseInt(options.numResults);
    //
    //         return Meteor.users.find(query, projection);
    //     }
    //
    //     if (options.searchType === "Anan#") {
    //         query = {
    //             "bio.ananNumber": {
    //                 $gte: options.ananNumberStart,
    //                 $lte: options.ananNumberEnd
    //             },
    //             ...query
    //             //"bio.membershipStatus": "APPLICANT"
    //         };
    //
    //         projection.limit = parseInt(options.numResults);
    //         return Meteor.users.find(query, projection);
    //     }
    // }
});

// Meteor.publish('files.documents.all', function (id) {
//     check(id, String);
//
//     return UserDocuments.find({userId: id}).cursor;
// });

Meteor.publish('reports.total.all', function () {
    //check that userId is only Admin before u execute
    return ;
});

Meteor.publish('reports.total.membership.status', function (membership) {
    //check that userId is only Admin before u execute
    const stats = Meteor.users.find({
        "bio.membershipStatus": membership.toUpperCase()
    }, {
        sort: {createdAt: -1}
    });
    return stats;
});

Meteor.publish('reports.total.staff', function () {
    //check that userId is only Admin before u execute
    const staff = Meteor.users.find({
        "bio.role": { $in: ['admin', 'Audit', 'Exams', 'Membership', 'Registry']}
    },{
        sort: {createdAt: -1}
    });
    return staff;
});

Meteor.publish('reports.total', function () {
    //check that userId is only Admin before u execute
    // Publish the number of all comments, re-run every second
    JoinServer.publish({
        context: this,
        name: 'totalStaff',
        interval: 10000,
        doJoin() {
            return Meteor.users.find({
                "bio.role": { $in: ['admin', 'Audit', 'Exams', 'Membership', 'Registry']}
            },{
                sort: {createdAt: -1}
            }).count();
        },
    });

    JoinServer.publish({
        context: this,
        name: 'totalCNA',
        interval: 10000,
        doJoin() {
            return Meteor.users.find({
                "bio.membershipStatus": "CNA"
            }, {
                sort: {createdAt: -1}
            }).count();
        },
    });

    JoinServer.publish({
        context: this,
        name: 'totalFCNA',
        interval: 10000,
        doJoin() {
            return Meteor.users.find({
                "bio.membershipStatus": "FCNA"
            }, {
                sort: {createdAt: -1}
            }).count();
        },
    });

    JoinServer.publish({
        context: this,
        name: 'totalApplicants',
        interval: 10000,
        doJoin() {
            return Meteor.users.find({
                "bio.membershipStatus": "APPLICANT"
                //"bio.role": { $nin: ['admin', 'Audit', 'Exams', 'Membership', 'Registry']}
            },{
                sort: {createdAt: -1}
            }).count();
        },
    });
});

Meteor.publish('reports.document.status', function() {
    JoinServer.publish({
        context: this,
        name: 'deletedApplications',
        interval: 10000,
        doJoin() {
            //return DeletedApplication.find({}).count()
            return 100;
        }
    })

    JoinServer.publish({
        context: this,
        name: 'submittedApplications',
        interval: 10000,
        doJoin() {
            return UserApplication.find({
                "ananNumber": "",
                "isApproved": false,
                "isSubmitted": true
             }).count()
        }
    })

    JoinServer.publish({
        context: this,
        name: 'membersWithDocuments',
        interval: 10000,
        doJoin() {
            return UserApplication.find({
                "ananNumber": { $ne: "" },
                "filesUploaded": { $gte: 1 }
            }).count()
        }
    })

    JoinServer.publish({
        context: this,
        name: 'membersWithoutDocuments',
        interval: 10000,
        doJoin() {
            return UserApplication.find({
                "ananNumber": { $ne: "" },
                "filesUploaded": { $eq: 0 }
            }).count()
        }
    })

    JoinServer.publish({
        context: this,
        name: 'applicantsWithDocuments',
        interval: 10000,
        doJoin() {
            return UserApplication.find({
                "ananNumber": { $eq: "" },
                "filesUploaded": { $gte: 1 }
            }).count()
        }
    })

    JoinServer.publish({
        context: this,
        name: 'applicantsWithoutDocuments',
        interval: 10000,
        doJoin() {
            return UserApplication.find({
                "ananNumber": { $eq: "" },
                "filesUploaded": { $eq: 0 }
            }).count()
        }
    })
});

Meteor.publish('reports.demographics', function () {
    JoinServer.publish({
        context: this,
        name: 'totalFemaleApplicants',
        interval: 10000,
        doJoin() {
            return Meteor.users.find({
                "bio.membershipStatus": "APPLICANT",
                "bio.gender": "Female"
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalMaleApplicants',
        interval: 10000,
        doJoin() {
            return Meteor.users.find({
                "bio.membershipStatus": "APPLICANT",
                "bio.gender": "Male"
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalFemaleMembers',
        interval: 10000,
        doJoin() {
            return Meteor.users.find({
                "bio.membershipStatus": { $in: ['FCNA', 'CNA']},
                "bio.gender": "Female"
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalMaleMembers',
        interval: 10000,
        doJoin() {
            return Meteor.users.find({
                "bio.membershipStatus": { $in: ['FCNA', 'CNA']},
                "bio.gender": "Male"
            }).count();
        }
    });
});

Meteor.publish('reports.applicant.approvals', function () {
    JoinServer.publish({
        context: this,
        name: 'totalApprovedByAudit',
        interval: 10000,
        doJoin() {
            return ApplicantApprovals.find({
                "audit.isApproved": true
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalPendingByAudit',
        interval: 10000,
        doJoin() {
            return ApplicantApprovals.find({
                "audit.isApproved": false
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalApprovedByExam',
        interval: 10000,
        doJoin() {
            return ApplicantApprovals.find({
                "exam.isApproved": true
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalPendingByExam',
        interval: 10000,
        doJoin() {
            return ApplicantApprovals.find({
                "exam.isApproved": false
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalApprovedByMembership',
        interval: 10000,
        doJoin() {
            return ApplicantApprovals.find({
                "membership.isApproved": true
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalPendingByMembership',
        interval: 10000,
        doJoin() {
            return ApplicantApprovals.find({
                "membership.isApproved": false
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalApprovedByRegistry',
        interval: 10000,
        doJoin() {
            return ApplicantApprovals.find({
                "registry.isApproved": true
            }).count();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'totalPendingByRegistry',
        interval: 10000,
        doJoin() {
            return ApplicantApprovals.find({
                "registry.isApproved": false
            }).count();
        }
    });
});

Meteor.publish('reports.audit', function () {
    JoinServer.publish({
        context: this,
        name: 'recent10Login',
        interval: 10000,
        doJoin() {
            let query = {},
                projection = {
                    sort: {datePerformed: -1},
                    fields: { staffName: 1, datePerformed: 1 },
                    limit: 10,
                };
            return LoginAuditTrail.find(query, projection).fetch();
        }
    });

    JoinServer.publish({
        context: this,
        name: 'recent10ApplicationSubmission',
        interval: 10000,
        doJoin() {
            let query = {},
                projection = {
                    sort: {datePerformed: -1},
                    fields: { applicantName: 1, datePerformed: 1 },
                    limit: 10,
                };
            return SubmissionAuditTrail.find(query, projection).fetch();
        }
    });
});
