/**
 * Created by ikedi on 02/12/2016.
 */
import { Accounts } from 'meteor/accounts-base';

Accounts.onCreateUser((options, user) => {
    user.bio = options.bio;
    return user;
});