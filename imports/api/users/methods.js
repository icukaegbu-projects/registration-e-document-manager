/**
 * Created by ikedi on 02/12/2016.
 */
import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import { Email } from 'meteor/email';
import { check } from 'meteor/check';
import moment from 'moment';
import sha512 from 'js-sha512';

import { UserProfileSchema, UserApplicationSchema, UserApplication, UserDocuments, StaffProfileSchema,
         UploadStatistics, ApplicantApprovalsSchema, ApplicantApprovals, ApprovalComments,
        AuditTrail, AuditTrailSchema, LoginAuditTrail, SubmissionAuditTrail,
        DeletedApplication, MemberRecordsUpdate, UserPayment, UserPaymentSchema, UserPaymentReferenceSchema  } from './collections';

import actionTypes from './actionTypes';

import { createUserFolder_V2, moveUserFolder_V2, deleteUserDocument_V2, getUserDocumentsList_V2 } from './DropboxMethods_V2';

const confirmationEmail = (name, email, password, phone) => {
    let mail = `
    Hi ${name}

    Thanks for signing up to start this wonderful journey in the world of accounting on the platform of ANAN (Association of National Accountants of Nigeria).

    We are excited to have you, and will give you regular updates on events, trainings, seminars and any other pertinent information to help you along in your journey.

    In the meantime, your login details are:\nEmail: ${email}\n Password: ${password}\n Phone: ${phone}\n

    Welcome onboard. Let’s make accounting proud and Nigeria better.

    Cheers.
    The Registration Team
    Association of National Accountants of Nigeria.`;
    return mail;
};

// export const recordAuditTrail = new ValidatedMethod({
//     name: 'record.audit.trail',
//     validate: AuditTrailSchema.validator(),
//     run({ staffId, actionType, actionStatus, actionDetails }) {
//         let u = {
//             staffId,
//             actionType,
//             actionStatus, //SET IF ACTION SUCCEEDS OR FAILS
//             actionDetails,
//             datePerformed: new Date()
//         };
//
//         let r = AuditTrail.insert(u);
//
//         return r;
//     }
// });

export const recordAuditTrail = (record, auditTrailType = 0) => {
    AuditTrailSchema.validate(record);

    const { staffId, staffName, actionType, actionStatus, actionDetails } = record;
    let u = {
        staffId,
        staffName: staffName.toUpperCase(),
        actionType,
        actionStatus, //SET IF ACTION SUCCEEDS OR FAILS
        actionDetails: actionDetails.toUpperCase(),
        datePerformed: new Date()
    };

    let r = 0;

    if(auditTrailType === 0) {
        r = AuditTrail.insert(u);
    } else if(auditTrailType === 1){
        r = LoginAuditTrail.insert(u);
    }else if(auditTrailType === 2) {
        u = {
            applicantId: staffId,
            applicantName: staffName.toUpperCase(),
            actionType,
            actionStatus, //SET IF ACTION SUCCEEDS OR FAILS
            actionDetails: actionDetails.toUpperCase(),
            datePerformed: new Date()
        };

        r = SubmissionAuditTrail.insert(u);
    }

    return r;
};


//UTILITY METHODS
export const deleteUnsubmittedApplications = async () => {
    let passedDelete = 0;
    const secondsPerDay = 86400, numDays = 14, msPerSecond = 1000;

    //get all users who are members
    let query = {
        $and: [
            { "ananNumber": "" },
            { "isSubmitted": false },
            {
                $gte: [{
                    $subtract: [new Date(), "applicationStarted"]
                }, numDays * secondsPerDay * msPerSecond]
            }
        ]

    },
    projection = {
        fields: {
            "userId": 1,
            "isSubmitted": 1,
            "applicationStarted": 1
        }
    };

    const userApplications = UserApplication.find(query, fields);

    //for each user, get a list of the documents,
    userApplications.forEach(user => {
        //get the list of documents, using the ananNumber
        const userData = Meteor.findOne({ _id: user.userId }, { fields: { "bio": 1, "emails": 1, "username": 1 } });
        const { lastName, firstName, middleName } = userData.bio;

        const userId = user.userId,
            path = `/${lastName.toUpperCase()}-${firstName.toUpperCase()}-${userData.username.toUpperCase()}`

        deleteUser_V2.call({ path, userId }, (err, result) => {
            if(err) {
                console.log(err);
            }else{
                //if user was successfully deleted, insert a record
                const deletedUser = {
                    userId,
                    email: userData.emails[0].address,
                    firstName,
                    lastName,
                    middleName,
                    applicationStarted: user.applicationStarted,
                    applicationDeleted: new Date()
                }

                const res = DeletedApplication.insert(deletedUser);

                if(res) {
                    passedDelete += 1;
                }
            }
        });
    });

    return {
        passedDelete
    }
}

export const membersDocumentList = async () => {
    let failedUpdate = 0, passedUpdate = 0;

    //get all users who are members
    let query = {
        "bio.membershipStatus": {
            $in: ["CNA", "FCNA"]
        },
        "bio.lastName": { $ne: "DELETED" }  //EXCLUDE ALL NAMES WHO HAVE BEEN DELETED
    },
    projection = {
        //sort by ananNumber descending
        sort: { "bio.ananNumber": -1, created_at: 1},
        fields: {
            "username": 1,
            "emails": 1,
            "bio": 1,
            "createdAt": 1
        }
    };

    const users = Meteor.users.find(query, projection);
    let records = [];

    //for each user, get a list of the documents,
    users.forEach(user => {
        //get the list of documents, using the ananNumber
        const options = {
            folderName: user.bio.ananNumber
        };
        let recordsUpdate = {
            userId: user._id,
            ananNumber: user.bio.ananNumber,
            bio:  {
                firstName: user.bio.firstName,
                lastName: user.bio.lastName,
                middleName: user.bio.middleName
            }
        }

        getUserDocumentsList_V2.call(options, (message, result) => {
            if(message) {
                //return message;
                failedUpdate += 1;
                recordsUpdate.bio.updateStatus = false;
                console.log('Failed: ', options.folderName, '. TotalFailed: ', failedUpdate);

                //update the UserApplication with the number of files
                let res = UserApplication.update({ userId: user._id }, {
                    $set: {
                        "filesUploaded": 'N/A',
                        "filesOutstanding": 'N/A'
                    }
                });
            }else{
                //update the UserApplication with the number of files
                let res = UserApplication.update({ userId: user._id }, {
                    $set: {
                        "filesUploaded": result.length || 0,
                        "filesOutstanding": 0
                    }
                });

                if (res) {
                    passedUpdate += 1;
                    recordsUpdate.bio.updateStatus = true
                }

                //console.log(result);
                console.log("folder returned successfully: ", options.folderName, '. TotalPassed: ', passedUpdate);
            }
        });

        //add this record to records array
        records.push(recordsUpdate);
    });

    //push all records to disk
    MemberRecordsUpdate.insert(records);

    return {
        failedUpdate,
        passedUpdate
    }
}

export const applicantsDocumentList = async () => {
    let failedUpdate = 0, passedUpdate = 0;

    //get all users who are members
    let query = {
        "bio.membershipStatus": "APPLICANT"
    },
    projection = {
        //sort by ananNumber descending
        sort: {created_at: 1},
        fields: {
            "username": 1,
            "emails": 1,
            "bio": 1,
            "createdAt": 1
        }
    };

    const users = Meteor.users.find(query, projection);

    //for each user, get a list of the documents,
    users.forEach(user => {
        const userApplication = UserApplication.findOne({ userId: user._id}, {
            fields: {
                files: 1,
                filesUploaded: 1,
                filesOutstanding: 1
            }
        });

        let filesUploaded = 0, filesOutstanding = 0;

        userApplication.files.forEach(file => {
            isUploaded === true ? filesUploaded +=1 : filesOutstanding += 1
        });

        let res = UserApplication.update({ userId: user._id }, {
            $set: {
                "filesUploaded": filesUploaded,
                "filesOutstanding": filesOutstanding
            }
        });

        if (res) {
            passedUpdate += 1;
        } else {
            failedUpdate += 1
        }

        //get the list of documents, using the ananNumber
        // const options = {
        //     folderName: user.bio.lastName.toUpperCase()+"-"+user.bio.firstName.toUpperCase()+"-"+user.username.toUpperCase();
        // };
        // getUserDocumentList_V2.call(options, (message, result) => {
        //     if(message) {
        //         console.log(message);
        //         return message;
        //     }else{
        //         console.log(result);
        //         console.log("folder returned successfully");

        //         //update the UserApplication with the number of files
        //         let res = UserApplication.update({ userId: user._id }, {
        //             $set: {
        //                 "filesUploaded": result.length || 0,
        //                 "filesOutstanding": 0
        //             }
        //         })
        //     }
        // });
    });

    return {
        failedUpdate,
        passedUpdate
    }
}


//VALIDATED METHODS
export const userSignup = new ValidatedMethod({
    name: 'users.signup',
    validate: UserProfileSchema.validator(),
    run (userData){
        let email = userData.emails[0].address,
            password = userData.password,
            phone = userData.phone;

        let userId = '', userApplicationId = '', userPaymentId = '';

        if(Meteor.isServer){
            //if email and phone is unique, create the user
            userId = Accounts.createUser({
                username: userData.username,
                email: email,
                password: userData.password,
                bio: {
                    acceptDeclaration: userData.acceptDeclaration,
                    role: 'Applicant',

                    firstName: userData.firstName,
                    middleName: userData.middleName,
                    lastName: userData.lastName,
                    phone: userData.phone,
                    secondPhone: userData.secondPhone,
                    gender: userData.gender,
                    maritalStatus: userData.maritalStatus,
                    dob: userData.dob,
                    privateAddress: userData.privateAddress,
                    stateOfOrigin: userData.stateOfOrigin,
                    nationality: userData.nationality,
                    ananBranch: userData.ananBranch,
                    membershipStatus: 'APPLICANT'
                }
            });

            if(userId) {
                //USER WAS SUCCESSFULLY CREATED, CREATE AN APPLICATION FOR HIM
                //files a user is expected to attach to his application
                const files = [
                    {
                        dropboxPath: "",
                        title: "Passport / Photograph (in .jpg only)",
                        nameFormat: "Passport.jpg",
                        isUploaded: false,
                    },
                    {
                        dropboxPath: "",
                        title: "Declaration (Signed by Commissioner of Oaths)",
                        nameFormat: "Declaration_Signed_by_Commissioner_of_Oaths.pdf",
                        isUploaded: false,
                    },
                    {
                        dropboxPath: "",
                        title: "Attestation Letter from Employer",
                        nameFormat: "Attestation_Letter_from_Employer.pdf",
                        isUploaded: false,
                    },
                    {
                        dropboxPath: "",
                        title: "Letter of Promotion",
                        nameFormat: "Letter_of_Promotion.pdf",
                        isUploaded: false,
                    },
                    {
                        dropboxPath: "",
                        title: "Letter of Appointment",
                        nameFormat: "Letter_of_Appointment.pdf",
                        isUploaded: false,
                    },
                    {
                        dropboxPath: "",
                        title: "BSc/HND Certificate or Statement of Result",
                        nameFormat: "BSc_HND_Certificate_or_Statement_of_Result.pdf",
                        isUploaded: false
                    },
                    {
                        dropboxPath: "",
                        title: "NYSC Discharge Certificate / Exemption",
                        nameFormat: "NYSC_Discharge_Certificate_or_Exemption.pdf",
                        isUploaded: false
                    },
                    {
                        dropboxPath: "",
                        title: "NCA Statement of Result",
                        nameFormat: "NCA_Statement_of_Result.pdf",
                        isUploaded: false
                    },
                    {
                        dropboxPath: "",
                        title: "NCA Conversion of Result (if applicable)",
                        nameFormat: "NCA_Conversion_of_Result.pdf",
                        isUploaded: false
                    },
                    {
                        dropboxPath: "",
                        title: "A.I.T Workshop",
                        nameFormat: "AIT_Workshop.pdf",
                        isUploaded: false
                    },
                    {
                        dropboxPath: "",
                        title: "Evidence of Change of Name (if Applicable)",
                        nameFormat: "Evidence_of_Change_of_Name.pdf",
                        isUploaded: false
                    },
                    {
                        dropboxPath: "",
                        title: "Certificate of Sponsor with Branch Endorsement",
                        nameFormat: "Certificate_of_Sponsor_with_Branch_Endorsement.pdf",
                        isUploaded: false
                    },
                    {
                        dropboxPath: "",
                        title: "Scanned Signature",
                        nameFormat: "Scanned_Signature.pdf",
                        isUploaded: false
                    },
                ];
                const fileCount = files.length;

                let userApp = {
                    userId: userId,
                    isSubmitted: false,            //false = Saved, true = Submitted
                    isApproved: 'Not Approved',    //false = not approved, true = approved
                    files: files,

                    filesUploaded: 0,
                    filesOutstanding: fileCount,

                    applicationStarted: new Date(),
                    applicationSubmitted: null,
                    applicationApproved: null,

                    nextOfKinName: "",
                    nextOfKinPhone: "",
                    nextOfKinRelationship: "",
                    nextOfKinAddress: "",

                    tertiaryInstitutionAttended: "",
                    qualificationsWithDates: "",

                    ananProfessionalExamDate: null,
                    ananTrainingStartDate: null,
                    ananTrainingEndDate: null,

                    presentEmployer: "",
                    employerAddress: "",
                    positionHeld: "",
                    appointmentDate: "",
                    supportingParticulars: "",

                    financialMisconduct: "",
                    financialMisconductDetails: "",
                    courtConviction: "",
                    courtConvictionDetails: "",
                    bankruptcy: "",
                    bankruptcyDetails: "",

                    previousAnanApplication: "",
                    previousAnanApplicationDetails: "",
                };

                let userPayment = {
                    custReference: userId,
                    isPaymentMade: false,
                    paymentDate: null,
                    amount: null
                };

                userApplicationId = UserApplication.insert(userApp);
                userPaymentId = UserPayment.insert(userPayment);
                // const all = UserPayment.find({}).fetch()
                // console.log(all)
            }

            Roles.addUsersToRoles(userId, 'applicant');

            //USER APPLICATION SUCCESSFULLY CREATED
            //CREATE A DROPBOX FOLDER FOR THE USER
            //USING HIS FULL_NAME COMBINATION
            let folderName = `${userData.lastName.toUpperCase()}-${userData.firstName.toUpperCase()}-${userData.username.toUpperCase()}`

            createUserFolder_V2.call({ folderName }, (message, result) => {
                if(message) {
                    return message;
                }else{
                    console.log(result);
                    console.log("Folder Succesfully created");
                }
            });

            //user registration is successful, send a confirmation email to user
            this.unblock();

            //SEND VERIFICATION LINK
            //Accounts.sendVerificationEmail(userId);
            //console.log("Verification Email Sent");

            //SEND EMAIL
            // let name = userData.lastName.toUpperCase()+", "+userData.firstName.toUpperCase();
            // const mailBody = confirmationEmail(name, email, password, phone);
            // Email.send({
            //     //to: 'ij.ekemezie@yahoo.com',
            //     to: email,
            //     from: 'Registration Team <anan@accountant.com>',
            //     subject: 'Welcome to ANAN',
            //     text: mailBody
            // });

            let u = Meteor.users.findOne(userId, {
                fields: {
                    "username": 1,
                    "emails": 1,
                    "bio": 1,
                    "roles": 1
                }
            });
            let ua = UserApplication.findOne(userApplicationId);

            return {
                u,
                ua
            }
        }
    }
});

export const userApplication = new ValidatedMethod({
    name: 'users.application',
    validate: UserApplicationSchema.validator(),
    run (userData){
        //if isSubmitted, set the applicationSubmitted date
        if(userData.isSubmitted) {
            userData.applicationSubmitted = new Date();

            //application is submitted, generate audit trail
            let username = Meteor.users.findOne({_id: userData.userId}).username;

            recordAuditTrail({
                staffId: userData.userId,
                staffName: username,
                actionType: actionTypes.SUBMIT_APPLICATION,
                actionStatus: actionTypes.SUCCESS, //SET IF ACTION SUCCEEDS OR FAILS
                actionDetails: `${username} submitted application on: ${moment(userData.applicationSubmitted).format('DD/MM/YYYY HH:MM')}; started: ${moment(userData).applicationStarted.format('DD/MM/YYYY HH:MM')}`
            }, 2);
        }

        let success = UserApplication.update({userId: userData.userId}, userData);

        //create a new record for the user in the ApplicantApprovals table
        let applicantApproval = {
            applicantId: userData.userId,
            audit: {
                dateApproved: null,
                approvedBy: '',
                isApproved: false
            },
            exam: {
                dateApproved: null,
                approvedBy: '',
                isApproved: false
            },
            membership: {
                dateApproved: null,
                approvedBy: '',
                isApproved: false
            },
            registry: {
                dateApproved: null,
                approvedBy: '',
                isApproved: false
            }
        };

        let applicant = ApplicantApprovals.insert(applicantApproval);

        // if(Meteor.isServer){
        //    //if isSubmitted, send email to notify user his application has been submitted
        //     if(userData.isSubmitted) {
        //         this.unblock();
        //         console.log('sending update email');
        //         const mailBody = confirmationEmail(email, password, phone);
        //         Email.send({
        //             to: user.email,
        //             from: 'admin@poploda.com',
        //             subject: 'Successful Signup on Poploda',
        //             text: mailBody
        //         });
        //     }
        // }
    }
});

export const userAssignAnanNumber = new ValidatedMethod({
    name: 'users.assignAnanNumber',
    validate: new SimpleSchema({
        userId: {type: String},
        ananNumber: {type: String}
    }).validator(),
    run (userData){
        //verify that the ANAN number is unique
        let verify = UserApplication.findOne({ananNumber: userData.ananNumber});
        let theUser = Meteor.users.findOne({ _id: userData.userId });
        let staffName = Meteor.users.findOne(this.userId).username;

        if(verify) {
            recordAuditTrail({
                staffId: this.userId,
                staffName,
                actionType: actionTypes.ASSIGN_ANAN_NUMBER,
                actionStatus: actionTypes.FAILURE, //SET IF ACTION SUCCEEDS OR FAILS
                actionDetails: `ANAN NUMBER ALREADY EXISTS`
            });

            throw new Meteor.Error('Existing ANAN Number', 'ANAN Number exists, try a new one')
        }

        //verify that the user does not already have an ANAN number
        let user = UserApplication.findOne({userId: userData.userId});
        if (user.ananNumber) {
            //LOG FAILURE
            recordAuditTrail({
                staffId: this.userId,
                staffName,
                actionType: actionTypes.ASSIGN_ANAN_NUMBER,
                actionStatus: actionTypes.FAILURE, //SET IF ACTION SUCCEEDS OR FAILS
                actionDetails: `${theUser.username} ALREADY HAS AN ANAN NUMBER`
            });

            throw new Meteor.Error('User already has ANAN Number', 'User already has ANAN Number');
        }

        //RENAME THE FOLDER ON DROPBOX TO ANAN-NUMBER
        //1. CREATE NEW FOLDER USING ANAN-NUMBER
        //2. MOVE CONTENTS OF FORMER FOLDER (USING NAME COMBO) TO NEW FOLDER
        //3. DELETE FOLDER
        //console.log('Starting to rename file...............')
        let fromPath = `/${theUser.bio.lastName.toUpperCase()}-${theUser.bio.firstName.toUpperCase()}-${theUser.username.toUpperCase()}`
        let toPath = `/${userData.ananNumber}`;

        //MOVE CONTENTS FROM OLD FOLDER TO NEW FOLDER
        let moveFiles = moveUserFolder_V2.call({ to_path: toPath, from_path: fromPath }, (message, result) => {
            if(message) {
                return message;
            }else{
                if(result) {
                    deleteUserDocument_V2.call({ path: fromPath }, (message, result) => {
                        if(message) {
                            return message
                        }else{
                            return result
                        }
                    })
                }
            }
        });

        //iterate over all the files and change the dropbox path to match
        user.ananNumber = userData.ananNumber;
        user.isApproved = true;
        user.applicationApproved = new Date();
        user.files.forEach(file => {
            file.dropboxPath = `/${userData.ananNumber}/${file.nameFormat}`
            //console.log(file)
        });

        //ALSO ASSIGN AN APPROVED DATE TO BIO & USERAPPLICATION
        //FINALLY SEND APPROVAL LETTER / CONFIRMATION TO USER
        let success = UserApplication.update({userId: userData.userId}, user);
        //ALSO INSERT INTO BIO OF METEOR.USERS
        let usersUpdate = Meteor.users.update({
            _id: userData.userId
        }, {
            $set: {
                "bio.ananNumber": userData.ananNumber,
                "bio.membershipStatus": "CNA"
            }
        });

        if(usersUpdate > 0) {
            recordAuditTrail({
                staffId: this.userId,
                staffName,
                actionType: actionTypes.ASSIGN_ANAN_NUMBER,
                actionStatus: actionTypes.SUCCESS, //SET IF ACTION SUCCEEDS OR FAILS
                actionDetails: `${theUser.username} SUCCESSFULLY ASSIGNED ANAN NUMBER ${userData.ananNumber}`
            });
        }
    }
});

export const saveArchiveUserAndStatistics = new ValidatedMethod({
    name: 'users.archive.statistics',
    validate: UserApplicationSchema.validator(),
    run (userData){
        let success = UserApplication.update({userId: userData.userId}, userData);
        if(success) {
            let user = Meteor.users.findOne({ _id: userData.userId },
                {
                    fields: { "bio": 1, "role": 1 }
                });

            //CHANGE USER ROLE FROM APPLICANT TO ARCHIVED
            //Roles.removeUsersFromRoles(user._id, 'applicant');
            Roles.setUserRoles(user._id, 'archivedMember');

            let name = `${user.bio.lastName.toUpperCase()}, ${user.bio.firstName} ${user.bio.middleName}`;

            let stats = {
                staffId: this.userId,
                memberId: userData.userId,
                memberApplicationId: userData._id,
                memberName: name,
                dateUploaded: new Date()
            };

            let statsSuccess = UploadStatistics.insert(stats);
        }
    }
});

export const applicantApprovals = new ValidatedMethod({
    name: 'users.applicant.approvals',
    validate: ApplicantApprovalsSchema.validator(),
    run (userData){
        console.log("User Data: ",userData);
        //if it doesnt exist, create it. If it does, update it
        if(Meteor.isServer) {
            let success = ApplicantApprovals.update({_id: userData._id, applicantId: userData.applicantId}, userData);

            let staff = Meteor.users.findOne(this.userId, { fields: { username: 1, roles: 1 }} );
            let userName = Meteor.users.findOne(userData.applicantId, { fields: { username: 1, roles: 1 }}).username;

            if(success > 0) {
                recordAuditTrail({
                    staffId: this.userId,
                    staffName: staff.username,
                    actionType: actionTypes.APPROVE_APPLICANT,
                    actionStatus: actionTypes.SUCCESS, //SET IF ACTION SUCCEEDS OR FAILS
                    actionDetails: `${staff.username} WITH ROLE ${staff.roles[0]} SUCCESSFULLY APPROVED APPLICANT WITH USERNAME: ${userName}`
                });
            }else{
                recordAuditTrail({
                    staffId: this.userId,
                    staffName: staff.username,
                    actionType: actionTypes.APPROVE_APPLICANT,
                    actionStatus: actionTypes.FAILURE, //SET IF ACTION SUCCEEDS OR FAILS
                    actionDetails: `${staff.username} WITH ROLE ${staff.roles[0]} COULD NOT APPROVE APPLICANT WITH USERNAME: ${userName}`
                });
            }
        }
    }
});

export const addApprovalComments = new ValidatedMethod({
    name: 'users.add.comments',
    validate: new SimpleSchema({
        staffId: { type: String },
        staffRole: { type: String },
        comment: { type: String },
        applicantId: { type: String }
    }).validator(),
    run ({ applicantId, staffId, staffRole, comment }){
        //based on role, determine which to set
        //if a delete operation, staffId and comment will be null,
        //therefore make date also null
        let options = {},
            dateCommented = staffId === "" && comment === ""
                            ? "" : new Date();

        if (staffRole === 'Membership'){
            options = {
                "membership": {
                    staffId,
                    comment,
                    dateCommented
                }
            }
        }else if (staffRole === 'Audit'){
            options = {
                "audit": {
                    staffId,
                    comment,
                    dateCommented
                }
            }
        }else if (staffRole === 'Exams'){
            options = {
                "exam": {
                    staffId,
                    comment,
                    dateCommented
                }
            }
        }else if (staffRole === 'Registry'){
            options = {
                "registry": {
                    staffId,
                    comment,
                    dateCommented
                }
            }
        }

        let success = ApprovalComments.upsert({ applicantId: applicantId },
            {
                $set: options
            }
        );
        return success;
    }
});

export const staffSignup = new ValidatedMethod({
    name: 'staff.signup',
    validate: StaffProfileSchema.validator(),
    run (userData){
        let email = userData.emails[0].address;

        let userId = '', userApplicationId = '';

        if(Meteor.isServer){
            //if email and phone is unique, create the user
            userId = Accounts.createUser({
                username: userData.username,
                email: email,
                password: userData.password,
                createdAt: new Date(),
                bio: {
                    role: userData.role,

                    firstName: userData.firstName,
                    middleName: userData.middleName,
                    lastName: userData.lastName,
                    phone: userData.phone,
                    gender: userData.gender,
                    maritalStatus: userData.maritalStatus,
                    privateAddress: userData.privateAddress,
                    stateOfOrigin: userData.stateOfOrigin,
                }
            });

            //if successful, assign role and then record an audit trail
            const staffName = Meteor.users.findOne(this.userId).username;
            if(userId) {
                Roles.addUsersToRoles(userId, userData.role);

                //staff registration successful
                recordAuditTrail({
                    staffId: this.userId,
                    staffName,
                    actionType: actionTypes.CREATE_STAFF,
                    actionStatus: actionTypes.SUCCESS, //SET IF ACTION SUCCEEDS OR FAILS
                    actionDetails: `${staffName} SUCCESSFULLY CREATED STAFF WITH USERNAME: ${userData.username}`
                });
            }else{
                //insert failed, record failed
                recordAuditTrail({
                    staffId: this.userId,
                    staffName,
                    actionType: actionTypes.CREATE_STAFF,
                    actionStatus: actionTypes.FAILURE, //SET IF ACTION SUCCEEDS OR FAILS
                    actionDetails: `${staffName} FAILED TO CREATE STAFF WITH USERNAME: ${userData.username}`
                });
            }

            //staff registration is successful, send a confirmation email to user
            //this.unblock();
            //const mailBody = confirmationEmail(email, password, phone);
            // Email.send({
            //     to: user.email,
            //     from: 'admin@poploda.com',
            //     subject: 'Successful Signup on Poploda',
            //     text: mailBody
            // });
        }
    }
});

export const staffDelete = new ValidatedMethod({
    name: 'staff.delete',
    validate: new SimpleSchema({
        userId: { type: String }
    }).validator(),
    run({ userId }) {
        let u = null;

        const staffName = Meteor.users.findOne(this.userId).username;
        const employeeName = Meteor.users.findOne(userId).username;

        try {
            //delete the user from UserApplication and Meteor.users
            u = Meteor.users.remove({ _id: userId });

            //record successful delete
            recordAuditTrail({
                staffId: this.userId,
                staffName,
                actionType: actionTypes.DELETE_STAFF,
                actionStatus: actionTypes.SUCCESS, //SET IF ACTION SUCCEEDS OR FAILS
                actionDetails: `${staffName} SUCCESSFULLY DELETED STAFF WITH USERNAME: ${employeeName}`
            });
        } catch (e) {
            // handle this however you want
            recordAuditTrail({
                staffId: this.userId,
                staffName,
                actionType: actionTypes.DELETE_STAFF,
                actionStatus: actionTypes.FAILURE, //SET IF ACTION SUCCEEDS OR FAILS
                actionDetails: `${staffName} FAILED TO DELETE STAFF WITH USERNAME: ${employeeName}`
            });

            throw new Meteor.Error('staff-delete', 'Failed to delete staff');
        }

        return u;
    }
});

export const uploadMembersCSV = new ValidatedMethod({
    name: 'users.upload.members.csv',
    validate: new SimpleSchema({
        data: { type: [Object]},
        'data.$.ananNumber': {type: String, optional: true},
        'data.$.email': {type: String, optional: true},
        'data.$.membershipStatus': {type: String, optional: true},
        'data.$.lastName': {type: String, optional: true},
        'data.$.firstName': {type: String, optional: true},
        'data.$.middleName': {type: String, optional: true},
        'data.$.privateAddress': {type: String, optional: true},
        'data.$.phone': {type: String, optional: true},
        'data.$.secondPhone': {type: String, optional: true},
        'data.$.ananBranch': {type: String, optional: true},
    }).validator(),
    run ({ data }){
        let failedData = [], successData = [];

        for ( let i = 0; i < data.length; i++ ) {
            let item   = data[ i ],
                exists = Meteor.users.findOne( { "bio.ananNumber": item.ananNumber } ),
                user = null;

            if ( !exists ) {
                user   = {
                    username: item.ananNumber,
                    password: `anan${item.ananNumber}`,
                    email: item.email ? item.email : `${item.ananNumber}@anan.org.ng`,
                    bio: {
                        firstName: item.firstName,
                        middleName: item.middleName,
                        lastName: item.lastName,
                        phone: item.phone,
                        secondPhone: item.secondPhone,
                        privateAddress: item.privateAddress,
                        nationality: "Nigerian",
                        ananBranch: item.ananBranch,
                        membershipStatus: item.membershipStatus,
                        ananNumber: item.ananNumber,
                        role: "Member"
                    }
                };

                //insert into Meteor.users and UserApplication
                let userId = Meteor.users.insert( user );

                let userApp = {
                    userId: userId,
                    isSubmitted: true,            //false = Saved, true = Submitted
                    isApproved: 'Archived User',    //false = not approved, true = approved

                    nextOfKinName: "",
                    nextOfKinPhone: "",
                    nextOfKinRelationship: "",
                    nextOfKinAddress: "",

                    tertiaryInstitutionAttended: "",
                    qualificationsWithDates: "",

                    ananProfessionalExamDate: null,
                    ananTrainingStartDate: null,
                    ananTrainingEndDate: null,

                    presentEmployer: "",
                    employerAddress: "",
                    positionHeld: "",
                    appointmentDate: "",
                    supportingParticulars: "",

                    financialMisconduct: "",
                    financialMisconductDetails: "",
                    courtConviction: "",
                    courtConvictionDetails: "",
                    bankruptcy: "",
                    bankruptcyDetails: "",

                    previousAnanApplication: "",
                    previousAnanApplicationDetails: "",
                };

                let userApplicationId = UserApplication.insert(userApp);

                //add role
                Roles.addUsersToRoles(userId, 'Member');

                successData.push(user.ananNumber);
            } else {
                //console.warn( 'Rejected. This user already exists.' );
                failedData.push(user.ananNumber);
            }
        }

        return {
            successData,
            failedData
        }
    }
});

export const userPayment = new ValidatedMethod({
    name: 'users.payment',
    validate: UserPaymentReferenceSchema.validator(),
    run (userData){
        console.log("hello", userData)
        const { PaymentReference, PaymentDate, CustReference, CustomerName, CustomerEmail,
                CustomerPhoneNo, amount, product, products, TotalAmount
            } = userData;

        const MerchantID = "000001";
        const API_KEY = "ANAN_KEY1";
        const concatString = `${MerchantID}${PaymentReference}${API_KEY}`;
        const hash = sha512(concatString).toLowerCase();

        //make call to api to save payment
        const data = {
            MerchantID,
            PaymentReference,
            CustReference,
            CustomerName,
            phoneno: CustomerPhoneNo,
            memail: CustomerEmail,
            hash,
            PaymentItemsString: JSON.stringify(userData.products),
            TotalAmount,
        }

        const userPaymentData = {
            custReference: userData._id,
            isPaymentMade: true,
            paymentReference: PaymentReference,
            paymentDate: PaymentDate,
            amount: TotalAmount
        };

        console.log(data)


        // let success = UserPayment.update({ custReference: userData.CustReference }, userData);
        // if(success) {
        //     return success;
        // }else{
        //     UserPayment.insert(userData);
        // }
    }
});

//USER-DOCUMENTS
Meteor.methods({
    // 'remove.user.documents'({ docId, userId }) {
    //     let success = UserDocuments.remove({_id: docId});
    // },

    'staff.login.audit.trail'() {
        if(Meteor.isServer) {
            let staffName = Meteor.users.findOne(this.userId, { fields: { username: 1}}).username;

            recordAuditTrail({
                staffId: this.userId,
                staffName,
                actionType: actionTypes.LOGGED_IN,
                actionStatus: actionTypes.SUCCESS, //SET IF ACTION SUCCEEDS OR FAILS
                actionDetails: `${staffName} successfully logged in`
            }, 1);
        }
    },

    'upload.members.csv'({ data }){
        check(data, Array);
        //console.log("Starting...")
        let failedData = [], successData = [];

        for ( let i = 0; i < data.length; i++ ) {
            let item   = data[ i ], exists = false, user = null;
            if(item){
                exists = Meteor.users.findOne( { "bio.ananNumber": item.ananNumber } )
            }

            //check if email already exists; if it does but ananNumber is unique, use the ananNumber as email
            let checkMail = data.find((elem, index) => {
                //check if current index is not equal i
                if (index !== i) {
                    return elem.email === item.email
                }
            });

            if ( !exists ) {
                user   = {
                    username: item.ananNumber.trim(),
                    password: `anan${item.ananNumber.trim()}`,
                    emails: item.email && item.email !== "." && item.email !== "" && item.email !== "nil" && !checkMail ?
                        [{ address: item.email.trim(), verified: false }]
                        : [{ address: `${item.ananNumber.trim()}@anan.org.ng`, verified: false }],
                    bio: {
                        firstName: item.firstName.trim(),
                        middleName: item.middleName.trim(),
                        lastName: item.lastName.trim(),
                        phone: item.phone.trim(),
                        secondPhone: item.secondPhone.trim(),
                        privateAddress: item.privateAddress.trim(),
                        nationality: "Nigerian",
                        ananBranch: item.ananBranch.trim(),
                        membershipStatus: item.membershipStatus.trim(),
                        ananNumber: item.ananNumber.trim(),
                    }
                };

                //insert into Meteor.users and UserApplication
                let userId = Meteor.users.insert( user );

                let userApp = {
                    userId: userId,
                    isSubmitted: true,            //false = Saved, true = Submitted
                    isApproved: 'Archived User',    //false = not approved, true = approved

                    nextOfKinName: "",
                    nextOfKinPhone: "",
                    nextOfKinRelationship: "",
                    nextOfKinAddress: "",

                    tertiaryInstitutionAttended: "",
                    qualificationsWithDates: "",

                    ananProfessionalExamDate: null,
                    ananTrainingStartDate: null,
                    ananTrainingEndDate: null,

                    presentEmployer: "",
                    employerAddress: "",
                    positionHeld: "",
                    appointmentDate: "",
                    supportingParticulars: "",

                    financialMisconduct: "",
                    financialMisconductDetails: "",
                    courtConviction: "",
                    courtConvictionDetails: "",
                    bankruptcy: "",
                    bankruptcyDetails: "",

                    previousAnanApplication: "",
                    previousAnanApplicationDetails: "",
                };

                let userApplicationId = UserApplication.insert(userApp);

                //add role
                Roles.addUsersToRoles(userId, 'Member');

                successData.push(user.ananNumber);
            } else {
                // console.log("Index at failure: "+i);
                // console.log("Current number in success: "+successData.length);
                // console.warn( 'Rejected. This user already exists.' );
                failedData.push(user);
                continue;
            }
        }

        return {
            successData,
            failedData
        }

    }
});

