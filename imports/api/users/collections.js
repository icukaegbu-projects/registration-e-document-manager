/**
 * Created by ikedi on 02/12/2016.
 */
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Mongo } from 'meteor/mongo'
//import { FilesCollection } from 'meteor/ostrio:files';

//define schemas
export const StaffProfileSchema = new SimpleSchema({
    _id: {type: String, optional: true},
    email: {type: String},
    emails: {type: Array},
    'emails.$': {type: Object},
    'emails.$.address': {type: String},
    'emails.$.verified': {type: Boolean},
    password: {type: String},
    confirmPassword: {type: String},
    username: {type: String},
    role: {type: String},
    firstName: {type: String},
    middleName: {type: String, optional: true},
    lastName: {type: String},
    phone: {type: String},
    gender: {type: String},
    maritalStatus: {type: String},
    privateAddress: {type: String},
    stateOfOrigin: {type: String},
});

export const UserProfileSchema = new SimpleSchema({
    _id: {type: String, optional: true},
    acceptDeclaration: {type: String},
    email: {type: String},
    emails: {type: Array},
    'emails.$': {type: Object},
    'emails.$.address': {type: String},
    'emails.$.verified': {type: Boolean},
    password: {type: String},
    confirmPassword: {type: String},
    username: {type: String},
    firstName: {type: String},
    middleName: {type: String, optional: true},
    lastName: {type: String},
    phone: {type: String},
    secondPhone: {type: String, optional: true},
    gender: {type: String},
    maritalStatus: {type: String},
    dob: {type: Date, optional: true},
    privateAddress: {type: String},
    stateOfOrigin: {type: String},
    nationality: {type: String},
    ananNumber: {type: String, optional: true},
    ananBranch: {type: String, optional: true},
    membershipStatus: {type: String, optional: true},
});

export const UserApplicationSchema = new SimpleSchema({
    _id: {type: String, optional: true},
    userId: {type: String, optional: true},  //HOLDS THE ID OF THE USER THAT OWNS THIS PROFILE
    ananNumber: {type: String, optional: true},  //HOLDS THE ASSIGNED ANAN NUMBER AFTER APPROVALS
    isSubmitted: {type: Boolean, optional: true},    //false = Saved, true = Submitted

    files: { type: Array, optional: true },
    'files.$': {type: Object},
    'files.$.nameFormat': {type: String},
    'files.$.dropboxPath': {type: String},
    'files.$.title': {type: String},
    'files.$.isUploaded': {type: Boolean},

    filesUploaded: { type: Number, optional: true },
    filesOutstanding: { type: Number, optional: true },

    applicationStarted: {type: Date, optional: true},
    applicationSubmitted: {type: Date, optional: true},
    applicationApproved: {type: Date, optional: true},
    isApproved: {type: String, optional: true},    //false = not approved, true = approved
    services: {type: Object, blackbox: true, optional: true},

    nextOfKinName: { type: String, optional: true},
    nextOfKinPhone: { type: String, optional: true},
    nextOfKinRelationship: { type: String, optional: true},
    nextOfKinAddress: { type: String, optional: true},

    tertiaryInstitutionAttended: { type: String, optional: true},
    qualificationsWithDates: { type: String, optional: true},

    ananProfessionalExamDate: {type: Date, optional: true},
    ananTrainingStartDate: {type: Date, optional: true},
    ananTrainingEndDate: {type: Date, optional: true},

    presentEmployer: { type: String, optional: true },
    employerAddress: { type: String, optional: true },
    positionHeld: { type: String, optional: true },
    appointmentDate: { type: Date, optional: true },
    supportingParticulars: { type: String, optional: true },

    financialMisconduct: {type: String, optional: true},
    financialMisconductDetails: { type: String, optional: true},
    courtConviction: {type: String, optional: true},
    courtConvictionDetails: { type: String, optional: true},
    bankruptcy: {type: String, optional: true},
    bankruptcyDetails: { type: String, optional: true},

    previousAnanApplication: { type: String, optional: true },
    previousAnanApplicationDetails: { type: String, optional: true },
});

export const UserPaymentSchema = new SimpleSchema({
    _id: {type: String, optional: true},
    custReference: {type: String, optional: true}, //POINTS TO THE _ID OF THE USER

    isPaymentMade: {type: Boolean, optional: true},
    paymentReference: {type: String, optional: true},
    paymentDate: {type: String, optional: true},
    //paymentDate: {type: Date, optional: true},
    amount: {type: Number, optional: true},
});

export const UserPaymentReferenceSchema = new SimpleSchema({
    //_id: {type: String, optional: true},

    // MerchantID: {type: String, optional: true},
    // API_KEY: {type: String, optional: true},
    // concatString: {type: String, optional: true},
    PaymentReference: {type: String, optional: true},
    PaymentDate: {type: String, optional: true},
    CustReference: {type: String, optional: true},
    CustomerName: {type: String, optional: true},
    CustomerEmail: {type: String, optional: true},
    CustomerPhoneNo: {type: String, optional: true},
    amount: {type: Number, optional: true},
    TotalAmount: {type: Number, optional: true},
    product: {type: Object, optional: true},
    'product.ItemName': {type: String, optional: true},
    'product.ItemCode': {type: String, optional: true},
    'product.ItemAmount': {type: Number, optional: true},
    products: {type: Array, optional: true},
    'products.$': {type: Object, optional: true},
    'products.$.ItemName': {type: String, optional: true},
    'products.$.ItemCode': {type: String, optional: true},
    'products.$.ItemAmount': {type: Number, optional: true},
});

export const DeletedApplicationSchema = new SimpleSchema({
    _id: {type: String, optional: true},
    userId: {type: String, optional: true},
    applicationStarted: {type: Date, optional: true},
    applicationDeleted: {type: Date, optional: true},
    email: {type: String},
    firstName: {type: String},
    middleName: {type: String, optional: true},
    lastName: {type: String},
});

export const UploadStatisticsSchema = new SimpleSchema({
    _id: {type: String, optional: true},
    staffId: {type: String},
    memberId: {type: String},
    memberApplicationId: {type: String},
    memberName: {type: String},
    dateUploaded: {type: Date, optional: true},
});

const ApprovalData = new SimpleSchema({
    dateApproved: { type: Date, optional: true },
    approvedBy: { type: String, optional: true },  //id of staff who approved it
    isApproved: { type: Boolean, optional: true }
});

export const ApplicantApprovalsSchema = new SimpleSchema({
    _id: { type: String, optional: true },
    applicantId: { type: String },
    exam: { type: ApprovalData },
    audit: { type: ApprovalData },
    membership: { type: ApprovalData },
    registry: { type: ApprovalData }
});

const CommentData = new SimpleSchema({
    dateCommented: { type: Date, optional: true },
    staffId: { type: String, optional: true },  //id of staff who approved it
    comment: { type: String, optional: true }
});

export const ApprovalCommentsSchema = new SimpleSchema({
    _id: { type: String, optional: true },
    applicantId: { type: String },
    exam: { type: CommentData },
    audit: { type: CommentData },
    membership: { type: CommentData },
    registry: { type: CommentData }
});

export const AuditTrailSchema = new SimpleSchema({
    _id: {type: String, optional: true},
    staffId: {type: String},
    staffName: {type: String},
    actionType: {type: String},
    actionStatus: {type: String}, //SUCCEEDED, FAILED
    actionDetails: {type: String},
    datePerformed: {type: Date, optional: true},
});

export const MemberRecordsUpdateSchema = new SimpleSchema({
    _id: {type: String, optional: true},
    userId: {type: String, optional: true},
    ananNumber: {type: String, optional: true},
    bio: {type: Object, optional: true},
    updateStatus: {type: Boolean, optional: true},
});


//define collections, attach schemas and export them
export const UserApplication = new Mongo.Collection('UserApplication');
UserApplication.schema = UserApplicationSchema;

export const UserPayment = new Mongo.Collection('UserPayment');
UserPayment.schema = UserPaymentSchema

export const UploadStatistics = new Mongo.Collection('UploadStatistics');
UploadStatistics.schema = UploadStatisticsSchema;

export const ApplicantApprovals = new Mongo.Collection('ApplicantApprovals');
ApplicantApprovals.schema = ApplicantApprovalsSchema;

export const ApprovalComments = new Mongo.Collection('ApprovalComments');
ApprovalComments.schema = ApprovalCommentsSchema;

export const AuditTrail = new Mongo.Collection('AuditTrail');
AuditTrail.schema = AuditTrailSchema;

export const LoginAuditTrail = new Mongo.Collection('LoginAuditTrail');
LoginAuditTrail.schema = AuditTrailSchema;

export const SubmissionAuditTrail = new Mongo.Collection('SubmissionAuditTrail');
SubmissionAuditTrail.schema = AuditTrailSchema;

export const DeletedApplication = new Mongo.Collection('DeletedApplication');
DeletedApplication.schema = DeletedApplicationSchema;

export const MemberRecordsUpdate = new Mongo.Collection('MemberRecordsUpdate');
MemberRecordsUpdate.schema = MemberRecordsUpdateSchema;
//DOCUMENTS
// export const UserDocuments = new FilesCollection({
//     // storagePath: Meteor.absolutePath + '/data/files',
//     // permissions: 0o777,
//     // parentDirPermissions: 0o777,
//     collectionName: 'UserDocuments',
//     allowClientCode: false, // Disallow remove files from Client
//     onBeforeUpload: function (file) {
//         // Allow upload files under 1MB, and only in png/jpg/jpeg formats
//         if (file.size <= 1048576 && /pdf|png|jpg|jpeg/i.test(file.extension)) {
//             return true;
//         } else {
//             return 'Please upload image, with size equal or less than 1MB';
//         }
//     }
// });
// UserDocuments.collection.attachSchema(new SimpleSchema(UserDocuments.schema));