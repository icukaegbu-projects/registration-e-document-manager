/**
 * Created by ikedi on 14/02/2017.
 */
require('isomorphic-fetch');
import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Dropbox } from 'dropbox';
import { HTTP } from 'meteor/http';

import { UserApplication, UserPayment } from './collections';
import { recordAuditTrail } from './methods';
import actionTypes from './actionTypes';

const callService = (type, url, options) => new Promise((resolve, reject) => {
    if(Meteor.isServer){
        HTTP.call(type, url, options, (error, result) => {
            if(error) {
                reject(error);
            } else {
                resolve(result);
            }
        });
    }
});

const DROPBOX_TOKEN = Meteor.settings.public.DROP_BOX_TOKEN;
//const DROPBOX_TOKEN = Meteor.settings.public.DROP_BOX_IJ;

const DROPBOX_INSTANCE = new Dropbox({
    accessToken: DROPBOX_TOKEN
});

const DB_CONSTANTS = {
    LIST_USER_DOCUMENTS: "LIST_USER_DOCUMENTS",
    LIST_USER_SHARED_LINKS: "LIST_USER_SHARED_LINKS",
    GET_USER_DOCUMENTS: "GET_USER_DOCUMENTS",
    GET_USER_DOCUMENTS_PREVIEW: "GET_USER_DOCUMENTS_PREVIEW",
    GET_USER_DOCUMENTS_LINK: "GET_USER_DOCUMENTS_LINK",
    GET_SHARED_LINK_FILE: "GET_SHARED_LINK_FILE",
    CREATE_SHARED_LINK: "CREATE_SHARED_LINK",
    CREATE_USER_FOLDER: "CREATE_USER_FOLDER",
    MOVE_USER_FOLDER: "MOVE_USER_FOLDER",
    UPLOAD_USER_FILE: "UPLOAD_USER_FILE",
    DELETE_USER_FILE: "DELETE_USER_FILE"
};

const DB_LINKS = {
    DELETE: "https://api.dropboxapi.com/2/files/delete",
    UPLOAD: "https://content.dropboxapi.com/2/files/upload",
    CREATE: "",
    DOWNLOAD: "https://content.dropboxapi.com/2/files/download",
    TOKEN: `Bearer ${DROPBOX_TOKEN}`,
}

const dropboxPromise = ({ option, path, url = "", contents = null }) => new Promise((resolve, reject) => {
    if(Meteor.isServer){
        switch(option) {
            case DB_CONSTANTS.LIST_USER_DOCUMENTS:
                //console.log('DB_CONSTANTS.LIST_USER_DOCUMENTS')
                DROPBOX_INSTANCE.filesListFolder( { path: path })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.GET_USER_DOCUMENTS:
                //console.log('DB_CONSTANTS.GET_USER_DOCUMENTS')
                DROPBOX_INSTANCE.filesDownload( { path: path })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.GET_USER_DOCUMENTS_PREVIEW:
                //console.log('DB_CONSTANTS.GET_USER_DOCUMENTS_PREVIEW')
                DROPBOX_INSTANCE.filesGetPreview( { path: path })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.GET_USER_DOCUMENTS_LINK:
                //console.log('DB_CONSTANTS.GET_USER_DOCUMENTS_LINK')
                DROPBOX_INSTANCE.filesGetTemporaryLink({ path: path })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.CREATE_SHARED_LINK:
                //console.log('DB_CONSTANTS.CREATE_SHARED_LINK')
                DROPBOX_INSTANCE.sharingCreateSharedLinkWithSettings({ path: path })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.GET_SHARED_LINK_FILE:
                //console.log('DB_CONSTANTS.GET_SHARED_LINK_FILE')
                DROPBOX_INSTANCE.sharingGetSharedLinkFile({ url: url, path: path })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.LIST_USER_SHARED_LINKS:
                //console.log('DB_CONSTANTS.LIST_USER_SHARED_LINKS')
                DROPBOX_INSTANCE.sharingListSharedLinks({ path: path })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.CREATE_USER_FOLDER:
                //console.log('DB_CONSTANTS.CREATE_USER_FOLDER')
                DROPBOX_INSTANCE.filesCreateFolder( { path: path })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.MOVE_USER_FOLDER:
                //console.log('DB_CONSTANTS.MOVE_USER_FOLDER')
                DROPBOX_INSTANCE.filesMove({ from_path: path, to_path: contents })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.DELETE_USER_FILE:
                //console.log('DB_CONSTANTS.DELETE_USER_FILE')
                DROPBOX_INSTANCE.filesDelete({ path: path })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
            case DB_CONSTANTS.UPLOAD_USER_FILE:
                //console.log('DB_CONSTANTS.UPLOAD_USER_FILE')
                DROPBOX_INSTANCE.filesUpload({ path: path, contents: contents })
                    .then((response) => {
                        resolve(response);
                    }).catch((error) => {
                    reject(error)
                });
                break;
        }

    }
});

export const createUserFolder_V2 = new ValidatedMethod({
    name: 'create.folder2',
    validate: new SimpleSchema({ folderName: { type: String }}).validator(),
    run({ folderName }) {
        let path = folderName === "" ? "" : `/${folderName}`;
        return dropboxPromise({ option: DB_CONSTANTS.CREATE_USER_FOLDER, path: path })
            .then((result) => {
                console.log(result)
                return result;
            }).catch((error) => {
                console.log(error)
                throw new Meteor.Error(checkError(error.status), "Error");
            })
    }
});

export const moveUserFolder_V2 = new ValidatedMethod({
    name: 'move.folder2',
    validate: new SimpleSchema({ to_path: { type: String }, from_path: { type: String }}).validator(),
    run({ to_path, from_path }) {
        return dropboxPromise({ option: DB_CONSTANTS.MOVE_USER_FOLDER, path: from_path, contents: to_path })
            .then((result) => {
                console.log(result)
                return result;
            }).catch((error) => {
                console.log(error)
                throw new Meteor.Error(checkError(error.status), "Error");
            })
    }
});

export const updateUserDocumentInfo_V2 = new ValidatedMethod({
    name: 'upload.file2',
    validate: new SimpleSchema({
        path: { type: String },
        userId: { type: String }
    }).validator(),
    run({ path, userId }) {
        //make a call to UserApplication and update the record to reflect the uploaded file
        //nameFormat === contents.name
        let ua = UserApplication.findOne({ userId: userId });
        console.log(ua)

        let arr = path.split('/'),
            pathName = arr[arr.length - 1];

        //find the file that matches and set its path and isUploaded
        let idx = ua.files.findIndex((file) => file.nameFormat === pathName );

        ua.files[idx].isUploaded = true;
        ua.files[idx].dropboxPath = path;
        const currUpload = ua.filesUploaded;
        const currOut = ua.filesOutstanding;

        let success = UserApplication.update({ userId: userId },
            { $set: {
                "files": ua.files,
                "filesUploaded": currUpload + 1,
                "filesOutstanding": currOut - 1
              }
            });

        //FILE SUCCESSFULLY CREATED, CREATE A PERMANENT SHARED LINK FOR IT
        // return dropboxPromise({ option: DB_CONSTANTS.CREATE_SHARED_LINK, path: path })
        //     .then((result2) => {
        //         console.log(result2)
        //         console.log("shared link created")
        //
        //         return result2
        //         //files.push(result)
        //     }).catch((error) => {
        //         console.log(error)
        //         throw new Meteor.Error(checkError(error.status), "Error");
        //     });
    }
});

export const deleteUserDocument_V2 = new ValidatedMethod({
    name: 'delete.file2',
    validate: new SimpleSchema({
        path: { type: String },
        userId: { type: String }
    }).validator(),
    run({ path, userId }) {

        return dropboxPromise({ option: DB_CONSTANTS.DELETE_USER_FILE, path: path })
            .then(Meteor.bindEnvironment((result) => {
                //make a call to UserApplication and update the record
                //to reflect the uploaded file
                //nameFormat === contents.name
                let ua = UserApplication.findOne({ userId: userId });

                let arr = path.split('/'),
                    pathName = arr[arr.length - 1];

                //find the file that matches and set its path and isUploaded
                let idx = ua.files.findIndex((file) => file.nameFormat === pathName );

                ua.files[idx].isUploaded = false;
                ua.files[idx].dropboxPath = "";
                const currUpload = ua.filesUploaded;
                const currOut = ua.filesOutstanding;

                let success = UserApplication.update({ userId: userId },
                    { $set: {
                        "files": ua.files,
                        "filesUploaded": currUpload - 1,
                        "filesOutstanding": currOut + 1
                      }
                    });

                return result;
            })).catch((error) => {
                throw new Meteor.Error(checkError(error.status), "Error");
            });
    }
});

export const deleteUser_V2 = new ValidatedMethod({
    name: 'delete.user2',
    validate: new SimpleSchema({
        path: { type: String },
        userId: { type: String }
    }).validator(),
    run({ path, userId }) {
        let staffName = '', applicantName = '';

        if(this.userId) {
            staffName = Meteor.users.findOne(this.userId).username;
        }else{
            //APPLICANT IS DELETING HIS OWN ACCOUNT
            staffName = Meteor.users.findOne(userId).username;
        }
        applicantName = Meteor.users.findOne(userId).username;

        return dropboxPromise({ option: DB_CONSTANTS.DELETE_USER_FILE, path: path })
            .then(Meteor.bindEnvironment((result) => {
                if (!Meteor.isServer) return;
                let ua = null, u = null, up = null;

                try {
                    //Meteor.users.remove(this.userId);
                    //delete the user from UserApplication and Meteor.users
                    up = UserPayment.remove({ custReference: userId })
                    ua = UserApplication.remove({ userId: userId });
                    u = Meteor.users.remove({ _id: userId });

                    if(u && ua && up) {
                        recordAuditTrail({
                            staffId: this.userId || `ACCOUNT DELETED BY APPLICANT: ${userId}`,
                            staffName,
                            actionType: actionTypes.DELETE_APPLICANT,
                            actionStatus: actionTypes.SUCCESS, //SET IF ACTION SUCCEEDS OR FAILS
                            actionDetails: `${staffName} SUCCESSFULLY DELETED APPLICANT ${applicantName}`
                        });
                    }

                } catch (e) {
                    // handle this however you want
                    recordAuditTrail({
                        staffId: this.userId,
                        staffName,
                        actionType: actionTypes.DELETE_APPLICANT,
                        actionStatus: actionTypes.FAILURE, //SET IF ACTION SUCCEEDS OR FAILS
                        actionDetails: `${staffName} COULD NOT DELETE APPLICANT ${applicantName}`
                    });

                    throw new Meteor.Error('self-delete', 'Failed to remove yourself');
                }

                return {
                    u,
                    ua,
                    result
                };
            })).catch((error) => {
                console.log(error)
                recordAuditTrail({
                    staffId: this.userId || `ACCOUNT DELETED BY APPLICANT: ${userId}`,
                    staffName,
                    actionType: actionTypes.DELETE_APPLICANT,
                    actionStatus: actionTypes.FAILURE, //SET IF ACTION SUCCEEDS OR FAILS
                    actionDetails: `${staffName} COULD NOT DELETE APPLICANT ${applicantName}: ${checkError(error.status)}`
                });

                throw new Meteor.Error(checkError(error.status), "Error");
            });
    }
});

export const getUserDocumentsList_V2 = new ValidatedMethod({
    name: 'get.documents.list2',
    validate: new SimpleSchema({ folderName: { type: String }}).validator(),
    run(options) {
        let path = options.folderName === "" ? "" : `/${options.folderName}`;

        return dropboxPromise({ option: DB_CONSTANTS.LIST_USER_DOCUMENTS, path: path })
            .then((result) => {
                //return result;
                //filter the array and remove Passport.jpg
                let arr = result.entries.filter((file) => file.name !== "Passport.jpg")
                return arr;
            }).catch((error) => {
                throw new Meteor.Error(checkError(error.status), "Error");
            });
    }
});

export const getUserDocuments_V2 = new ValidatedMethod({
    name: 'get.single.documents',
    validate: new SimpleSchema({ folderName: { type: String }}).validator(),
    run(options) {
        let reader = null;

        return dropboxPromise({ option: DB_CONSTANTS.GET_USER_DOCUMENTS, path: options.folderName })
            .then((result) => {
                return result;
            }).catch((error) => {
                console.log(error)
                throw new Meteor.Error(checkError(error.status), "Error");
            });
    }
});

const checkError = (code) => {
    switch(code) {
        case 409:
            return "File/Folder does not exist";
            break;
        case undefined:
            return "Offline/Cannot connect to server";
            break;
        default:
            return "Error";
            break;
    }
}