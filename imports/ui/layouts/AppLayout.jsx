import React, { Component } from 'react';
import { Grid, Row, Col, Container } from 'reactstrap';

import AppHeader from '../components/AppHeader'

let imgUrl = 'images/back5.jpg';

const styles = {
    root: {
        backgroundImage: 'url(' + imgUrl + ')',
        backgroundSize: 'cover',
        height: 'auto',
        minHeight: '150vh',
        width: '100%',
    },
    container: {
        zIndex: 1,
        marginTop: 50,
    }
};

class AppLayout extends Component {
    constructor(props) {
        super(props);

        this.renderWithMenu = this.renderWithMenu.bind(this);
        this.renderWithoutMenu = this.renderWithoutMenu.bind(this);
    }

    renderWithMenu(){
        return (
            <Container style={styles.root}>
                <Row>
                    <AppHeader/>
                </Row>
                <Col sm={12} md={12}>
                    {this.props.content}
                </Col>
            </Container>
        );
    }

    renderWithoutMenu(){
        return (
            <div style={styles.root}>
                <Row type="flex" justify="center" align="middle">
                    <Col sm={12} md={12}>
                        {this.props.content}
                    </Col>
                </Row>
            </div>
        );
    }

    render() {
        return (
            <div>
                {
                    this.props.renderWithMenu ?
                        this.renderWithMenu() :
                        this.renderWithoutMenu()
                }
            </div>
        );
    }
}

export default AppLayout;