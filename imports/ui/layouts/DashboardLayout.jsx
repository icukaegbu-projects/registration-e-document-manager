import React from 'react';
import { Grid } from 'reactstrap';

const styles = {
    root: {
        width: '100%',
        overflow: 'auto'
    },
    container: {
        zIndex: 1,
        marginTop: 50,
    }
};

export default DashboardLayout = (props) => {
    return (
        <div style={styles.root}>
            {props.content}
        </div>
    );
}