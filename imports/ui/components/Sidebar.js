import React from 'react';
import { Nav, NavLink, NavItem } from 'reactstrap';
import { adminDashboard, applicants, archivedMembers, viewMembers, staff,
    uploadCSV, profile, auditTrail } from '../components/admin/MenuActions';

const SidebarLink = ({ label, onClick }) => {
    return (
        <NavItem
            style={{
                borderBottom: '1px solid white',
                paddingBottom: '6px'
            }}
        >
            <NavLink href="#" onClick={onClick}>{label}</NavLink>
        </NavItem>
    )
};

export default Sidebar = (props) => {
    let menu = [];
    const userRole = Roles.getRolesForUser(Meteor.userId());

    switch(userRole.toString()) {
        case "admin":
            menu =  [
                { menu: 'Dashboard', onClick: adminDashboard },
                { menu: 'Upload Archived Members', onClick: archivedMembers },
                { menu: 'View Members', onClick: viewMembers },
                { menu: 'Applicants', onClick: applicants },
                { menu: 'View Staff', onClick: staff },
                { menu: 'Upload CSV', onClick: uploadCSV },
                { menu: 'Audit Trail', onClick: auditTrail },
                { menu: 'Profile', onClick: profile }
            ];
            break;
        case "Supervisor":
            menu =  [
                { menu: 'Dashboard', onClick: adminDashboard },
                { menu: 'Upload Archived Members', onClick: archivedMembers },
                { menu: 'View Members', onClick: viewMembers },
                { menu: 'View Staff Work', onClick: () => alert('Pending') },
                { menu: 'Profile', onClick: profile }
            ];
            break;
        case "DataCapture":
            menu =  [
                { menu: 'Dashboard', onClick: adminDashboard },
                { menu: 'Upload Archived Members', onClick: archivedMembers },
                { menu: 'Profile', onClick: profile }
            ];
            break;
        case "Membership":
            menu =  [
                { menu: 'Dashboard', onClick: adminDashboard },
                { menu: 'View Members', onClick: viewMembers },
                { menu: 'Applicants', onClick: applicants },
                { menu: 'Profile', onClick: profile }
            ];
            break;
        case "Audit":
        case "Exams":
        case "Registry":
            menu =  [
                { menu: 'Dashboard', onClick: adminDashboard },
                { menu: 'Applicants', onClick: applicants },
                { menu: 'Profile', onClick: profile }
            ];
            break;
    }

    return (
        <div className="sidebar" data-background-color="brown" data-active-color="danger">
            <div className="logo">
                <span className="simple-text">
                    ANAN
                </span>
            </div>

            <div className="sidebar-wrapper">
                <div className="user">
                    <div className="photo">
                        <img src="images/AnanlogoHead.jpg" />
                    </div>
                    <div className="info">
                        Welcome
                    </div>
                </div>
                <Nav>
                    {
                        menu ?
                            menu.map(item => {
                                return <SidebarLink key={item.menu} label={item.menu} onClick={item.onClick}/>
                            }) : <div></div>
                    }
                </Nav>
            </div>
        </div>
    );
};