import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

class DashboardNavbar extends Component {
    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
        this.renderEdit = this.renderEdit.bind(this);
    }

    logout() {
        Meteor.logout((error) => {
            if (error) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error.reason, 'Logout Error');
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.info('Logout Successful', 'Success');

                this.setState({ loggedIn: false });

                FlowRouter.go('index');
            }
        })
    }

    renderEdit(){
        return (
            this.props.isSubmitted ? '' :
                <span>
                    <NavItem>
                        <NavLink href="#" className="btn btn-danger" onClick={this.props.onDelete}>Delete</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#" className="btn btn-primary" onClick={this.props.onEditChange}>Edit</NavLink>
                    </NavItem>
                </span>
        )
    }

    render() {
        return (
            <Navbar className="navbar navbar-default">
                {
                    this.props.user && this.props.user.username ?
                        <NavbarBrand href="#">
                            Welcome, {`${this.props.user.username.toUpperCase()}  [${this.props.user.emails[0].address}]`}
                        </NavbarBrand>
                        :
                        <NavbarBrand href="#">
                            Welcome
                        </NavbarBrand>
                }

                <Nav navbar className="float-xs-right">
                    {
                        this.props.isEditing
                        ?
                            <div style={{paddingTop: 15}}>
                                <NavItem>
                                    <NavLink href="#" className="btn btn-primary" onClick={this.props.onEditChange}>Cancel</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="#" className="btn btn-default" onClick={this.props.handleApplicationSave}>Save</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="#" className="btn btn-danger" onClick={this.props.handleApplicationSubmit}>Submit</NavLink>
                                </NavItem>
                            </div>
                        :
                            <div style={{paddingTop: 15}}>
                                {this.renderEdit()}
                                <NavItem style={{paddingLeft: 15}}>
                                    <NavLink href="#" className="btn btn-default" onClick={this.logout}>Logout</NavLink>
                                </NavItem>
                            </div>
                    }
                </Nav>
            </Navbar>
        );
    }
}

DashboardNavbar.propTypes = {
    user: PropTypes.object.isRequired
};

export default DashboardNavbar;