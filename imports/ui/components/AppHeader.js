import React from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

export default AppHeader = (props) => {
    return (
        <Navbar className="navbar-ct-transparent" >
            <NavbarBrand href="/">
                <img
                    src="images/Ananlogo.jpg"
                    alt=""
                    style={{
                        display: 'block',
                        width: '140px',
                        height: '90px',
                        paddingBottom: '20px',
                        paddingTop: '0'
                    }}
                />
            </NavbarBrand>
            <Nav className="float-xs-right" navbar>
                <NavItem>
                    <NavLink href="http://www.anan.org.ng" className="btn btn-default">Return to Website</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href={FlowRouter.path('register')} className="btn btn-default">Register</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href={FlowRouter.path('login')} className="btn btn-default">Login</NavLink>
                </NavItem>
            </Nav>
        </Navbar>
    );
}