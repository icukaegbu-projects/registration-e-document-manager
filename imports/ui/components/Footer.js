import React from 'react';

export default Footer = (props) => {
    return (
        <footer
            className="footer"
            style={{
                position: 'absolute',
                right: 0,
                bottom: 0,
                left: 0,
                height: 70
            }}>
            <div className="container-fluid">
                <div className="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, Copyright <a href="#">ANAN</a>
                </div>
            </div>
        </footer>
    );
};

// return (
//     <div className="demo-footer text-center">
//         <h6>&copy; 2016, made with <i className="fa fa-heart heart"></i> by Creative Tim</h6>
//     </div>
// );