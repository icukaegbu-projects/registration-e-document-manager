/**
 * Created by ikedi on 18/01/2017.
 */
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

import AdminDashboard from './AdminDashboard';

export default Admin = createContainer(({ params }) => {

    const usersHandle = Meteor.subscribe('users.one', Meteor.userId());
    const loading = !usersHandle.ready();
    const user = Meteor.users.findOne({
        _id: Meteor.userId()
    }, {
        fields: {
            "createdAt": 0
        }
    });

    return {
        loading,
        user
    };
}, AdminDashboard);