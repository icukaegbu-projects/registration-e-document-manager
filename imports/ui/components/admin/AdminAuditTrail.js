/**
 * Created by ic on 7/18/17.
 */
import React, { Component } from 'react';
import { Table, Container, Row, Col, Card } from 'reactstrap';
import { Tabs, Tab } from 'react-bootstrap-tabs';
import moment from 'moment';

import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';

import Sidebar from '../Sidebar';
import SearchInput from '../lib/SearchInput';
import AdminDashboardNavbar from './AdminDashboardNavbar';
import { AuditTrail, LoginAuditTrail, SubmissionAuditTrail } from '../../../api/users/collections';

class AdminAuditTrailForm extends Component {
    renderInterface({ data, title, loading, isSubmission = false }) {
        return (
            <Tab label={title}>
                <Card block>
                    <Row>
                        <Col md={12} sm={12}>
                            <SearchInput
                                onClick={this.props.onSearchClick}
                                showAnanNumber={false}
                            />
                        </Col>
                    </Row>
                    <Row style={{padding: 30}}>
                        <Table striped size="sm" style={{
                            borderLeft: '1px solid grey',
                            borderRight: '1px solid grey',
                            borderBottom: '1px solid grey'
                        }}>
                            <thead>
                            <tr>
                                <th>{isSubmission ? 'STUDENT' : 'STAFF'}</th>
                                <th>ACTION</th>
                                <th>STATUS</th>
                                <th>DETAILS</th>
                                <th>DATE</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                !loading && data ?
                                    data.map((trail) => {
                                        return (
                                            <tr key={trail._id}>
                                                <td>{isSubmission ? trail.applicantName: trail.staffName}</td>
                                                <td>{trail.actionType}</td>
                                                <td>{trail.actionStatus}</td>
                                                <td>{trail.actionDetails}</td>
                                                <td>{moment(trail.datePerformed).format('DD/MM/YYYY HH:MM')}</td>
                                            </tr>
                                        );
                                    }) : <div><h4>No Audit Records</h4></div>
                            }
                            </tbody>
                        </Table>
                    </Row>
                    {/*<Row>*/}
                        {/*{*/}
                            {/*users.length === 0 ? null :*/}
                                {/*<Col md={{size: 2, push: 3, pull: 3, offset: 1}}>*/}
                                    {/*<Button className="btn btn-info" onClick={this.props.loadMore}>*/}
                                        {/*<span style={{ paddingRight: 10 }}>Load More</span>*/}
                                        {/*<i className="fa fa-arrow-circle-down"></i>*/}
                                    {/*</Button>*/}
                                {/*</Col>*/}
                        {/*}*/}
                    {/*</Row>*/}
                </Card>
            </Tab>
        )
    }
    render() {
        let { auditTrail, loginAuditTrail, submissionAuditTrail, loading } = this.props;
        return (
            <div className="wrapper">
                <Sidebar />

                <Container fluid={true} className="main-panel">
                    <Row>
                        <AdminDashboardNavbar showAdminMenu={false} />
                    </Row>
                    <div className="content" type="flex">
                        <Tabs>
                            {this.renderInterface({ data: auditTrail, title: "Audit Trail", loading })}
                            {this.renderInterface({ data: loginAuditTrail, title: "Login Audit Trail", loading })}
                            {this.renderInterface({ data: submissionAuditTrail, title: "Submission Audit Trail", loading, isSubmission: true })}
                        </Tabs>
                    </div>
                </Container>
            </div>
        )
    }
}

export default AdminAuditTrail = createContainer(({ params }) => {
    const auditTrailHandle = Meteor.subscribe('audit.trail');
    const loginAuditTrailHandle = Meteor.subscribe('login.audit.trail');
    const submissionAuditTrailHandle = Meteor.subscribe('submission.audit.trail');
    const loading = !auditTrailHandle.ready() && !loginAuditTrailHandle.ready() &&
                    !submissionAuditTrailHandle.ready();

    const auditTrail = AuditTrail.find().fetch();
    const loginAuditTrail = LoginAuditTrail.find().fetch();
    const submissionAuditTrail = SubmissionAuditTrail.find().fetch();

    return {
        loading,
        auditTrail,
        loginAuditTrail,
        submissionAuditTrail
    };
}, AdminAuditTrailForm);