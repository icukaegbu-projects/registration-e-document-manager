/**
 * Created by ikedi on 23/03/2017.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col, Button } from 'reactstrap';

import Sidebar from '../Sidebar';
import AdminDashboardNavbar from './AdminDashboardNavbar';
import RenderPDFDocuments from './RenderPDFDocuments';
import { ProfileUser3, ProfileUser2 } from '../user/ProfileFormHelpers';
import SearchInput from '../lib/SearchInput';

import { getUserDocumentsList_V2 } from '../../../api/users/DropboxMethods_V2';

import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

class ViewMembersForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            selectedUser: null,
            loading: false,
            userFileList: [],
            singleDoc: null,
            currentFileName: ''
        };

        this.showDocuments = this.showDocuments.bind(this);
        this.showSingleDocument = this.showSingleDocument.bind(this);
        this.saveDocument = this.saveDocument.bind(this);
    }

    showDocuments(user) {
        //THE USER RETURNED HERE IS AN INSTANCE OF USERAPPLICATION: IT HAS ANANNUMBER
        //BUT NOT FIRSTNAME ETC.
        //TO FIND THE FULL USER, ITERATE THROUGH THE THIS.PROPS.USER PROPERTY
        this.setState({
            visible: !this.state.visible,
            selectedUser: user
        });

        if(!user) {
            this.setState({
                userFileList: [],
                singleDoc: null,
                currentFileName: ''
            });
            return;
        }else {
            //generate user folder name if he doesnt yet have an anan number
            //if bio object has an ananNumber field, use it else use
            let path = "";

            if(user.bio.ananNumber && user.bio.ananNumber != "") {
                console.log("anan: "+user.bio.ananNumber)
                path = user.bio.ananNumber;
            }else{
                path = user.bio.lastName.toUpperCase()+"-"+user.bio.firstName.toUpperCase()+"-"+user.username.toUpperCase();
            }
            console.log(path);

            this.setState({
                loading: true
            });

            getUserDocumentsList_V2.call({ folderName: path }, (err, result) => {
                if(err) {
                    this.setState({
                        loading: false
                    });
                }else{
                    //let files = result.entries.map((file) => {
                    let files = result.map((file) => {
                        return {
                            name: file.name,
                            path_display: file.path_display,
                            path_lower: file.path_lower,
                            size: file.size,
                            id: file.id
                        }
                    });

                    this.setState({
                        loading: false,
                        userFileList: files
                    });
                }
            });
        }
    }

    showSingleDocument(path) {
        console.log(path)
        let xhr = new XMLHttpRequest();
        xhr.responseType = 'arraybuffer';

        let arr = path.split("/"),
            fileName = arr[arr.length - 1].split("_").join(" ").toUpperCase(),
            name = path.split("/").join("_");

        this.setState({
            loading: true
        });

        xhr.onload = () => {
            if (xhr.status === 200) {
                let blob = new Blob([xhr.response], {type: 'application/pdf'});

                this.setState({
                    singleDoc: blob,
                    loading: false,
                    currentFileName: fileName
                });
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('File downloaded successfully', 'File Downloaded');
            }
            else {
                var errorMessage = xhr.response || 'Unable to download file';
                // Upload failed. Do something here with the error.
                this.setState({
                    loading: false
                });

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(errorMessage, 'Error in downloading file');
            }
        };

        xhr.open('POST', 'https://content.dropboxapi.com/2/files/download');
        //xhr.setRequestHeader('Authorization', 'Bearer ' + Meteor.settings.public.DROP_BOX_IJ);
        xhr.setRequestHeader('Authorization', 'Bearer ' + Meteor.settings.public.DROP_BOX_TOKEN);
        xhr.setRequestHeader('Dropbox-API-Arg', JSON.stringify({
            path: path
        }));
        xhr.send();
    }

    saveDocument(user) {
        this.setState({
            visible: false
        })
    }

    render() {
        const { membersAll, loading } = this.props;
        return (
            <div className="wrapper">
                <Sidebar />

                <Container fluid={true} className="main-panel">
                    <Row>
                        <AdminDashboardNavbar showAdminMenu={false} />
                    </Row>
                    {
                        this.state.selectedUser ?
                            <RenderPDFDocuments
                                user={this.state.selectedUser}
                                showDocuments={this.showDocuments}
                                showSingleDocument={this.showSingleDocument}
                                loading={this.state.loading}
                                userFileList={this.state.userFileList}
                                doc={this.state.singleDoc}
                                fileName={this.state.currentFileName}
                                staffId={this.props.loggedInStaff._id}
                                staffRole={Roles.getRolesForUser(this.props.loggedInStaff._id).toString() || this.props.loggedInStaff.bio.role}
                            />
                            :
                            <div className="content" type="flex">
                                <Row>
                                    <Col md={12} sm={12}>
                                        <SearchInput onClick={this.props.onSearchClick} />
                                    </Col>
                                </Row>
                                <Row>
                                    {
                                        !loading && membersAll ?
                                            membersAll.map(user => {
                                                return (
                                                    <Col  key={user._id} md={4} sm={6}>
                                                        <ProfileUser3
                                                            user={user}
                                                            profilePicture={true}
                                                        >
                                                            {
                                                                <div
                                                                    style={{
                                                                        background: 'grey',
                                                                        color: 'white',
                                                                        fontWeight: 600,
                                                                        padding: 10,
                                                                        fontSize: 11
                                                                    }}
                                                                >
                                                                    <div
                                                                        style={{
                                                                            borderBottom: '1px dashed white',
                                                                            paddingBottom: '8px',
                                                                            marginBottom: '8px'
                                                                        }}
                                                                    >
                                                                        Anan#: {user.bio.ananNumber}<br/>
                                                                        Branch: {user.bio.ananBranch}<br/>
                                                                        Membership: {user.bio.membershipStatus}<br/>
                                                                        Phone #: {user.bio.phone}<br/>
                                                                        Address: {user.bio.privateAddress}
                                                                    </div>
                                                                    <button
                                                                        className="btn btn-fill btn-warning"
                                                                        onClick={this.showDocuments.bind(null, user)}
                                                                        disabled={this.state.selectedUser}
                                                                    >
                                                                        <i className="fa fa-eye"></i>  View Docs
                                                                    </button>
                                                                </div>
                                                            }
                                                        </ProfileUser3>
                                                    </Col>
                                                )
                                            })
                                            :
                                            <div>
                                                <h4>No Members</h4>
                                            </div>
                                    }
                                </Row>
                                <Row>
                                    <Col md={{size: 2, push: 4, pull: 4, offset: 1}}>
                                        <div style={{ marginTop: 30, marginBottom: 30 }}>
                                            {
                                                membersAll.length === 0 ?
                                                    <h4>No Members</h4>
                                                    :
                                                    <Button className="btn btn-info" onClick={this.props.loadMore}>
                                                        <span style={{ paddingRight: 10 }}>Load More</span>
                                                        <i className="fa fa-arrow-circle-down"></i>
                                                    </Button>
                                            }
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                    }
                </Container>
            </div>
        )
    }
}

ViewMembersForm.propTypes = {
    membersAll: PropTypes.array.isRequired,
    loggedInStaff: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    onSearchClick: PropTypes.func,
    loadMore: PropTypes.func
};

const membersOptions = new ReactiveVar();
const pageNumber = new ReactiveVar(1);
const RECORDS_PER_PAGE = 10;

const onSearchClick = (fields) => {
    membersOptions.set(fields)
};

const loadMore = () => {
    pageNumber.set(pageNumber.get() + 1);
    let fields = membersOptions.get();
    if(!fields) {
        fields = {};
        fields.numResults = 0;
    }
    fields.numResults = pageNumber.get() * RECORDS_PER_PAGE;
    membersOptions.set(fields);
    // console.log("Fields");
    // console.log(fields);
};

export default ViewMembers = createContainer(({ params }) => {
    const membersHandle = Meteor.subscribe('members.all', membersOptions.get());
    const loading = !membersHandle.ready();

    //const users = Meteor.users.find({username: { $ne: 'admin' }}, { sort: {createdAt: -1}}).fetch();
    const membersAll = Meteor.users.find({
        "bio.membershipStatus": {
            $in: ["CNA", "FCNA"]
        }

        //CHECK IF THE ABOVE WORKS, ELSE REVERT TO THIS
        // username: { $ne: 'admin' },
        // "bio.role": { $nin: ['Admin', 'Supervisor', 'DataCapture',
        //     'Audit', 'Exams', 'Membership', 'Registry']}

        // "bio.role": "Member"
    }, {
        sort: {createdAt: -1}
    }).fetch();

    //return the role of the currently logged in staff
    const loggedInStaff = Meteor.users.findOne({ _id: Meteor.userId() }) || {};

    return {
        loading,
        membersAll,
        loggedInStaff,
        onSearchClick,
        loadMore
    }

}, ViewMembersForm);