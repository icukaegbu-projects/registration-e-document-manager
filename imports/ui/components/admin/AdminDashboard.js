import React, {Component} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import moment from 'moment';

import { Container, Row, Col, Card, CardText, CardBlock, ListGroup, ListGroupItem, Badge } from 'reactstrap';
import { Circle } from 'rc-progress';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

import Sidebar from '../Sidebar';
import AdminDashboardNavbar from './AdminDashboardNavbar';
import Footer from '../Footer';

import { createContainer } from 'meteor/react-meteor-data';
import { JoinClient } from 'meteor-publish-join';

class AdminDashboardForm extends Component {
    constructor(props) {
        super(props);

        this.renderInterface = this.renderInterface.bind(this);
        this.createCard = this.createCard.bind(this);
        this.createColoredCard = this.createColoredCard.bind(this);
        this.createBarChart = this.createBarChart.bind(this);
    }

    createCard(title, number, icon, iconType, loading, backgroundColor = 'white') {
        return (
            <Card  style={{padding: 10, paddingLeft: 20, backgroundColor: backgroundColor}}>
                <CardText tag="div" className="content" >
                    <Row>
                        <Col xs={4} md={4}>
                            <div className={`icon-big icon-${iconType} text-center`}>
                                <i className={`fa fa-${icon}`}></i>
                            </div>
                        </Col>
                        <Col xs={7} md={7}>
                            <div className="numbers">
                                <p>{title.toUpperCase()}</p>
                                <h5>
                                    {
                                        number || number === 0 ?
                                            <ReactCSSTransitionGroup
                                                transitionName="report"
                                                transitionAppear={true}
                                                transitionAppearTimeout={500}
                                                transitionEnterTimeout={500}
                                                transitionLeaveTimeout={300}>
                                                <span>{number}</span>
                                            </ReactCSSTransitionGroup>
                                            :
                                            <i className="fa fa-spinner"></i>
                                    }
                                </h5>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <div>
                            <hr />
                            <div className="stats">
                                <i className="fa fa-recycle"></i> Updated now
                            </div>
                        </div>
                    </Row>
                </CardText>
            </Card>
        )
    }

    createAuditData(data, isApplicationSubmission = false) {
        const list = (
            data && data.length > 0 ?
                data.map(d => {
                    return (
                        <div style={{color: 'white', borderBottom: '1px solid grey', paddingBottom: 2, paddingTop: 3}} key={d._id}>
                            <h6 style={{display:'inline-block', margin: 0}}>
                                { !isApplicationSubmission ? d.staffName : d.applicantName}
                            </h6>
                            <span className="pull-right">{moment(d.datePerformed).format('DD/MM/YYYY HH:SS')}</span>
                        </div>
                    )}
                ) : <div></div>
        );

        return (
            <div>
                {list}
            </div>
        );
    }

    createColoredCard({title, description, data = [], color="blue"}) {
        return (
            <Card
                style={{padding: 10, paddingLeft: 20, height: 370}}
                className="card card-circle-chart"
                data-background="color"
                data-color={color}
            >
                <div className="header text-center">
                    <h4
                        className="title"
                        style={{
                            fontSize: 30,
                            fontWeight: 900
                        }}>
                        {title.toUpperCase()}
                    </h4>
                    <p
                        className="description"
                        style={{fontSize: 20, fontWeight: 500}}
                    >{description}</p>
                </div>
                <CardBlock style={{margin: 0}}>
                    {data}
                </CardBlock>
            </Card>
        );
    }

    createBarChart(data = [], title = "Report", subtitle = "Report") {
        return (
            <Card
                style={{color: '#333', height: 500}}
                className="card card-circle-chart"
                data-background="color"
                data-color="white"
            >
                <div className="header text-center">
                    <h4
                        className="title"
                        style={{
                            fontSize: 30,
                            fontWeight: 600,
                            color: '#333'
                        }}>
                        {title}
                    </h4>
                    <p
                        className="description"
                        style={{fontSize: 20, fontWeight: 500, color: '#333'}}
                    >
                        {subtitle}
                    </p>
                </div>
                <CardText tag="div" className="content">
                    <BarChart width={600} height={300} data={data} margin={{top: 5, right: 30, left: 5, bottom: 5}}>
                        <XAxis dataKey="name" />
                        <YAxis />
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="approved" fill="#8884d8" />
                        <Bar dataKey="pending" fill="#82ca9d" />
                    </BarChart>
                </CardText>
            </Card>
        )
    }

    applicationApprovalsReport() {
        const data = [
            {name: 'Audit', approved: this.props.totalApprovedByAudit, pending: this.props.totalPendingByAudit, amt: this.props.totalApplicants},
            {name: 'Exams', approved: this.props.totalApprovedByExam, pending: this.props.totalPendingByExam, amt: this.props.totalApplicants},
            {name: 'Membership', approved: this.props.totalApprovedByMembership, pending: this.props.totalPendingByRegistry, amt: this.props.totalApplicants},
            {name: 'Registry', approved: this.props.totalApprovedByRegistry, pending: this.props.totalPendingByRegistry, amt: this.props.totalApplicants},
        ];

        return this.createBarChart(data, "Applications Approvals Report", "Applications (2017)");
    }

    renderInterface() {
        const loginArray = this.createAuditData(this.props.recentLogin);
        const submissionArray = this.createAuditData(this.props.recentSubmission, true);

        return (
            <div>
                <Row>
                    <AdminDashboardNavbar
                        user={this.props.user}
                    />
                </Row>

                <Row className="content" style={{padding: 25}}>
                    <h3 style={{paddingLeft: 12, marginTop: 0}}>MEMBERSHIP STATUS REPORT</h3>
                    <Col lg={12} md={12}>
                        <Row>
                            <Col sm={12} md={3}>
                                {this.createCard('Applicants', this.props.totalApplicants, 'users', 'warning', this.props.loading)}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('FCNA', this.props.totalFcna, 'legal', 'info', this.props.loading)}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('CNA', this.props.totalCna, 'sticky-note', 'success', this.props.loading)}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('Staff', this.props.totalStaff, 'user', 'danger', this.props.loading)}
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="content" style={{padding: 25}}>
                    <h3 style={{paddingLeft: 12, marginTop: 0}}>DEMOGRAPHICS REPORT</h3>
                    <Col lg={12} md={12}>
                        <Row>
                            <Col sm={12} md={3}>
                                {this.createCard('Female Applicants', this.props.totalFemaleApplicants, 'vcard', 'warning', this.props.loading, "lightGrey")}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('Male Applicants', this.props.totalMaleApplicants, 'address-book', 'info', this.props.loading, "orange")}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('Female Members', this.props.totalFemaleMembers, 'vcard-o', 'success', this.props.loading, "green")}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('Male Members', this.props.totalMaleMembers, 'address-book-o', 'danger', this.props.loading, "brown")}
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="content" style={{padding: 25}}>
                    <h3 style={{paddingLeft: 12, marginTop: 0}}>APPLICATION REPORT</h3>
                    <Col lg={12} md={12}>
                        <Row>
                            <Col sm={12} md={3}>
                                {this.createCard('Submitted Applications', this.props.totalSubmittedApplications, 'envelope', 'warning', this.props.documentsLoading)}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('Deleted Applications', this.props.totalDeletedApplications, 'exclamation', 'info', this.props.documentsLoading, "pink")}
                            </Col>
                            {/* <Col sm={12} md={3}>
                                {this.createCard('Members with Documents', this.props.totalMembersWithDocuments, 'folder', 'success', this.props.loading)}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('Members without Documents', this.props.totalMembersWithoutDocuments, 'file-o', 'danger', this.props.loading, "purple")}
                            </Col> */}
                        </Row>
                    </Col>
                </Row>
                <Row className="content" style={{padding: 25}}>
                    <h3 style={{paddingLeft: 12, marginTop: 0}}>DOCUMENT REPORT</h3>
                    <Col lg={12} md={12}>
                        <Row>
                            <Col sm={12} md={3}>
                                {this.createCard('Applicants with Documents', this.props.totalApplicantsWithDocuments, 'envelope', 'warning', this.props.documentsLoading, "grey")}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('Applicants without Documents', this.props.totalApplicantsWithoutDocuments, 'exclamation', 'info', this.props.documentsLoading)}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('Members with Documents', this.props.totalMembersWithDocuments, 'folder', 'success', this.props.documentsLoading, "dark-blue")}
                            </Col>
                            <Col sm={12} md={3}>
                                {this.createCard('Members without Documents', this.props.totalMembersWithoutDocuments, 'file-o', 'danger', this.props.documentsLoading)}
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="content" style={{padding: 25}}>
                    <Col lg={12} md={12}>
                        <Row>
                            <Col sm={12} md={6}>
                                { this.props.approvalLoading ? this.applicationApprovalsReport() : null}
                            </Col>
                            <Col sm={12} md={6}>
                                {this.createBarChart()}
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={12} md={6}>
                                {
                                    this.createColoredCard({title:"Login Attempts", description:"Top 10 Recent Logins", data:loginArray})
                                }
                            </Col>
                            <Col sm={12} md={6}>
                                {this.createColoredCard({title:"Applicant Submissions", description:"Top 10 Recent Submissions", data: submissionArray
                                    , color: "orange"})
                                }
                            </Col>
                        </Row>
                        <Row>
                            {/*<Col sm={12} md={6}>*/}
                                {/*{*/}
                                    {/*this.createColoredCard({title:"Analysis", description:"Total Income",*/}
                                    {/*content: <Circle*/}
                                        {/*percent={78}*/}
                                        {/*strokeWidth="6"*/}
                                        {/*strokeColor="#ffffff"*/}
                                        {/*style={{height: 150}} />*/}
                                    {/*, color: "green"})*/}
                                {/*}*/}
                            {/*</Col>*/}
                            {/*<Col sm={12} md={6}>*/}
                                {/*{*/}
                                    {/*this.createColoredCard({title: "2017", description:"Projections for 2016",*/}
                                    {/*content: <Circle*/}
                                        {/*percent={54}*/}
                                        {/*strokeWidth="6"*/}
                                        {/*strokeColor="#ffffff"*/}
                                        {/*style={{height: 150}} />*/}
                                    {/*, color: "brown"})*/}
                                {/*}*/}
                            {/*</Col>*/}
                        </Row>
                    </Col>
                </Row>
            </div>
        )
    }

    render() {
        return (
            <div>
                <div className="wrapper">
                    <Sidebar />

                    <Container fluid={true} className="main-panel">
                        {this.renderInterface()}
                        {/*<Footer />*/}
                    </Container>
                </div>
            </div>
        )
    }
}

export default AdminDashboard = createContainer(({ params }) => {
    const reportsCountHandle = Meteor.subscribe('reports.total');
    const reportsDemographicsHandle = Meteor.subscribe('reports.demographics');
    const reportsApprovalsHandle = Meteor.subscribe('reports.applicant.approvals');
    const reportsDocumentStatusHandle = Meteor.subscribe('reports.document.status');

    const reportsLoginRecent10Handle = Meteor.subscribe('reports.audit');

    const loading = !reportsCountHandle.ready() && !reportsDemographicsHandle.ready(); 
    const documentsLoading = !reportsDocumentStatusHandle.ready();
    const approvalLoading = !reportsApprovalsHandle.ready();
    const loginLoading = !reportsLoginRecent10Handle.ready();

    let totalStaff = JoinClient.get('totalStaff'),
        totalCna =  JoinClient.get('totalCNA'),
        totalFcna = JoinClient.get('totalFCNA'),
        totalApplicants = JoinClient.get('totalApplicants');

    let totalFemaleApplicants = JoinClient.get('totalFemaleApplicants'),
        totalMaleApplicants = JoinClient.get('totalMaleApplicants'),
        totalFemaleMembers = JoinClient.get('totalFemaleMembers'),
        totalMaleMembers = JoinClient.get('totalMaleMembers');

    let totalApprovedByAudit = JoinClient.get('totalApprovedByAudit'),
        totalPendingByAudit = JoinClient.get('totalPendingByAudit'),
        totalApprovedByExam = JoinClient.get('totalApprovedByExam'),
        totalPendingByExam = JoinClient.get('totalPendingByExam'),
        totalApprovedByMembership = JoinClient.get('totalApprovedByMembership'),
        totalPendingByMembership = JoinClient.get('totalPendingByMembership'),
        totalApprovedByRegistry = JoinClient.get('totalApprovedByRegistry'),
        totalPendingByRegistry = JoinClient.get('totalPendingByRegistry');

    let totalDeletedApplications = JoinClient.get('deletedApplications'),
        totalSubmittedApplications = JoinClient.get('submittedApplications'),
        totalMembersWithDocuments = JoinClient.get('membersWithDocuments'),
        totalMembersWithoutDocuments = JoinClient.get('membersWithoutDocuments'),
        totalApplicantsWithDocuments = JoinClient.get('applicantsWithDocuments'),
        totalApplicantsWithoutDocuments = JoinClient.get('applicantsWithoutDocuments');

    let recentLogin = JoinClient.get('recent10Login'),
        recentSubmission = JoinClient.get('recent10ApplicationSubmission');

    return {
        loading,
        approvalLoading,
        loginLoading,
        documentsLoading,
        totalStaff,
        totalCna,
        totalFcna,
        totalApplicants,
        totalFemaleApplicants,
        totalMaleApplicants,
        totalFemaleMembers,
        totalMaleMembers,
        totalApprovedByAudit,
        totalPendingByAudit,
        totalApprovedByExam,
        totalPendingByExam,
        totalApprovedByMembership,
        totalPendingByMembership,
        totalApprovedByRegistry,
        totalPendingByRegistry,
        totalDeletedApplications,
        totalSubmittedApplications,
        totalMembersWithDocuments,
        totalMembersWithoutDocuments,
        totalApplicantsWithDocuments,
        totalApplicantsWithoutDocuments,
        recentLogin,
        recentSubmission
    }
}, AdminDashboardForm);
