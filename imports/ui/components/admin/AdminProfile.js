// /**
//  * Created by ikedi on 14/12/2016.
//  */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col,
    Card, CardTitle, CardBlock } from 'reactstrap';

import Sidebar from '../Sidebar';
import DashboardNavbar from '../DashboardNavbar';
import Footer from '../Footer';

import { ProfileUser, ApplicationStatus, UploadDocuments, FormTabs } from '../user/ProfileFormHelpers';

class AdminProfileForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            userApplication: {},
            isEditing: false,
            isSubmitted: false
        };

        this.renderInterface = this.renderInterface.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            user: nextProps.user,
            userApplication: nextProps.userApplication,
            isSubmitted: nextProps.userApplication.isSubmitted
        });
    }

    renderInterface() {
        if(this.state.user && this.state.userApplication) {
            const { user, userApplication, isEditing, isSubmitted } = this.state;
            return (
                <div>
                    <Row>
                        <DashboardNavbar
                            user={user}
                            isEditing={isEditing}
                            isSubmitted={isSubmitted}
                            onEditChange={this.onEditChange}
                            handleApplicationSave={this.handleApplicationSave}
                            handleApplicationSubmit={this.handleApplicationSubmit}
                        />
                    </Row>

                    <Row className="content">
                        <Col lg={4} md={5}>
                            <ProfileUser
                                user={user}
                            />

                            <ApplicationStatus
                                applicationStarted={ this.formatDate(userApplication.applicationStarted) }
                                applicationSubmitted={ this.formatDate(userApplication.applicationSubmitted) }
                                applicationApproved={ this.formatDate(userApplication.applicationApproved) }
                                isApproved={userApplication.isApproved}
                            />
                        </Col>
                        <Col lg={8} md={7}>
                            <Row>
                                <Card>
                                    <CardTitle>
                                        <span
                                            className="title"
                                            style={{paddingLeft: '15px'}}>
                                            Data Profile
                                        </span>
                                    </CardTitle>
                                    <CardBlock>
                                        <FormTabs
                                            user={user}
                                            userApplication={userApplication}
                                            isEditing={isEditing}
                                            handleApplicationSave={this.handleApplicationSave}
                                            handleUserInputChange={this.handleUserInputChange}
                                            handleUserApplicationInputChange={this.handleUserApplicationInputChange}
                                            handleProfessionalExam={this.handleProfessionalExam}
                                            handleTrainingEnd={this.handleTrainingEnd}
                                            handleTrainingStart={this.handleTrainingStart}
                                            handleAppointmentDate={this.handleAppointmentDate}
                                        />
                                    </CardBlock>
                                </Card>
                            </Row>
                        </Col>
                    </Row>
                </div>
            )
        }
        return <div></div>
    }

    render() {
        return (
            <div>
                <div className="wrapper">
                    <Sidebar
                        menu={['Applicants', 'Staff', 'Submitted Douments']}
                    />

                    <Container fluid={true} className="main-panel">
                        {
                            this.props.loading
                                ?
                                <div className="loading">Loading...</div>
                                :
                                this.renderInterface()
                        }

                        <Footer />
                    </Container>
                </div>
            </div>
        );
    }
}

AdminProfileForm.propTypes = {
    loading: PropTypes.bool,
    user: PropTypes.object.isRequired,
    userApplication: PropTypes.object.isRequired,
};

export default AdminProfileForm;
