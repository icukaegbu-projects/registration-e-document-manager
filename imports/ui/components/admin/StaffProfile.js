/**
 * Created by ic on 4/27/17.
 */
/**
 * Created by ikedi on 14/12/2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row } from 'reactstrap';

import Sidebar from '../Sidebar';
import AdminDashboardNavbar from './AdminDashboardNavbar';

import { StaffProfileDisplayForm } from '../user/ProfileFormHelpers';

import { createContainer } from 'meteor/react-meteor-data';

class StaffProfileForm extends Component {
    constructor(props) {
        super(props);

        this.renderInterface = this.renderInterface.bind(this);
        this.formatDate = this.formatDate.bind(this);
        this.onEditChange = this.onEditChange.bind(this);
    }

    // componentWillReceiveProps(nextProps) {
    //     if(nextProps && nextProps.user) {
    //         this.setState({
    //             user: nextProps.user
    //         });
    //     }
    // }

    onEditChange() {
        this.setState({
            isEditing: !this.state.isEditing
        })
    }

    formatDate(dateObj) {
        return (
            dateObj ? dateObj.toDateString() : 'Pending'
        );
    }

    renderInterface() {
        if(this.props.user) {
            const { user } = this.props;
            return (
                <div>
                    <Row>
                        <AdminDashboardNavbar user={user}/>
                    </Row>
                    <StaffProfileDisplayForm user={user}/>
                </div>
            )
        }
        return <div></div>
    }

    render() {
        return (
            <div>
                <div className="wrapper">
                    <Sidebar />

                    <Container fluid={true} className="main-panel">
                        {
                            this.props.loading
                                ?
                                <div className="loading">Loading...</div>
                                :
                                this.renderInterface()
                        }
                    </Container>
                </div>
            </div>
        );
    }
}

StaffProfileForm.propTypes = {
    loading: PropTypes.bool,
    user: PropTypes.object
};


export default StaffProfile = createContainer(({ userId }) => {
    //console.log(params)
    const _id = userId ? userId : Meteor.userId();
    console.log(_id);

    const userHandle = Meteor.subscribe('users.one', _id);
    const loading = !userHandle.ready();

    const user = Meteor.users.findOne({ _id: Meteor.userId() });

    return {
        user,
        loading
    }
}, StaffProfileForm);