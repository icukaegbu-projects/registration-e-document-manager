import React, {Component} from 'react';
import { Container, Row, Col, Card, CardBlock, Progress } from 'reactstrap';
import Dropzone from 'react-dropzone';
import Papa from 'papaparse';

import Sidebar from '../Sidebar';
import AdminDashboardNavbar from './AdminDashboardNavbar';

class UploadCSV extends Component {
    constructor(props) {
        super(props);

        this.state = {
            progress: 0,
            inProgress: false,
            uploaded: false,
            checkFileSizeError: false
        };

        this.showUploadProgress = this.showUploadProgress.bind(this);
        this.onDrop = this.onDrop.bind(this);
    }

    onDrop (files) {
        if(files && files[0]) {
            let file = files[0],
                self = this;

            if (file) {
                if (file.size > 10240000) {
                    toastr.options = {"positionClass": 'toast-top-left'};
                    toastr.error('File size greater than 1MB', 'Large File Size');
                    this.setState({
                        checkFileSizeError: true
                    })
                    return;
                }

                //start uploading, display
                this.setState({
                    uploading: file,
                    inProgress: true
                });

                Papa.parse( file, {
                    header: true,
                    complete( results, file ) {
                        Meteor.call('upload.members.csv', { data: results.data }, (err, result) => {
                            if(err) {
                                console.log(err);
                                self.setState({
                                    progress: 0,
                                    inProgress: false
                                })
                            }
                            else {
                                self.setState({
                                    progress: 0,
                                    inProgress: false,
                                    checkFileSize: false
                                })
                            }
                        });
                    }
                });
            }
        }
    }

    showUploadProgress() {
        if (this.state.inProgress) {
            return (
                <div>
                    <Progress
                        animated
                        color="success"
                        value={this.state.progress}
                        aria-valuemax="100"
                        aria-valuemin="0"
                        aria-valuenow={this.state.progress || 0}
                    >
                        <span className="sr-only">{this.state.progress}% Complete (success)</span>
                        <span>{this.state.progress}%</span>
                    </Progress>

                    <div className="loader2">Loading...</div>
                </div>
            );
        }
    }

    render() {
        return (
            <div className="wrapper">
                <Sidebar />

                <Container fluid={true} className="main-panel">
                    <Row>
                        <AdminDashboardNavbar showAdminMenu={false} />
                    </Row>
                    <Row className="content" type="flex">
                        <Col  md={4} sm={4}>
                            <Card
                                className="card-just-text card-big-shadow "
                                data-background="color" data-color="blue"
                                data-radius="none"
                                style={{height: '300px'}}
                            >
                                <CardBlock className="content">
                                    <h6 className="category">Please Upload CSV File</h6>
                                    <h6 className="title" style={{fontWeight: 700}}>Please Upload CSV File</h6>
                                    <div className="description">
                                        {this.showUploadProgress()}
                                    </div>
                                    <Row>
                                        <Col md={12} sm={12}>
                                            {
                                                !this.state.inProgress ?
                                                    <Dropzone
                                                        onDrop={this.onDrop}
                                                        multiple={false}
                                                        className="btn btn-primary"
                                                    >
                                                        Drag file on or Click to Upload
                                                    </Dropzone>
                                                    : <div></div>
                                            }

                                        </Col>
                                    </Row>
                                </CardBlock>
                            </Card>
                        </Col>
                    </Row>

                </Container>
            </div>
        );
    }
}

export default UploadCSV;