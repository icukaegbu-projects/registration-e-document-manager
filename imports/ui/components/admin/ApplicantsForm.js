import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col, Button } from 'reactstrap';

import Sidebar from '../Sidebar';
import AdminDashboardNavbar from './AdminDashboardNavbar';
import ApprovalComponent from './ApprovalComponent';
import RenderPDFDocuments from './RenderPDFDocuments';
import SearchInput from '../lib/SearchInput';
import { ProfileUser2, ProfileUser3 } from '../user/ProfileFormHelpers';

import { getUserDocumentsList_V2, deleteUser_V2 } from '../../../api/users/DropboxMethods_V2';

import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { UserApplication } from '../../../api/users/collections';

class ApplicantsForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            selectedUser: null,
            loading: false,
            userFileList: [],
            singleDoc: null,
            currentFileName: ''
        };

        this.onDelete = this.onDelete.bind(this);
        this.showDocuments = this.showDocuments.bind(this);
        this.showSingleDocument = this.showSingleDocument.bind(this);
        this.saveDocument = this.saveDocument.bind(this);
        this.renderApprovalIfSubmitted = this.renderApprovalIfSubmitted.bind(this);
    }

    onDelete(user) {
        let isDelete = confirm("Delete Account?");
        if(isDelete) {
            //if he has ananNumber use as his path; else use his name combo
            //delete his folder from Dropbox
            //delete his records from Meteor.users and UserApplication
            //send him a mail to notify him of the deletion

            let userId = user._id,
                path = "";

            if(user.ananNumber && user.ananNumber !== "") {
                path = user.ananNumber;
            }else{
                path = `/${user.bio.lastName.toUpperCase()}-${user.bio.firstName.toUpperCase()}-${user.username.toUpperCase()}`;
            }

            deleteUser_V2.call({ path, userId }, (err, result) => {
                if(err) {
                    //console.log(err);
                    toastr.options = {"positionClass":'toast-top-right'};
                    toastr.error(err.reason, 'Delete Error');
                }else{
                    toastr.options = {"positionClass":'toast-top-right'};
                    toastr.warning('Account Deleted Successful', 'Success');
                    //console.log(result)
                }
            });
        }
    }

    showDocuments(user) {
        //sets the selectedUser to the passed in user
        //the id of the user will be used to determine which documents to load and where to store comments

        //THE USER RETURNED HERE IS AN INSTANCE OF USERAPPLICATION: IT HAS ANANNUMBER
        //BUT NOT FIRSTNAME ETC.
        //TO FIND THE FULL USER, ITERATE THROUGH THE THIS.PROPS.USER PROPERTY
        this.setState({
            visible: !this.state.visible,
            selectedUser: user
        });

        if(!user) {
            this.setState({
                userFileList: [],
                singleDoc: null,
                currentFileName: ''
            });
           return;
        }else {
            //generate user folder name if he doesnt yet have an anan number
            //if bio object has an ananNumber field, use it else use
            let path = "";

            if(user.bio.ananNumber && user.bio.ananNumber != "") {
                //console.log("anan: "+user.bio.ananNumber)
                path = user.bio.ananNumber;
            }else{
                path = user.bio.lastName.toUpperCase()+"-"+user.bio.firstName.toUpperCase()+"-"+user.username.toUpperCase();
            }
            this.setState({
                loading: true
            });

            getUserDocumentsList_V2.call({ folderName: path }, (err, result) => {
                if(err) {
                    this.setState({
                        loading: false
                    });
                }else{
                    let files = result.map((file) => {
                        return {
                            name: file.name,
                            path_display: file.path_display,
                            path_lower: file.path_lower,
                            size: file.size,
                            id: file.id
                        }
                    });

                    this.setState({
                        loading: false,
                        userFileList: files
                    });
                }
            });
        }
    }

    showSingleDocument(path) {
        let xhr = new XMLHttpRequest();
        xhr.responseType = 'arraybuffer';

        let arr = path.split("/"),
            fileName = arr[arr.length - 1].split("_").join(" ").toUpperCase(),
            name = path.split("/").join("_");

        //check if file exists on FileSystem, if it does, load it, else load from internet

        this.setState({
            loading: true
        });

        xhr.onload = () => {
            if (xhr.status === 200) {
                let blob = new Blob([xhr.response], {type: 'application/pdf'});

                this.setState({
                    singleDoc: blob,
                    loading: false,
                    currentFileName: fileName
                });
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('File downloaded successfully', 'File Downloaded');
            }
            else {
                var errorMessage = xhr.response || 'Unable to download file';
                // Upload failed. Do something here with the error.
                this.setState({
                    loading: false
                });

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(errorMessage, 'Error in downloading file');
            }
        };

        xhr.open('POST', 'https://content.dropboxapi.com/2/files/download');
        //xhr.setRequestHeader('Authorization', 'Bearer ' + Meteor.settings.public.DROP_BOX_IJ);
        xhr.setRequestHeader('Authorization', 'Bearer ' + Meteor.settings.public.DROP_BOX_TOKEN);
        xhr.setRequestHeader('Dropbox-API-Arg', JSON.stringify({
            path: path
        }));
        xhr.send();
    }

    saveDocument(user) {
        //perform some action to save the form
        //persist any comment made on the document to the DB
        this.setState({
            visible: false
        })
    }

    renderApprovalIfSubmitted(user) {
        //only display ApprovalComponent if user has submitted his application
        return (
            this.props.userApplications.find(userApplication => {
                return (user._id === userApplication.userId) && userApplication.isSubmitted
            })
            ?
                <ApprovalComponent
                    applicant={user}
                    showDocuments={this.showDocuments}
                    loggedInStaff={this.props.loggedInStaff}
                />
                :
                <div
                    style={{
                        height: 85,
                        backgroundColor: '#f57c00',
                        color: 'white',
                        fontWeight: 700,
                        fontSize: 16,
                        paddingTop: 25,
                        paddingLeft: 35
                    }}
                >
                    Application Not Yet Submitted
                </div>
        )
    }

    render() {
        const { users, userApplications, loading } = this.props;

        return (
            <div className="wrapper">
                <Sidebar />

                <Container fluid={true} className="main-panel">
                    <Row>
                        <AdminDashboardNavbar showAdminMenu={false} />
                    </Row>
                    {
                        this.state.selectedUser ?
                            <RenderPDFDocuments
                                user={this.state.selectedUser}
                                showDocuments={this.showDocuments}
                                showSingleDocument={this.showSingleDocument}
                                loading={this.state.loading}
                                userFileList={this.state.userFileList}
                                doc={this.state.singleDoc}
                                fileName={this.state.currentFileName}
                                staffId={this.props.loggedInStaff._id}
                                staffRole={Roles.getRolesForUser(this.props.loggedInStaff._id).toString() || this.props.loggedInStaff.bio.role}
                                showAddComments={true}
                            />
                        :
                            <div className="content" type="flex">
                                <Row>
                                    <Col md={12} sm={12}>
                                        <SearchInput
                                            onClick={this.props.onSearchClick}
                                            showAnanNumber={false}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    {
                                        !loading && users ?
                                            users.map(user => {
                                                const uas = userApplications.find(app => app.userId === user._id);
                                                let uploadStatus = {
                                                    filesUploaded: 0,
                                                    filesOutstanding: 0
                                                };

                                                if(uas) {
                                                    const { filesUploaded, filesOutstanding } = uas;

                                                    if(filesUploaded >= 0 && filesOutstanding >= 0) {
                                                        uploadStatus = {
                                                            filesUploaded,
                                                            filesOutstanding
                                                        }
                                                    } else {
                                                        let up = 0, out = 0;
                                                        uas.files.forEach(file => {
                                                            file.isUploaded ? up+=1 : out+=1
                                                        });
                                                        uploadStatus = {
                                                            filesUploaded: up,
                                                            filesOutstanding: out
                                                        }
                                                    }
                                                }


                                                return (
                                                    <Col key={user._id} md={4} sm={6}>
                                                        <ProfileUser3
                                                            user={user}
                                                            uploadStatus={uploadStatus}
                                                            profilePicture={true}
                                                            deleteUser={this.onDelete}
                                                        >
                                                            {this.renderApprovalIfSubmitted(user)}
                                                        </ProfileUser3>
                                                    </Col>
                                                )
                                            })
                                            :
                                            <div>
                                                <h4>No Applicants</h4>
                                            </div>
                                    }
                                </Row>
                                <Row>
                                    {
                                        users.length === 0 ? null :
                                            <Col md={{size: 2, push: 3, pull: 3, offset: 1}}>
                                                <Button className="btn btn-info" onClick={this.props.loadMore}>
                                                    <span style={{ paddingRight: 10 }}>Load More</span>
                                                    <i className="fa fa-arrow-circle-down"></i>
                                                </Button>
                                            </Col>
                                    }
                                </Row>
                            </div>
                    }
                </Container>
            </div>
        )
    }
}

ApplicantsForm.propTypes = {
    users: PropTypes.array.isRequired,
    userApplications: PropTypes.array.isRequired,
    loggedInStaff: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    onSearchClick: PropTypes.func,
    loadMore: PropTypes.func
};

const applicantsOptions = new ReactiveVar();
const pageNumber = new ReactiveVar(1);
const RECORDS_PER_PAGE = 10;

const onSearchClick = (fields) => {
    applicantsOptions.set(fields)
    //console.log(applicantsOptions.get())
};

const loadMore = () => {
    pageNumber.set(pageNumber.get() + 1);
    let fields = applicantsOptions.get();
    if(!fields) {
        fields = {};
        fields.numResults = 0;
    }
    fields.numResults = pageNumber.get() * RECORDS_PER_PAGE;
    applicantsOptions.set(fields);
    // console.log('Applicant Fields');
    // console.log(fields);
};

export default Applicants = createContainer(({ params }) => {
    //console.log(applicantsOptions.get())
    const usersHandle = Meteor.subscribe('applicants.all', applicantsOptions.get());
    const usersAll = Meteor.users.find({
        //"bio.role": "Applicant"
        "bio.membershipStatus": "APPLICANT"
    }, {
        sort: {createdAt: -1}
    }).fetch();

    const userIds = [];
    usersAll.forEach((user) => userIds.push(user._id));

    //only get the userApplications for the selected users
    const applicantsHandle = Meteor.subscribe('userApplication.all', userIds);
    const userApplications = UserApplication.find({}).fetch();

    //return only users who are Applicants
    const users = usersAll;
    const loading = !usersHandle.ready() && !applicantsHandle.ready();

    //return the role of the currently logged in staff
    const loggedInStaff = Meteor.users.findOne({ _id: Meteor.userId() }) || {};

    return {
        loading,
        users,
        userApplications,
        loggedInStaff,
        onSearchClick,
        loadMore
    }

}, ApplicantsForm);

// export default Applicants = createContainer(({ params }) => {
//     const usersHandle = Meteor.subscribe('applicants.all', applicantsOptions.get());
//
//     //only get the userApplications for the selected users
//     const applicantsHandle = Meteor.subscribe('userApplication.all');
//     const loading = !usersHandle.ready() && !applicantsHandle.ready();
//
//     const usersAll = Meteor.users.find({
//                                 // username: { $ne: 'admin' },
//                                 // "bio.role": { $nin: ['Admin', 'Supervisor', 'DataCapture',
//                                 //                     'Audit', 'Exams', 'Membership', 'Registry']}
//                             }, {
//                                 sort: {createdAt: -1}
//                             }).fetch();
//
//     //return only users who are Applicants
//     const users = usersAll;
//
//     const userApplications = UserApplication.find({}).fetch();
//
//     //return the role of the currently logged in staff
//     const loggedInStaff = Meteor.users.findOne({ _id: Meteor.userId() }) || {};
//
//     return {
//         loading,
//         users,
//         userApplications,
//         loggedInStaff,
//         onSearchClick
//     }
//
// }, ApplicantsForm);