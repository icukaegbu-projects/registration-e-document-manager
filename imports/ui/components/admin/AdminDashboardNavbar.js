/**
 * Created by ikedi on 18/01/2017.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { adminDashboard, applicants, archivedMembers, viewMembers, staff, uploadCSV } from './MenuActions';

import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

class AdminDashboardNavbar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null
        }

        this.logout = this.logout.bind(this);
        this.displayMail = this.displayMail.bind(this);
        this.adminDashboardMenu = this.adminDashboardMenu.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            user: nextProps.user
        });
    }

    displayMail() {
        if(this.props.user && this.props.user.username) {
            return  `, ${this.props.user.username.toUpperCase()}`
        }
    }


    adminDashboardMenu() {
        if (this.props.user && this.props.user.bio) {
            if ( this.props.user.username === "admin" ) {
                return (
                    <span>
                        <NavItem>
                            <NavLink href="#" className="btn btn-success" onClick={uploadCSV}>Upload/Download CSV</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#" className="btn btn-primary" onClick={viewMembers}>View Members</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#" className="btn btn-warning" onClick={archivedMembers}>Archived Members</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#" className="btn btn-default" onClick={staff}>View Staff</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#" className="btn btn-danger" onClick={applicants} >View Applicants</NavLink>
                        </NavItem>
                    </span>
                );
            }

            switch(this.props.user.bio.role) {
                case "Admin":
                    return (
                        <span>
                            <NavItem>
                                <NavLink href="#" className="btn btn-primary" onClick={viewMembers}>View Members</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#" className="btn btn-warning" onClick={archivedMembers}>Archived Members</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#" className="btn btn-default" onClick={staff}>View Staff</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#" className="btn btn-danger" onClick={applicants} >View Applicants</NavLink>
                            </NavItem>
                        </span>
                    );
                    break;
                case "Supervisor":
                    return (
                        <span>
                            <NavItem>
                                <NavLink href="#" className="btn btn-primary" onClick={viewMembers}>View Members</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#" className="btn btn-warning" onClick={archivedMembers}>Archived Members</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#" className="btn btn-default" onClick={staff}>View Staff Work???</NavLink>
                            </NavItem>
                        </span>
                    );
                    break;
                case "DataCapture":
                    return (
                        <span>
                            <NavItem>
                                <NavLink href="#" className="btn btn-primary" onClick={archivedMembers}>Archived Members</NavLink>
                            </NavItem>
                        </span>
                    );
                    break;
                case "Audit":
                case "Exams":
                case "Membership":
                case "Registry":
                    return (
                        <span>
                        <NavItem>
                            <NavLink href="#" className="btn btn-danger" onClick={applicants} >View Applicants</NavLink>
                        </NavItem>
                    </span>
                    );
                    break;
            }
        } else {
            return <span></span>
        }
    }

    logout() {
        Meteor.logout((error) => {
            if (error) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error.reason, 'Logout Error');
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.info('Logout Successful', 'Success');

                this.setState({ loggedIn: false });

                FlowRouter.go('index');
            }
        })
    }

    render() {
        return (
            <Navbar className="navbar navbar-default">
                <NavbarBrand href="#" onClick={adminDashboard}>
                    <i className="fa fa-dashboard"></i> Welcome {/*this.displayMail()*/}
                </NavbarBrand>
                <Nav navbar className="float-xs-right">
                    <div style={{paddingTop: 15}}>
                        {this.props.children}
                        {this.props.showAdminMenu ? this.adminDashboardMenu() : ''}
                        <NavItem style={{marginLeft: 20}}>
                            <NavLink href="#" className="btn btn-default" onClick={this.logout}>Logout</NavLink>
                        </NavItem>
                    </div>
                </Nav>
            </Navbar>
        );
    }

}

AdminDashboardNavbar.propTypes = {
    user: PropTypes.object.isRequired,
    showAdminMenu: PropTypes.bool
};

AdminDashboardNavbar.defaultProps = {
    user: {
        username: '',
        emails: [{address: '', verified: false}]
    },
    showAdminMenu: true
}

export default AdminDashboardNavbar;