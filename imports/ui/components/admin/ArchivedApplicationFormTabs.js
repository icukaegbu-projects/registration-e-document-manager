/**
 * Created by ikedi on 19/01/2017.
 */
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { FormTabs } from '../user/ProfileFormHelpers';
import { UserApplication } from '../../../api/users/collections';

export default ArchivedApplicationFormTabs = createContainer(({ params, userId, userApplicationId, isEditing,
    handleUserApplicationInputChange, handleProfessionalExam,  handleTrainingEnd, handleTrainingStart,
    handleAppointmentDate }) => {

    const usersHandle = Meteor.subscribe('users.one', userId);
    const userApplicationHandle = Meteor.subscribe('userApplication.one', userId);
    const loading = !usersHandle.ready() && !userApplicationHandle.ready();

    const user = Meteor.users.findOne({
        _id: userId
    }, {
        fields: {
            "roles": 0,
            "createdAt": 0
        }
    });

    const userApplication = UserApplication.findOne({ _id: userApplicationId });

    return {
        loading,
        user,
        userApplication,
        isEditing,
        handleUserApplicationInputChange,
        handleProfessionalExam,
        handleTrainingEnd,
        handleTrainingStart,
        handleAppointmentDate
    };
}, FormTabs);

