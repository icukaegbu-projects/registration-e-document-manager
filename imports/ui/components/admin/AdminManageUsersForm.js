import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

import { Card, CardTitle, CardText, Form, Button, Container, Row, Col,
        NavLink, NavItem } from 'reactstrap';

import { StaffBioData, LoginDetails, AnanDisplay, StyledDivider } from '../user/RegisterFormHelpers';
import { StatusDisplay } from '../user/ProfileFormHelpers';

import Sidebar from '../Sidebar';
import AdminDashboardNavbar from './AdminDashboardNavbar';

import { staffSignup } from '../../../api/users/methods';

const StaffDisplayForm = ({ staff, count, loading }) => {
    let values = [];

    if ( staff && staff.length > 0 ) {
        staff.map(worker => {
            const name = `${worker.bio.lastName.toUpperCase()}, ${worker.bio.firstName}`;
            let icon = '', color = '';
            switch(worker.bio.role) {
                case 'Supervisor':
                    icon = 'fa-gavel';
                    color = '#5D4037';
                    break;
                case 'DataCapture':
                    icon = 'fa-folder-open';
                    color = '#7ACB58';
                    break;
                case 'Audit':
                    icon = 'fa-binoculars';
                    color = '#FF4A00';
                    break;
                case 'Exams':
                    icon = 'fa-book';
                    color = '#0081E9';
                    break;
                case 'Membership':
                    icon = 'fa-id-card';
                    color = '#760076';
                    break;
                case 'Registry':
                    icon = 'fa-key';
                    color = '#009688';
                    break;
                default:
                    icon = 'fa-check';
                    color = '#c2185b';
                    break;
            }

            values.push({
                label: worker.bio.role.toUpperCase(),
                icon: icon,
                color: color,
                data: name,
                key: worker._id
            });
        });
    }

    return (
        loading ?
            <StatusDisplay
                title="Staff Members"
                subtitle={`Total Staff: Pending`}
                values={[]}
            />
            : <StatusDisplay
                title="Staff Members"
                subtitle={`Total Staff: ${count}`}
                values={values}
            />
    );
}

const StaffDisplay = createContainer(({ params }) => {
    const usersHandle = Meteor.subscribe('users.all');
    const loading = !usersHandle.ready();

    const staff = Meteor.users.find({
        "bio.role": { $in: ['Supervisor', 'DataCapture', 'Audit', 'Exams', 'Membership', 'Registry'] }
    }, {
        sort: { createdAt: -1, "bio.role": -1 }
    }).fetch();

    const count = staff ? staff.length : 0;

    return {
        loading,
        staff,
        count
    };

}, StaffDisplayForm);

class AdminManageUsersForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            registerStaff: false,
            fields: {
                username: '',
                email: '',
                password: '',
                confirmPassword: '',

                role: '',
                firstName: '',
                middleName: '',
                lastName: '',
                phone: '',
                gender: '',
                maritalStatus: '',
                privateAddress: '',
                stateOfOrigin: '',
                isRegistering: false
            }
        }

        this.createMenu = this.createMenu.bind(this);
        this.registerStaff = this.registerStaff.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resetState = this.resetState.bind(this);
    }

    createMenu() {
        if ( this.state.registerStaff === false ){
            return (
                <NavItem>
                    <NavLink
                        href="#"
                        className="btn btn-primary"
                        onClick={this.registerStaff}
                        style={{minWidth: 100}}
                    >
                        <i className="fa fa-plus" style={{paddingRight: 10, fontSize: 15}}></i>
                        Add Staff
                    </NavLink>
                </NavItem>
            );
        } else if ( this.state.registerStaff === true ) {
            return (
                <span>
                        <NavItem>
                            <NavLink
                                href="#"
                                className="btn btn-danger"
                                onClick={this.registerStaff}
                                style={{minWidth: 100}}
                            >
                                Cancel
                            </NavLink>
                        </NavItem>
                    </span>
            )
        }
    }

    registerStaff() {
        this.setState({
            registerStaff: !this.state.registerStaff
        });
    }

    handleInputChange(evt)
    {
        const fields = this.state.fields;
        fields[evt.target.name] = evt.target.value;

        this.setState({ fields });
    }

    resetState() {
        this.setState({
            registerStaff: false,
            fields: {
                username: '',
                email: '',
                password: '',
                confirmPassword: '',

                role: '',
                firstName: '',
                middleName: '',
                lastName: '',
                phone: '',
                gender: '',
                maritalStatus: '',
                privateAddress: '',
                stateOfOrigin: '',
                isRegistering: false
            }
        })
    }

    handleSubmit(e) {
        e.preventDefault();

        //set the isRegistering field
        const fields = this.state.fields;

        fields.isRegistering = !fields.isRegistering;
        this.setState({ fields });

        let userData = this.state.fields;

        //validate the form
        let emails = [];
        let user_email = { address: userData.email, verified: false };
        emails.push(user_email);
        userData.emails = emails;

        //exclude isRegistering from fields submitted to server
        let { isRegistering, ...userDataSubmit } = userData;

        staffSignup.call(userDataSubmit,  (message) => {
            if (message) {
                if(message.reason === 'Email already exists' || message.reason === 'Phone number already exists'){
                    this.resetState();
                }

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(message.reason, 'Signup Error');

                //hide the pre-loader
                fields.isRegistering = !fields.isRegistering;
                this.setState({ fields });
                return;
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Registration Successful', 'Success');

                this.resetState();
                return;
            }
        });
    }

    render() {
        return (
            <div className="wrapper">
                <Sidebar
                />

                <Container fluid={true} className="main-panel">
                    <Row>
                        <AdminDashboardNavbar showAdminMenu={false}>
                            {this.createMenu()}
                        </AdminDashboardNavbar>
                    </Row>
                    <Row className="content" type="flex">
                        <Col sm={12} md={4}>
                            <StaffDisplay/>
                        </Col>
                        <Col sm={12} md={6}>
                            {
                                this.state.registerStaff ?
                                    <div className="full-screen register background">
                                        <div className="wrapper">
                                            <div className="filter-black"></div>
                                            <Card className="demo-card">
                                                <CardTitle className="title text-center">Register</CardTitle>
                                                <AnanDisplay />

                                                <CardText tag="div">
                                                    <StyledDivider/>

                                                    <Form className="register-form">

                                                        <LoginDetails
                                                            username={this.state.fields.username}
                                                            email={this.state.fields.email}
                                                            password={this.state.fields.password}
                                                            confirmPassword={this.state.fields.confirmPassword}
                                                            handleInputChange={this.handleInputChange}
                                                        />

                                                        <StyledDivider/>

                                                        <StaffBioData
                                                            firstName={this.state.fields.firstName}
                                                            lastName={this.state.fields.lastName}
                                                            middleName={this.state.fields.middleName}
                                                            phone={this.state.fields.phone}
                                                            gender={this.state.fields.gender}
                                                            maritalStatus={this.state.fields.maritalStatus}
                                                            privateAddress={this.state.fields.privateAddress}
                                                            stateOfOrigin={this.state.fields.stateOfOrigin}
                                                            handleInputChange={this.handleInputChange}
                                                            isEditing={true}
                                                        />

                                                        <Button
                                                            className="btn btn-fill btn-block"
                                                            onClick={this.handleSubmit}
                                                        >
                                                            Register
                                                        </Button>
                                                    </Form>
                                                </CardText>
                                            </Card>
                                        </div>
                                    </div>
                                    :
                                    <div></div>
                            }
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default AdminManageUsersForm;