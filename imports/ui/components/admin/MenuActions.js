/**
 * Created by ikedi on 22/03/2017.
 */
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

export const adminDashboard = () => {
    FlowRouter.go('admin-dashboard')
};

export const archivedMembers = () => {
    FlowRouter.go('archived-members');
};

export const viewMembers = () => {
    FlowRouter.go('members');
};

export const applicants = () => {
    FlowRouter.go('applicants');
};

export const staff = () => {
    FlowRouter.go('staff');
};

export const uploadCSV = () => {
    FlowRouter.go('upload-csv');
};

export const auditTrail = () => {
    FlowRouter.go('audit-trail');
};

export const profile = () => {
    FlowRouter.go('staff-profile');
};