/**
 * Created by ic on 7/17/17.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

import { Card, CardTitle, CardText, Form, Button, Container, Row, Col,
    NavLink, NavItem } from 'reactstrap';

import { StaffBioData, LoginDetails, AnanDisplay, StyledDivider } from '../user/RegisterFormHelpers';
import { ProfileUser2, StaffProfileDisplayForm } from '../user/ProfileFormHelpers';

import Sidebar from '../Sidebar';
import AdminDashboardNavbar from './AdminDashboardNavbar';

import { staffSignup, staffDelete } from '../../../api/users/methods';

class AdminManageUsersForm2Display extends Component {
    constructor(props) {
        super(props);

        this.state = {
            registerStaff: false,
            showStaffProfile: false,
            selectedStaff: null,
            fields: {
                username: '',
                email: '',
                password: '',
                confirmPassword: '',

                role: '',
                firstName: '',
                middleName: '',
                lastName: '',
                phone: '',
                gender: '',
                maritalStatus: '',
                privateAddress: '',
                stateOfOrigin: '',
                isRegistering: false
            }
        }

        this.createMenu = this.createMenu.bind(this);
        this.registerStaff = this.registerStaff.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resetState = this.resetState.bind(this);
        this.showStaffProfile = this.showStaffProfile.bind(this);
        this.showAllStaff = this.showAllStaff.bind(this);
        this.showRegisterStaff = this.showRegisterStaff.bind(this);
        this.renderInterface = this.renderInterface.bind(this);
    }

    createMenu() {
        if ( this.state.registerStaff === false ){
            return (
                <NavItem>
                    <NavLink
                        href="#"
                        className="btn btn-primary"
                        onClick={this.registerStaff}
                        style={{minWidth: 100}}
                    >
                        <i className="fa fa-plus" style={{paddingRight: 10, fontSize: 15}}></i>
                        Add Staff
                    </NavLink>
                </NavItem>
            );
        } else if ( this.state.registerStaff === true ) {
            return (
                <span>
                        <NavItem>
                            <NavLink
                                href="#"
                                className="btn btn-danger"
                                onClick={this.registerStaff}
                                style={{minWidth: 100}}
                            >
                                Cancel
                            </NavLink>
                        </NavItem>
                    </span>
            )
        }
    }

    registerStaff() {
        this.setState({
            registerStaff: !this.state.registerStaff
        });
    }

    handleInputChange(evt)
    {
        const fields = this.state.fields;
        fields[evt.target.name] = evt.target.value;

        this.setState({ fields });
    }

    resetState() {
        this.setState({
            registerStaff: false,
            fields: {
                username: '',
                email: '',
                password: '',
                confirmPassword: '',

                role: '',
                firstName: '',
                middleName: '',
                lastName: '',
                phone: '',
                gender: '',
                maritalStatus: '',
                privateAddress: '',
                stateOfOrigin: '',
                isRegistering: false
            }
        })
    }

    onDelete(user) {
        let isDelete = confirm("Delete Staff Account?");
        if(isDelete) {
            let userId = user._id;

            staffDelete.call({ userId }, (err, result) => {
                if(err) {
                    console.log(err);
                    toastr.options = {"positionClass":'toast-top-left'};
                    toastr.error(err.reason, 'Delete Error');
                }else{
                    toastr.options = {"positionClass":'toast-top-left'};
                    toastr.warning('Account Deleted Successful', 'Success');
                    console.log(result)
                }
            });
        }
    }

    handleSubmit(e) {
        e.preventDefault();

        //set the isRegistering field
        const fields = this.state.fields;

        fields.isRegistering = !fields.isRegistering;
        this.setState({ fields });

        let userData = this.state.fields;

        //validate the form
        let emails = [];
        let user_email = { address: userData.email, verified: false };
        emails.push(user_email);
        userData.emails = emails;

        //exclude isRegistering from fields submitted to server
        let { isRegistering, ...userDataSubmit } = userData;

        staffSignup.call(userDataSubmit,  (message) => {
            if (message) {
                if(message.reason === 'Email already exists' || message.reason === 'Phone number already exists'){
                    this.resetState();
                }

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(message.reason, 'Signup Error');

                //hide the pre-loader
                fields.isRegistering = !fields.isRegistering;
                this.setState({ fields });
                return;
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Registration Successful', 'Success');

                this.resetState();
                return;
            }
        });
    }

    showRegisterStaff() {
        return (
            <div className="full-screen register background">
                <div className="wrapper">
                    <div className="filter-black"></div>
                    <Card className="demo-card">
                        <CardTitle className="title text-center">Register</CardTitle>
                        <AnanDisplay />

                        <CardText tag="div">
                            <StyledDivider/>

                            <Form className="register-form">

                                <LoginDetails
                                    username={this.state.fields.username}
                                    email={this.state.fields.email}
                                    password={this.state.fields.password}
                                    confirmPassword={this.state.fields.confirmPassword}
                                    handleInputChange={this.handleInputChange}
                                />

                                <StyledDivider/>

                                <StaffBioData
                                    firstName={this.state.fields.firstName}
                                    lastName={this.state.fields.lastName}
                                    middleName={this.state.fields.middleName}
                                    phone={this.state.fields.phone}
                                    gender={this.state.fields.gender}
                                    maritalStatus={this.state.fields.maritalStatus}
                                    privateAddress={this.state.fields.privateAddress}
                                    stateOfOrigin={this.state.fields.stateOfOrigin}
                                    handleInputChange={this.handleInputChange}
                                    isEditing={true}
                                />

                                <Button
                                    className="btn btn-fill btn-block"
                                    onClick={this.handleSubmit}
                                >
                                    Register
                                </Button>
                            </Form>
                        </CardText>
                    </Card>
                </div>
            </div>
        )
    }

    showAllStaff() {
        return (
            <Row>
                {
                    this.props.staff.map((staff) => {
                        return (
                            <Col  key={staff._id} md={4} sm={6}>
                                <ProfileUser2
                                    user={staff}
                                    isStaff={true}
                                    deleteUser={this.onDelete.bind(null, staff)}
                                    showCollapsible={false}
                                >
                                    <button
                                        style={{ marginTop: 5 }}
                                        className="btn btn-success btn-fill"
                                        onClick={this.showStaffProfile.bind(null,staff)}>
                                        <i className="fa fa-eye"></i>  View Profile
                                    </button>
                                </ProfileUser2>
                            </Col>
                        )
                    })
                }
            </Row>
        )
    }

    showStaffProfile(staff) {
        this.setState({
            showStaffProfile: true,
            selectedStaff: staff
        });
    }

    renderInterface() {
        if(this.state.showStaffProfile) {
            return (
                <div>
                    <button
                        className="btn btn-danger"
                        style={{marginLeft: 20}}
                        onClick={() => {
                        this.setState({
                            showStaffProfile: false,
                            selectedStaff: null
                        });
                    }}><i className="fa fa-close"></i></button>
                    <span style={{marginLeft: 5, fontWeight: 700}}>Click to Close</span>
                    <StaffProfileDisplayForm user={this.state.selectedStaff}/>
                </div>
            )
        }else{
            return this.state.registerStaff ?
                this.showRegisterStaff()
                :
                this.showAllStaff()
        }
    }

    render() {
        return (
            <div className="wrapper">
                <Sidebar
                />

                <Container fluid={true} className="main-panel">
                    <Row>
                        <AdminDashboardNavbar showAdminMenu={false}>
                            {this.createMenu()}
                        </AdminDashboardNavbar>
                    </Row>
                    <Row>
                        <Col sm={12} md={12}>
                            <h4 style={{marginLeft: 15}}>Total Staff: {this.props.count}</h4>
                        </Col>
                    </Row>
                    <Row className="content" type="flex">
                        <Col sm={12} md={12}>
                            {
                                this.renderInterface()
                            }
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

const AdminManageUsersForm2 = createContainer(({ params }) => {
    const usersHandle = Meteor.subscribe('users.all');
    const loading = !usersHandle.ready();

    const staff = Meteor.users.find({
        "bio.role": { $in: ['Supervisor', 'DataCapture', 'Audit', 'Exams', 'Membership', 'Registry'] }
    }, {
        sort: { createdAt: -1, "bio.role": -1 }
    }).fetch();

    const count = staff ? staff.length : 0;

    return {
        loading,
        staff,
        count
    };

}, AdminManageUsersForm2Display);

export default AdminManageUsersForm2;