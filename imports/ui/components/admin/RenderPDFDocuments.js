import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Row, Col, Form, InputGroup, InputGroupButton } from 'reactstrap';
import Collapsible from 'react-collapsible';

import { StatusDisplay, ProfileUser } from '../user/ProfileFormHelpers';
import { InputFormComponent } from '../user/RegisterFormHelpers';
import PDFViewer from '../lib/PDFViewer';

import { createContainer } from 'meteor/react-meteor-data';
import { ApprovalComments } from '../../../api/users/collections';
import { addApprovalComments } from '../../../api/users/methods';

const ApprovalCommentForm = ({ comment, handleInputChange, onSubmitComment, onDeleteComment }) => {
    return (
        <Form className="register-form">
            <Row>
                <Col md={12} sm={12}>
                    <InputGroup>
                        <InputFormComponent
                            placeholder="Enter Comment here"
                            type="textarea"
                            name="comment"
                            onChange={handleInputChange}
                            value={comment}
                        />
                        <InputGroupButton>
                            <Button
                                className="btn-success btn-fill btn-sm"
                                disabled={comment===''}
                                onClick={onSubmitComment}
                            >
                                <i className="fa fa-plus"></i>
                            </Button>
                            <Button
                                className="btn-primary btn-fill btn-sm"
                                disabled={comment===''}
                                onClick={onDeleteComment}
                            >
                                <i className="fa fa-minus"></i>
                            </Button>
                        </InputGroupButton>
                    </InputGroup>
                </Col>
            </Row>
        </Form>
    );
};

class RenderPDF extends Component {
    constructor(props) {
        super(props);

        this.state = {
            doc: null,
            addComment: false,
            comment: ""
        };

        this.addComment = this.addComment.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmitComment = this.onSubmitComment.bind(this);
        this.onDeleteComment = this.onDeleteComment.bind(this);
        this.displayComment = this.displayComment.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let comment = "";
        this.setState({
            doc: nextProps.doc
        });

        if(this.props.approvalComments) {
            //if staffRole is Membership, set comment to membership object
            switch(this.props.staffRole) {
                case "Membership":
                    comment = nextProps.approvalComments.membership &&
                    nextProps.approvalComments.membership.comment ?
                        nextProps.approvalComments.membership.comment : "";

                    this.setState({
                        comment: comment
                    });
                    break;
                case "Audit":
                    comment = nextProps.approvalComments.audit &&
                    nextProps.approvalComments.audit.comment ?
                        nextProps.approvalComments.audit.comment : "";

                    this.setState({
                        comment: comment
                    });
                    break;
                case "Exams":
                    comment = nextProps.approvalComments.exam &&
                    nextProps.approvalComments.exam.comment ?
                        nextProps.approvalComments.exam.comment : "";

                    this.setState({
                        comment: comment
                    });
                    break;
                case "Registry":
                    comment = nextProps.approvalComments.registry &&
                    nextProps.approvalComments.registry.comment ?
                        nextProps.approvalComments.registry.comment : "";

                    this.setState({
                        comment: comment
                    });
                    break;
                default:
                case "Membership":
                    this.setState({
                        comment: ""
                    });
                    break;
            }
        }
    }

    addComment() {
        if (this.state.addComment) {
            return  (
                <div>
                    <ApprovalCommentForm
                        comment={this.state.comment}
                        handleInputChange={this.handleInputChange}
                        onSubmitComment={this.onSubmitComment}
                        onDeleteComment={this.onDeleteComment}
                    />
                    <hr style={{ borderBottom: '1px solid #433ED2' }}/>
                </div>
            );
        }

        return (
            <div></div>
        )
    }

    handleInputChange(evt) {
        evt.preventDefault();
        console.log(evt.target.value)

        this.setState({
            comment: evt.target.value
        });
    }

    onDeleteComment(evt) {
        evt.preventDefault();

        this.setState({
            comment: ""
        });

        addApprovalComments.call({
            applicantId: this.props.user._id,
            staffId: "",
            staffRole: this.props.staffRole,
            comment: ""
        }, (err, response) => {
            if (err) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error('Failed to delete Comment', 'Comment Failure');
            }else{
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.info('Comment deleted successfully', 'Comment deleted');
            }
        })
    }

    onSubmitComment(evt) {
        evt.preventDefault();

        addApprovalComments.call({
            applicantId: this.props.user._id,
            staffId: this.props.staffId,
            staffRole: this.props.staffRole,
            comment: this.state.comment
        }, (err, response) => {
            if (err) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error('Failed to add Comment', 'Comment Failure');
            }else{
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Comment added successfully', 'Comment added');
            }
        });
    }

    displayComment() {
        let comments = [];
        //if membership, display the comments
        if (this.props.staffRole === 'Membership' && this.props.approvalComments) {
            for( key in this.props.approvalComments ) {
                if (key !== '_id' && key !== 'applicantId') {
                    if(this.props.approvalComments[key].comment !== "") {
                        comments.push({
                            staffRole: key.toUpperCase(),
                            comment: this.props.approvalComments[key].comment,
                            commentDate: this.props.approvalComments[key].dateCommented.toDateString()
                        });
                    }
                }
            }

            return (
                <div className="panel-group">
                    {
                        comments.map(comment => {
                            return (
                                <Collapsible
                                    trigger={`+ ${comment.staffRole} --- Click to Expand`}
                                    key={comment.staffRole}
                                    classParentString="panel panel-default"
                                >
                                    <div>{comment.comment}</div>
                                    <div>{comment.commentDate}</div>
                                </Collapsible>
                            )
                        })
                    }
                </div>
            )
        }
    }

    render() {
        let { user, showDocuments, loading, userFileList, showSingleDocument, showAddComments } = this.props;

        const values = userFileList.map((file) => {
            return {
                key: file.id,
                label: "",
                icon: "fa-file",
                color: "blue",
                data: file.name,
                isButton: true,
                onClick: showSingleDocument.bind(null, file.path_lower)
            }
        });

        return (
            <div style={{
                marginTop: 20
            }}>
                <Col sm={12} md={4}>
                    <div>
                        <Button
                            className="btn btn-fill btn-danger"
                            onClick={showDocuments.bind(null, null)}
                            style={{
                                marginBottom: 7
                            }}
                        >
                            <i className="fa fa-close"></i>
                        </Button>
                        <span style={{
                            marginLeft: 10,
                            fontWeight: 600
                        }}>
                        Click to Close
                    </span>
                    </div>

                    <ProfileUser
                        user={user}
                    >
                        { this.props.approvalComments && this.props.approvalComments.length > 0 ? <h5>Comments</h5> : <h5></h5> }
                        {
                            this.props.staffRole ?
                                this.displayComment() : <div></div>
                        }
                    </ProfileUser>
                    <StatusDisplay
                        title="Uploaded Documents"
                        values={values}
                    />

                </Col>
                <Col sm={12} md={8}>
                    {
                        showAddComments ?
                            <div style={{padding: 5}}>
                                <Button
                                    disabled={this.props.loading}
                                    className={this.state.addComment ? "btn btn-fill btn-default" : "btn btn-fill btn-danger"}
                                    onClick={() => this.setState({ addComment: !this.state.addComment })}
                                    style={{
                                        marginBottom: 7,
                                        marginLeft: 4
                                    }}
                                >
                                    {this.state.addComment ? "Hide" : "+ Add Comment" }
                                </Button>
                                {this.addComment()}
                            </div> : <div></div>
                    }
                    {
                        loading ?
                            <div className="loader2">Loading...</div>
                        :
                          this.state.doc ?
                              <PDFViewer
                                  doc={this.state.doc}
                                  fileName={this.props.fileName}
                              />
                        : <div></div>
                    }
                </Col>
            </div>
        );
    }

};

RenderPDF.propTypes = {
    user: PropTypes.object.isRequired,
    showDocuments: PropTypes.func,
    showSingleDocument: PropTypes.func,
    loading: PropTypes.bool,
    userFileList: PropTypes.array,
    doc: PropTypes.object,
    fileName: PropTypes.string,
    staffId: PropTypes.string,
    staffRole: PropTypes.string,
    commentLoading: PropTypes.bool,
    approvalComments: PropTypes.object,
    showAddComments: PropTypes.bool
};

RenderPDF.defaultProps = {
    showAddComments: false
}

export default RenderPDFDocuments = createContainer(({ user, showDocuments, showSingleDocument,
        loading, userFileList, doc, fileName, staffId, staffRole, showAddComments })=>{

    let approvalCommentsHandle = Meteor.subscribe('approval.comments', user._id);

    let commentLoading = !approvalCommentsHandle.ready();
    let approvalComments = null;

    //return comments based on staff role
    if (staffRole === 'Membership'){
        approvalComments = ApprovalComments.findOne({ applicantId: user._id });
    }else if(staffRole === 'Audit'){
        approvalComments = ApprovalComments.findOne({ applicantId: user._id },{
            fields: {
                "membership": 0,
                "exam": 0,
                "registry": 0
            }
        });
    }else if(staffRole === 'Exams'){
        approvalComments = ApprovalComments.findOne({ applicantId: user._id },{
            fields: {
                "membership": 0,
                "audit": 0,
                "registry": 0
            }
        });
    }else if(staffRole === 'Registry'){
        approvalComments = ApprovalComments.findOne({ applicantId: user._id },{
            fields: {
                "membership": 0,
                "exam": 0,
                "audit": 0
            }
        });
    }else{
        approvalComments = {}
    }

    return {
        user,
        showDocuments,
        showSingleDocument,
        loading,
        userFileList,
        doc,
        fileName,
        staffId,
        staffRole,
        commentLoading,
        approvalComments,
        showAddComments
    }

}, RenderPDF);