import React, {Component} from 'react';
import moment from 'moment';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { UploadStatistics } from '../../../api/users/collections';
import { Container, Row, Col, NavItem, NavLink } from 'reactstrap';
import Sidebar from '../Sidebar';
import AdminDashboardNavbar from './AdminDashboardNavbar';

import RegisterForm from '../user/RegisterForm';
import { FormTabs, StatusDisplay } from '../user/ProfileFormHelpers';

import { userSignup, saveArchiveUserAndStatistics } from '../../../api/users/methods';

const UploadStatsDisplayForm = ({ uploadStatistics, count, loading }) => {
    let values = [];

    uploadStatistics.map(stats => {
        values.push({
            label: stats.memberName,
            icon: "fa-user",
            color: "#7ACB58",
            data: stats.dateUploaded.toDateString(),
            key: stats.memberId
        })
    });

    return (
        loading ?
            <div></div>
        : <StatusDisplay
                title={`Members added today: ${new Date().toDateString()}`}
                subtitle={`Total Uploaded: ${count}`}
                values={values}
            />
    )
}

const UploadStatsDisplay = createContainer(({ params }) => {

    const uploadStats = Meteor.subscribe('uploadStatistics.user', Meteor.userId());
    const loading = !uploadStats.ready();

    const uploadStatistics = UploadStatistics.find({
        staffId: Meteor.userId(),
    }).fetch();

    const count = uploadStatistics ? uploadStatistics.length : 0;

    return {
        loading,
        uploadStatistics,
        count
    };

}, UploadStatsDisplayForm);

class ArchivedApplicantForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            addMember: false,
            registerMember: false,
            user: '',
            userApplication: '',
            fields: {
                acceptDeclaration: '',
                username: '',
                email: '',
                password: '',
                confirmPassword: '',

                firstName: '',
                middleName: '',
                lastName: '',
                phone: '',
                gender: '',
                maritalStatus: '',
                dob: moment(),
                privateAddress: '',
                stateOfOrigin: '',
                nationality: '',
                isRegistering: false
            }
        };


        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleRegistration = this.handleRegistration.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resetState = this.resetState.bind(this);
        this.completeRegistration = this.completeRegistration.bind(this);

        this.handleUserApplicationInputChange = this.handleUserApplicationInputChange.bind(this);
        this.handleProfessionalExam = this.handleProfessionalExam.bind(this);
        this.handleTrainingEnd = this.handleTrainingEnd.bind(this);
        this.handleTrainingStart = this.handleTrainingStart.bind(this);
        this.handleAppointmentDate = this.handleAppointmentDate.bind(this);
        this.saveUserApplication = this.saveUserApplication.bind(this);

        this.createMenu = this.createMenu.bind(this);
        this.addMember = this.addMember.bind(this);
    }

    createMenu() {
        if(this.state.addMember){

            if ( this.state.registerMember == true ) {
               return (
                   <span>
                        <NavItem>
                            <NavLink
                                href="#"
                                className="btn btn-default"
                                onClick={this.addMember}
                                style={{minWidth: 100}}
                            >
                                Cancel
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                href="#"
                                className="btn btn-danger"
                                onClick={this.handleFormSubmitted}
                                style={{minWidth: 100}}
                            >
                                Save
                            </NavLink>
                        </NavItem>
                    </span>
               )
            }else if (this.state.registerMember == false) {
                return (
                    <NavItem>
                        <NavLink
                            href="#"
                            className="btn btn-default"
                            onClick={this.addMember}
                            style={{minWidth: 100}}
                        >
                            Cancel
                        </NavLink>
                    </NavItem>
                );
            }
        }else {
            if ( this.state.registerMember == false ){
                return (
                    <NavItem>
                        <NavLink
                            href="#"
                            className="btn btn-primary"
                            onClick={() => this.setState({ addMember: true })}
                            style={{minWidth: 100}}
                        >
                            <i className="fa fa-plus" style={{paddingRight: 10, fontSize: 15}}></i>
                            Add Member
                        </NavLink>
                    </NavItem>
                );
            } else if ( this.state.registerMember == true ) {
                return (
                    <span>
                        <NavItem>
                            <NavLink
                                href="#"
                                className="btn btn-default"
                                onClick={this.addMember}
                                style={{minWidth: 100}}
                            >
                                Cancel
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                href="#"
                                className="btn btn-danger"
                                onClick={this.handleSubmit}
                                style={{minWidth: 100}}
                            >
                                Save
                            </NavLink>
                        </NavItem>
                    </span>
                )
            }

        }
    }

    addMember() {
        this.setState({
            addMember: false,
            registerMember: false
        });
    }

    handleInputChange(evt)
    {
        const fields = this.state.fields;
        fields[evt.target.name] = evt.target.value;
        this.setState({ fields });
    }

    handleDateChange(date)
    {
        const fields = this.state.fields;
        fields.dob = date.toDate();
        this.setState({ fields });
    }

    handleProfessionalExam(date)
    {
        const ua = this.state.userApplication;
        ua.ananProfessionalExamDate = date.toDate();
        this.setState({ ua });
    }

    handleTrainingStart(date)
    {
        const ua = this.state.userApplication;
        ua.ananTrainingStartDate = date.toDate();
        this.setState({ ua });
    }

    handleTrainingEnd(date)
    {
        const ua = this.state.userApplication;
        ua.ananTrainingEndDate = date.toDate();
        this.setState({ ua });
    }

    handleAppointmentDate(date)
    {
        const ua = this.state.userApplication;
        ua.appointmentDate = date.toDate();
        this.setState({ ua });
    }

    handleUserApplicationInputChange(evt)
    {
        const userApplication = this.state.userApplication;
        userApplication[evt.target.name] = evt.target.value;
        this.setState({ userApplication });
    }

    completeRegistration(u, ua) {
        this.resetState();
        this.setState({
            user: u,
            userApplication: ua,
            addMember: !this.state.addMember,
            registerMember: true
        });
    }

    handleRegistration(e)
    {
        e.preventDefault();
        //set the isRegistering field
        const fields = this.state.fields;

        fields.isRegistering = !fields.isRegistering;
        this.setState({ fields });

        let userData = this.state.fields;

        //validate the form
        let emails = [];
        let user_email = { address: userData.email, verified: false };
        emails.push(user_email);
        userData.emails = emails;

        //exclude isRegistering from fields submitted to server
        let { isRegistering, ...userDataSubmit } = userData;

        userSignup.call(userDataSubmit,  (message, user) => {
            if (message) {
                if(message.reason === 'Email already exists' || message.reason === 'Phone number already exists'){
                    this.resetState();
                }

                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(message.reason, 'Signup Error');
                //hide the pre-loader
                fields.isRegistering = !fields.isRegistering;
                this.setState({ fields });
                return;
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Registration Successful', 'Success');

                this.completeRegistration(user.u, user.ua);
                return;
            }
        });
    }

    saveUserApplication(userData, saveMessage) {
        saveArchiveUserAndStatistics.call(userData , (message) => {
            if (message) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(message.reason, 'Update Error');
                return;
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success(saveMessage, 'Success');
            }
        });
    }

    handleSubmit() {
        this.saveUserApplication(this.state.userApplication, 'Member Data Saved');

        this.setState({
            addMember: false,
            registerMember: false
        });
    }

    resetState() {
        this.setState({
            fields: {
                acceptDeclaration: '',
                username: '',
                email: '',
                password: '',
                confirmPassword: '',

                firstName: '',
                middleName: '',
                lastName: '',
                phone: '',
                gender: '',
                maritalStatus: '',
                dob: moment(),
                privateAddress: '',
                stateOfOrigin: '',
                nationality: '',
                isRegistering: false
            }
        })
    }

    renderFormTabs() {
        if (this.state.user._id && this.state.userApplication._id) {
            if (!!this.state.registerMember) {
                return (
                    <FormTabs
                        user={this.state.user}
                        userApplication={this.state.userApplication}

                        isEditing={true}
                        handleUserInputChange={this.handleUserInputChange}
                        handleUserApplicationInputChange={this.handleUserApplicationInputChange}
                        handleProfessionalExam={this.handleProfessionalExam}
                        handleTrainingEnd={this.handleTrainingEnd}
                        handleTrainingStart={this.handleTrainingStart}
                        handleAppointmentDate={this.handleAppointmentDate}
                    />
                );
            }
        }

        return <div></div>
    }

    render() {
        return (
            <div className="wrapper">
                <Sidebar />

                <Container fluid={true} className="main-panel">
                    <Row>
                        <AdminDashboardNavbar showAdminMenu={false}>
                            {this.createMenu()}
                        </AdminDashboardNavbar>
                    </Row>
                    <Row className="content" type="flex">
                        <Col sm={12} md={4}>
                            <UploadStatsDisplay />
                        </Col>
                        <Col sm={12} md={6}>
                            {
                                !!this.state.addMember === true ?
                                    <RegisterForm
                                        renderNew={false}
                                        acceptDeclaration={this.state.fields.acceptDeclaration}
                                        username={this.state.fields.username}
                                        email={this.state.fields.email}
                                        password={this.state.fields.password}
                                        confirmPassword={this.state.fields.confirmPassword}

                                        firstName={this.state.fields.firstName}
                                        lastName={this.state.fields.lastName}
                                        middleName={this.state.fields.middleName}
                                        phone={this.state.fields.phone}
                                        gender={this.state.fields.gender}
                                        maritalStatus={this.state.fields.maritalStatus}
                                        dob={this.state.fields.dob}
                                        privateAddress={this.state.fields.privateAddress}
                                        stateOfOrigin={this.state.fields.stateOfOrigin}
                                        nationality={this.state.fields.nationality}

                                        isRegistering={this.state.fields.isRegistering}
                                        handleInputChange={this.handleInputChange}
                                        handleDateChange={this.handleDateChange}
                                        handleRegistration={this.handleRegistration}
                                    />
                                :
                                    ''
                            }
                        </Col>
                        <Col sm={12} md={8}>
                            {
                                this.renderFormTabs()
                            }
                        </Col>
                    </Row>

                </Container>
            </div>
        );
    }
}

export default ArchivedApplicantForm;