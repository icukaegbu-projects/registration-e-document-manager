import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { ApplicantApprovals } from '../../../api/users/collections';
import { applicantApprovals, userAssignAnanNumber } from '../../../api/users/methods';

import Toggle from 'react-toggle';
import { Card, CardGroup, Input, InputGroup, InputGroupButton, Button} from 'reactstrap';

const ApprovalItem = ({ title, color, data, onChange, disabled, onClick }) => {
    return (
        <Card block inverse color={color} style={{padding: 2, paddingLeft: 4, backgroundColor: color, height: 160}}>
            <h6 style={{fontSize: 10}}>{title}</h6>
            <div>
                {/* Render Toggle if staff role === title && enable it using check*/}
                {
                  Roles.userIsInRole(Meteor.userId(), title) ?
                      <div>
                          <div>
                              <Toggle
                                  defaultChecked={data.isApproved}
                                  onChange={onChange}
                                  disabled={disabled || data.isApproved}
                                  name={title}
                              />
                          </div>
                          <div>
                              <button
                                  className="btn btn-fill btn-warning"
                                  onClick={onClick}
                                  disabled={disabled}
                              >
                                  <i className="fa fa-eye"></i>
                              </button>
                          </div>
                      </div>
                      :
                        <div style={{height: 65}}></div>
                }
                <p style={{fontSize: 10, fontWeight: 700, marginTop: 5, color: '#222'}}>
                    Approved:<br/>{data.isApproved ? moment(data.dateApproved).format('DD/MM/YY') : 'Pending' }
                </p>
            </div>
        </Card>
    );
};

class ApprovalComponentForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            applicantApproval: null,
            enabled: false,
            ananNumber: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onChangeInput = this.onChangeInput.bind(this);
        this.onSubmitAnanNumber = this.onSubmitAnanNumber.bind(this);
        this.renderApprovalItems = this.renderApprovalItems.bind(this);
        this.renderAnanNumber = this.renderAnanNumber.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
           applicantApproval: nextProps.applicantApproval
        });
    }

    onChange(evt) {
        let { applicantApproval } = this.state;
        let title = evt.target.name.toLowerCase();
        if (title === 'exams') title = 'exam';

        applicantApproval[title].isApproved = !applicantApproval[title].isApproved;

        if(applicantApproval[title].isApproved) {
            applicantApproval[title].approvedBy = Meteor.userId();
            applicantApproval[title].dateApproved = new Date();
        }else{
            applicantApproval[title].approvedBy = '';
            applicantApproval[title].dateApproved = null;
        }

        this.setState({
            applicantApproval: applicantApproval
        });

        //save to server
        applicantApprovals.call(applicantApproval,  (message) => {
            if (message) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(message.reason, 'Approval Failed');
                return;
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Approval Successful', 'Success');
                return;
            }
        });
    }

    onChangeInput(evt) {
        if (Number.isInteger(Number.parseInt(evt.target.value))) {
            this.setState({
                ananNumber: evt.target.value
            })
        }
    }

    onSubmitAnanNumber(evt) {
        evt.preventDefault();

        let user = {
            ananNumber: this.state.ananNumber,
            userId: this.props.applicant._id
        };

        userAssignAnanNumber.call(user, (message) => {
            if (message) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(message.reason, 'ANAN Number Allotment Failed');
                return;
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('ANAN Number Allotment Successful', 'Success');

                this.setState({ ananNumber: '' });
                return;
            }
        })
    }

    renderAnanNumber() {
        if (this.props.applicant.bio.ananNumber) {
            return (
                <div
                    style={{
                        height: 40,
                        backgroundColor: '#f57c00',
                        color: 'white',
                        fontWeight: 700,
                        fontSize: 20,
                        paddingTop: 7,
                        paddingLeft: 65,
                        marginBottom: 15
                    }}
                >
                    ANAN #: {this.props.applicant.bio.ananNumber}
                </div>
            )
        }
        return (
            <InputGroup>
                <Input
                    placeholder="Enter ANAN Number"
                    value={this.state.ananNumber}
                    onChange={this.onChangeInput}
                />
                <InputGroupButton>
                    <Button
                        className="btn-success btn-fill btn-sm"
                        disabled={this.state.ananNumber===''}
                        onClick={this.onSubmitAnanNumber}
                    >
                        <i className="fa fa-plus"></i>
                    </Button>
                    <Button
                        className="btn-primary btn-fill btn-sm"
                        disabled={this.state.ananNumber===''}
                        onClick={() => this.setState({ ananNumber: '' })}
                    >
                        <i className="fa fa-minus"></i>
                    </Button>
                </InputGroupButton>
            </InputGroup>
        )
    }

    renderAssignAnanNumber() {
        let { membership, exam, audit, registry } = this.props.applicantApproval;
        let checkApproval = membership.isApproved && exam.isApproved &&
                            audit.isApproved && registry.isApproved;

        return Roles.userIsInRole(Meteor.userId(), 'Membership') && checkApproval;
        // return Roles.userIsInRole(Meteor.userId(), 'Membership') && checkApproval ?
        //             this.renderAnanNumber()
        //         : <span></span>
    }

    renderApprovalItems() {
        let { membership, audit, exam, registry } = this.state.applicantApproval;

        return (
            <div>
                {
                    this.renderAssignAnanNumber() ?
                        this.renderAnanNumber() :
                        <CardGroup style={{height: 50}}>
                            {/*<Row style={{padding: 0}}>*/}
                            <ApprovalItem
                                title="Membership"
                                color="#4fc3f7"
                                onClick={this.props.showDocuments.bind(null, this.props.applicant)}
                                data={membership}
                                onChange={this.onChange}
                                disabled={false}
                            />
                            <ApprovalItem
                                title="Exams"
                                color="#9575cd"
                                onClick={this.props.showDocuments.bind(null, this.props.applicant)}
                                data={exam}
                                onChange={this.onChange}
                                disabled={!membership.isApproved}
                            />
                            <ApprovalItem
                                title="Audit"
                                color="#ff7043"
                                onClick={this.props.showDocuments.bind(null, this.props.applicant)}
                                data={audit}
                                onChange={this.onChange}
                                disabled={!(membership.isApproved && exam.isApproved)}
                            />
                            <ApprovalItem
                                title="Registry"
                                color="#8bc34a"
                                onClick={this.props.showDocuments.bind(null, this.props.applicant)}
                                data={registry}
                                onChange={this.onChange}
                                disabled={!(membership.isApproved && exam.isApproved && audit.isApproved)}
                            />
                            {/*</Row>*/}
                        </CardGroup>
                }

            </div>
        );
    }

    render() {
        return (
            <div ref={(div) => this.containerComponent = div}>
                {
                    this.props.loading ?
                        <CardGroup>
                            {/*Loading...*/}
                        </CardGroup>
                        :
                        this.renderApprovalItems()
                }
            </div>
        );
    }
}

ApprovalComponentForm.propTypes = {
    showDocuments: PropTypes.func.isRequired,
    applicant: PropTypes.object,
    applicantApproval: PropTypes.object,
    loading: PropTypes.bool,
};

export default ApprovalComponent = createContainer(({ params, applicant, showDocuments }) => {
    const applicantApprovalStatus = Meteor.subscribe('applicantApproval.user', applicant._id);
    const loading = !applicantApprovalStatus.ready();

    const applicantApproval = ApplicantApprovals.findOne({ applicantId: applicant._id });

    return {
        applicant,
        applicantApproval,
        loading,
        showDocuments
    }
}, ApprovalComponentForm);