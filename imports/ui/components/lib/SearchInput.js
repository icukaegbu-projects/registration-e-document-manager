/**
 * Created by ikedi on 26/03/2017.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { InputFormComponent, SelectFormComponent } from '../user/RegisterFormHelpers';
import { Card, CardTitle, CardBlock, Row, Col, Button } from 'reactstrap';

export default class SearchInput extends Component {
    constructor() {
        super();

        this.state = {
            showSearch: false,
            fields: {
                searchType: "None",
                lastName: "",
                firstName: "",
                ananNumberStart: "",
                ananNumberEnd: "",
                numResults: "20"
            }
        };

        this.onChange = this.onChange.bind(this);
        this.toggle = this.toggle.bind(this);
        this.displaySearchButton = this.displaySearchButton.bind(this);
    }

    onChange(e) {
        let { fields } = this.state;
        fields[e.target.name] = e.target.value;
        this.setState({ fields });
    }

    toggle() {
        this.setState({ showSearch: !this.state.showSearch });
    }

    displaySearchButton() {
        return (
            this.state.showSearch ?
                <button className="btn btn-danger btn-outline-danger" onClick={this.toggle}>
                    <i className="fa fa-minus"></i>
                </button>
                :
                <button className="btn btn-primary btn-outline-primary" onClick={this.toggle}>
                    <i className="fa fa-plus"></i>
                </button>
        )
    }

    render() {
        return (
            <Card style={{padding: 0}}>
                <CardTitle style={{paddingLeft: 20}}>
                    <span style={{paddingRight: 20}}>Search</span>
                    {this.displaySearchButton()}
                </CardTitle>
                {
                    this.state.showSearch ?
                        <CardBlock>
                            <Row>
                                <Col md={2} sm={12}>
                                    <SelectFormComponent
                                        values={ this.props.showAnanNumber ? ["None","Name", "Anan#"] : ["None", "Name"]}
                                        name="searchType"
                                        onChange={this.onChange}
                                        value={this.state.fields.searchType}
                                    />
                                </Col>
                                {
                                    this.state.fields.searchType === "Name" ?
                                        <div>
                                            <Col md={3} sm={12}>
                                                <InputFormComponent
                                                    placeholder="Last Name"
                                                    name="lastName"
                                                    onChange={this.onChange}
                                                    value={this.state.fields.lastName}
                                                />
                                            </Col>
                                            <Col md={3} sm={12}>
                                                <InputFormComponent
                                                    placeholder="First Name"
                                                    name="firstName"
                                                    onChange={this.onChange}
                                                    value={this.state.fields.firstName}
                                                />
                                            </Col>
                                        </div>
                                        : null
                                }
                                {
                                    this.state.fields.searchType === "Anan#" ?
                                        <div>
                                            <Col md={3} sm={12}>
                                                <InputFormComponent
                                                    placeholder="ANAN # (Single or Start)"
                                                    name="ananNumberStart"
                                                    onChange={this.onChange}
                                                    value={this.state.fields.ananNumberStart}
                                                />
                                            </Col>
                                            <Col md={3} sm={12}>
                                                <InputFormComponent
                                                    placeholder="ANAN # (Single or Start)"
                                                    name="ananNumberEnd"
                                                    onChange={this.onChange}
                                                    value={this.state.fields.ananNumberEnd}
                                                />
                                            </Col>
                                        </div>
                                        : null
                                }
                                <Col md={2} sm={12}>
                                    <SelectFormComponent
                                        values={["No of Results", "20","50","75","100"]}
                                        name="numResults"
                                        onChange={this.onChange}
                                        value={this.state.fields.numResults}
                                    />
                                </Col>
                                <Col md={{size:1, offset:1}} sm={12}>
                                    <Button
                                        onClick={this.props.onClick.bind(null, this.state.fields)}
                                    >
                                        <i className="fa fa-search"></i>
                                    </Button>
                                </Col>
                            </Row>
                        </CardBlock>
                        : null
                }
            </Card>
        )
    }
}

SearchInput.propTypes = {
    onClick: PropTypes.func.isRequired,
    showAnanNumber: PropTypes.bool
};

SearchInput.defaultProps = {
    showAnanNumber: true
};