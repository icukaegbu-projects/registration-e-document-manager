/**
 * Created by ikedi on 22/01/2017.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PDF from 'react-pdf-js';

class PDFViewer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: 0,
            pages: 0
        };

        this.onDocumentComplete = this.onDocumentComplete.bind(this);
        this.onPageComplete = this.onPageComplete.bind(this);
        this.handlePrevious = this.handlePrevious.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.renderPagination = this.renderPagination.bind(this);
        this.renderDocs = this.renderDocs.bind(this);
    }

    onDocumentComplete(pages) {
        this.setState({ page: 1, pages });
    }

    onPageComplete(page) {
        this.setState({ page });
    }

    handlePrevious() {
        this.setState({ page: this.state.page - 1 });
    }

    handleNext() {
        this.setState({ page: this.state.page + 1 });
    }

    renderPagination(page, pages) {
        let previousButton = <a href="#" className="previous btn btn-default" onClick={this.handlePrevious}><i className="fa fa-arrow-left"></i> Previous</a>;
        if (page === 1) {
            previousButton = <a href="#" className="previous disabled btn btn-default"><i className="fa fa-arrow-left"></i> Previous</a>;
        }

        let nextButton = <a href="#" className="next btn btn-primary" onClick={this.handleNext}>Next <i className="fa fa-arrow-right"></i></a>;
        if (page === pages) {
            nextButton = <a href="#" className="next disabled btn btn-primary">Next <i className="fa fa-arrow-right"></i></a>;
        }
        return (
            <nav
                style={{
                    marginTop: 20,
                    marginBottom: 20,
                    paddingTop: 10,
                    paddingBottom: 10
                }}
            >
                {previousButton}
                <span style={{ paddingLeft: 10, paddingRight: 10}}>|</span>
                {nextButton}
            </nav>
        );
    }
    
    renderDocs() {
        var obj_url = window.URL.createObjectURL(this.props.doc);
        var iframe = <iframe id="viewer" />;
        iframe.setAttribute('src', obj_url);
        window.URL.revokeObjectURL(obj_url);

        return (
            iframe
        )
    }

    render() {
        let pagination = null;
        if (this.state.pages) {
            pagination = this.renderPagination(this.state.page, this.state.pages);
        }

        return (
            <div>
                {
                    this.props.doc ?
                        <div>
                            {console.log(this.props.doc)}
                            <h4>File: <strong>{this.props.fileName}</strong></h4>
                            {pagination}
                            <PDF
                                file={this.props.doc}
                                onDocumentComplete={this.onDocumentComplete}
                                onPageComplete={this.onPageComplete}
                                page={this.state.page}
                                style={{
                                    height: 850,
                                    width: 700
                                }}
                            />
                        </div>:
                        <div>Document not found</div>
                }
            </div>
        );
    }
}

PDFViewer.propTypes = {
    doc: PropTypes.object.isRequired,
    fileName: PropTypes.string
}

//module.exports = PDFViewer;
export default PDFViewer;