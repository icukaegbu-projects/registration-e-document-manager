/**
 * Created by ikedi on 13/02/2017.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FileUploadCard2 from './FileUploadCard2';
import {_} from 'meteor/underscore';

import { Row, Col } from 'reactstrap';

class FileUploadForm2 extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        //PUT THIS IN A COLLECTION, ADD AN isUploaded PROPERTY TO IT TO USE AND
        //CHECK IF IT HAS BEEN UPLOADED; IF UPLOADED, HIDE THE UPLOAD BUTTON
        //ALSO STORE THE ID OF THE OWNER
        //ALSO STORE THE DROPBOX FILENAME SO THAT IT CAN BE USED LATER TO DELETE THE FILE
        const colors = ["blue","green","yellow","brown","purple","orange","blue","green","yellow","brown", "purple","orange", "blue"];

        return (
            <div>
                <h4>DOCUMENTS</h4>
                <Row className="coloured-cards">
                    {
                        this.props.userApplication.files.map((file, idx) => {
                            console.log(file)
                            return (
                                <Col key={idx} md={6} sm={12}>
                                    <FileUploadCard2
                                        color={colors[idx]}
                                        title={file.title}
                                        user={this.props.user}
                                        isUploaded={file.isUploaded}
                                        nameFormat={file.nameFormat}
                                        dropboxPath={file.dropboxPath}
                                    />
                                </Col>
                            );
                        })
                    }
                </Row>
            </div>
        )
    }
}

FileUploadForm2.propTypes = {
    isEditing: PropTypes.bool.isRequired,
    user: PropTypes.object.isRequired,
    userApplication: PropTypes.object.isRequired
};

export default FileUploadForm2;