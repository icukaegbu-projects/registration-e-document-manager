 /**
 * Created by ikedi on 12/12/2016.
 */
import React from 'react';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

import { Form, FormGroup, Input, Row, Col, Button, Label, FormFeedback } from 'reactstrap';
import DateTime from 'react-datetime';

const STATES = ["State of Origin", "Abia", "Adamawa", "Anambra", "Akwa Ibom", "Bauchi", "Bayelsa",
    "Benue", "Borno", "Cross River", "Delta", "Ebonyi", "Enugu", "Edo", "Ekiti",
    "Gombe", "Imo", "Jigawa", "Kaduna", "Kano", "Katsina", "Kebbi", "Kogi",
    "Kwara", "Lagos", "Nasarawa", "Niger", "Ogun", "Ondo", "Osun", "Oyo",
    "Plateau", "Rivers", "Sokoto", "Taraba", "Yobe", "Zamfara"];

const RELATIONSHIPS = ["Relationship", "Father", "Mother", "Brother", "Sister", "Uncle",
    "Aunty", "Husband", "Wife", "Son", "Daughter", "Inlaw", "Other"];

const MEMBERSHIP_STATUS = ["APPLICANT", "CNA", "FCNA", "EXPELLED", "DECEASED"];

export const InputFormComponent = ({icon="user", placeholder, type = "text",
                            renderLabel = false, name, value = "", onChange, isLoggingIn, error}) => {
    const textarea = (
        renderLabel ?
            <FormGroup>
                <Label for={name}>{placeholder}</Label>
                <Input
                    id={name}
                    placeholder={placeholder}
                    type="textarea"
                    rows={3}
                    name={name}
                    value={value}
                    onChange={onChange}
                    disabled={isLoggingIn}
                    state="danger"
                    style={{
                        marginBottom: '5px',
                        height: '80px'
                    }}
                />
            </FormGroup>
            :
            <Input
                id={name}
                placeholder={placeholder}
                type="textarea"
                rows={3}
                name={name}
                value={value}
                onChange={onChange}
                disabled={isLoggingIn}
                state="danger"
                style={{
                    marginBottom: '5px',
                    height: '80px'
                }}
            />
    );

    const input = (
        renderLabel ?
            <FormGroup>
                <Label for={name}>{placeholder}</Label>
                <Input
                    id={name}
                    placeholder={placeholder}
                    type={type}
                    name={name}
                    value={value}
                    onChange={onChange}
                    state="danger"
                />
            </FormGroup>
            :
            <Input
                id={name}
                placeholder={placeholder}
                type={type}
                name={name}
                value={value}
                onChange={onChange}
                state="danger"
            />
    );

    if (type === "textarea") {
        return (
            <div>
                {textarea}
                <FormFeedback style={{fontSize: 12, fontWeight: 900, color: 'red', paddingRight: 10}}>
                   {error}
                </FormFeedback>
            </div>

        )
    }
    else {
        return (
            <div>
                {input}
                <FormFeedback style={{fontSize: 12, fontWeight: 900, color: 'red', paddingRight: 10}}>
                   {error}
                </FormFeedback>
            </div>

        )
    }


};

export const SelectFormComponent = ({values = [], placeholder, renderLabel = false, name, value, onChange, error }) => {
    return (
        renderLabel ?
            <FormGroup>
                <Label>{placeholder}</Label>
                <Input
                    type="select"
                    placeholder={placeholder}
                    name={name}
                    value={value}
                    onChange={onChange}
                    style={{
                        marginBottom: '5px'
                    }}>
                    {values.map(item => <option key={item} value={item}>{item}</option> )}
                </Input>
                <FormFeedback style={{fontSize: 12, fontWeight: 900, color: 'red', paddingRight: 10}}>
                    {error}
                </FormFeedback>
            </FormGroup>
            :
            <FormGroup>
                <Input
                    type="select"
                    placeholder={placeholder}
                    name={name}
                    value={value}
                    onChange={onChange}
                    style={{
                        marginBottom: '5px'
                    }}>
                    {values.map(item => <option key={item} value={item}>{item}</option> )}
                </Input>
                <FormFeedback style={{fontSize: 12, fontWeight: 900, color: 'red', paddingRight: 10}}>
                    {error}
                </FormFeedback>
            </FormGroup>
    );
};
//
export const DateFormComponent = ({ placeholder, renderLabel = false, onChange, value, name, isEditing, error }) => {
    return (
        renderLabel ?
            <FormGroup>
                <Label>{placeholder}</Label>
                <br/>
                <DateTime
                    inputProps={{
                        placeholder: placeholder,
                        name: name
                    }}
                    closeOnSelect={true}
                    timeFormat={false}
                    onChange={onChange}
                    name={name}
                    value={value}
                />
                <FormFeedback style={{fontSize: 12, fontWeight: 900, color: 'red', paddingRight: 10}}>
                    {error}
                </FormFeedback>
            </FormGroup>
            :
            <FormGroup>
                <DateTime
                    inputProps={{placeholder: placeholder, name: name}}
                    closeOnSelect={true}
                    timeFormat={false}
                    onChange={onChange}
                    name={name}
                    value={value}
                />
                <FormFeedback style={{fontSize: 12, fontWeight: 900, color: 'red', paddingRight: 10}}>
                    {error}
                </FormFeedback>
            </FormGroup>
    );
};

export const StyledDivider = () => {
    return (
        <div className="division">
            <div className="line l"></div>
            <span>|</span>
            <div className="line r"></div>
        </div>
    );
};

export const AnanDisplay = () => {
    return (
        <div className="social">
            <button href="#" className="btn btn-fill btn-icon btn-facebook">A</button>
            <button href="#" className="btn btn-fill btn-icon btn-google">N</button>
            <button href="#" className="btn btn-fill btn-icon btn-twitter">A</button>
            <button href="#" className="btn btn-fill btn-icon btn-magnify">N</button>
        </div>
    );
};

export const LoginFormDetails = ({email, password, loginUser, handleInputChange, resetState, isLoggingIn}) => {
    return (
        <Form className="register-form" onSubmit={loginUser}>
            <Row>
                {isLoggingIn ? <div className="loader">Loading...</div> : ''}
            </Row>

            <Row>
                <Col md={12} sm={12}>
                    <InputFormComponent
                        placeholder="Username/Email"
                        icon="edit"
                        name="email"
                        onChange={handleInputChange}
                        value={email}
                        disabled={isLoggingIn}
                    />
                </Col>
                <Col md={12} sm={12}>
                    <InputFormComponent
                        placeholder="Password"
                        type="password"
                        icon="lock"
                        name="password"
                        onChange={handleInputChange}
                        value={password}
                        disabled={isLoggingIn}
                    />
                </Col>
            </Row>

            <Button className="btn btn-fill btn-block" onClick={loginUser} disabled={isLoggingIn}>Login</Button>
            <div className="login">
                <p>Don't have an account? <a href={FlowRouter.path('register')}>Register</a>.</p>
            </div>
        </Form>
    );
};
//
export const LoginDetails = ({ username, email, password, confirmPassword, handleInputChange, errors }) => {

    return (
        <div>
            <Row>
                <Col md={12} sm={12}>
                    <InputFormComponent
                        placeholder="Username"
                        icon="edit"
                        name="username"
                        onChange={handleInputChange}
                        value={username}
                        error={errors ? errors.username : null}
                    />
                </Col>
                <Col md={12} sm={12}>
                    <InputFormComponent
                        placeholder="Email"
                        type="email"
                        name="email"
                        onChange={handleInputChange}
                        value={email}
                        error={errors ? errors.email : null}
                    />
                </Col>
                <Col md={12} sm={12}>
                    <InputFormComponent
                        placeholder="Password"
                        type="password"
                        icon="lock"
                        name="password"
                        onChange={handleInputChange}
                        value={password}
                        error={errors ? errors.password : null}
                    />
                </Col>
                <Col md={12} sm={12}>
                    <InputFormComponent
                        placeholder="Confirm Password"
                        type="password"
                        icon="lock"
                        name="confirmPassword"
                        onChange={handleInputChange}
                        value={confirmPassword}
                        error={errors ? errors.confirmPassword : null}
                    />
                </Col>
            </Row>
        </div>
    );
};

 export const ChangePasswordForm = ({ oldPassword, newPassword, confirmPassword, handleInputChange, onClick }) => {

     return (
         <div>
             <Row>
                 <Col md={12} sm={12}>
                     <InputFormComponent
                         placeholder="Old Password"
                         type="password"
                         icon="lock"
                         name="oldPassword"
                         onChange={handleInputChange}
                         value={oldPassword}
                     />
                     <br/>
                 </Col>
                 <Col md={12} sm={12}>
                     <InputFormComponent
                         placeholder="New Password"
                         type="password"
                         icon="lock"
                         name="newPassword"
                         onChange={handleInputChange}
                         value={newPassword}
                     />
                     <br/>
                 </Col>
                 <Col md={12} sm={12}>
                     <InputFormComponent
                         placeholder="Confirm Password"
                         type="password"
                         icon="lock"
                         name="confirmPassword"
                         onChange={handleInputChange}
                         value={confirmPassword}
                     />
                     <br/>
                 </Col>
             </Row>
             <Row>
                 <Col md={6} sm={12} style={{marginBottom: 20}}>
                     <button className="btn btn-danger" onClick={onClick}>Change Password</button>
                 </Col>
             </Row>
         </div>
     );
 };
//
export const Declaration = ({ acceptDeclaration, handleInputChange }) => {
    return (
        <div style={{
            background: 'rgba(0,0,0, 0.6)',
            padding: '20px'
        }}>
            <div className="info info-horizontal">
                <div className="description">
                    <h3> ANAN DECLARATION </h3>
                    <p style={{fontSize: '18px'}}>
                        I, having completed the Professional Examination of the Association of National
                        Accountants of Nigeria at the Nigerian College of Accountancy, and satisfied the
                        prescribed accountancy practical experience under the accountant-in-training programme;
                        being eligible and qualified thereunder, desire to be admitted a member of the
                        Association, and undertake, if elected, to be bound by the provisions of the Decree No 76
                        of 1993, CAP A26 LFN, 2004 and the regulations and Rules of the Association for the time
                        being in force.
                    </p>
                </div>
            </div>
            <div className="info info-horizontal">
                <div className="description">
                    <p style={{fontSize: '18px'}}>
                        I affirm that the particulars given below are true to the best of my knowledge and belief,
                        and I undertake to produce (if required) original documents or references in support of this
                        application, and to pay the requisite application and registration fees as attached.
                    </p>
                </div>
            </div>
            <div className="info info-horizontal">
                <div className="description">
                    <h3> Confirm Declaration </h3>
                    <SelectFormComponent
                        values={['Select an option', 'I agree', 'I disagree']}
                        name="acceptDeclaration"
                        onChange={handleInputChange}
                        value={acceptDeclaration}
                    />
                </div>
            </div>
        </div>
    )
}

//Staff BioData
export const StaffBioData = ({ renderLabel, firstName, lastName, middleName, phone, gender,
    maritalStatus, role, privateAddress, stateOfOrigin, isEditing, handleInputChange, errors }) => {

    return (
        <div>
            <Row>
                <Col sm={12} md={12}>
                    <SelectFormComponent
                        values={["Select Role", "Admin", "Supervisor", "DataCapture", "Audit", "Exams", "Membership", "Registry"]}
                        placeholder="Role"
                        renderLabel={renderLabel}
                        name="role"
                        onChange={isEditing ? handleInputChange : ''}
                        value={role}
                    />
                </Col>
            </Row>
            <hr/>
            <Row>
               <Col sm={12} md={12}>
                   <InputFormComponent
                       placeholder="Last Name"
                       renderLabel={renderLabel}
                       name="lastName"
                       onChange={isEditing ? handleInputChange : ''}
                       value={lastName}
                   />
               </Col>
                <Col sm={12} md={12}>
                    <InputFormComponent
                        placeholder="First Name"
                        renderLabel={renderLabel}
                        name="firstName"
                        onChange={isEditing ? handleInputChange : ''}
                        value={firstName}
                    />
                </Col>
                <Col sm={12} md={12}>
                    <InputFormComponent
                        placeholder="Middle Name / Initials"
                        renderLabel={renderLabel}
                        name="middleName"
                        onChange={isEditing ? handleInputChange : ''}
                        value={middleName}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={6}>
                    <InputFormComponent
                        placeholder="Tel No. / GSM"
                        type="tel"
                        icon="phone"
                        renderLabel={renderLabel}
                        name="phone"
                        onChange={isEditing ? handleInputChange : ''}
                        value={phone}
                    />
                </Col>
                <Col sm={12} md={6}>
                    <SelectFormComponent
                        values={["Marital Status", "Single", "Married", "Widowed", "Divorced"]}
                        placeholder="Marital Status"
                        renderLabel={renderLabel}
                        name="maritalStatus"
                        onChange={isEditing ? handleInputChange : ''}
                        value={maritalStatus}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={6}>
                    <SelectFormComponent
                        values={["Gender", "Male", "Female"]}
                        placeholder="Gender"
                        renderLabel={renderLabel}
                        name="gender"
                        onChange={isEditing ? handleInputChange : ''}
                        value={gender}
                    />
                </Col>
                <Col sm={12} md={6}>
                    <SelectFormComponent
                        values={STATES}
                        placeholder="State of Origin"
                        renderLabel={renderLabel}
                        name="stateOfOrigin"
                        onChange={isEditing ? handleInputChange : ''}
                        value={stateOfOrigin}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={12}>
                    <InputFormComponent
                        placeholder="Postal/Contact Address"
                        type="textarea"
                        renderLabel={renderLabel}
                        name="privateAddress"
                        onChange={isEditing ? handleInputChange : ''}
                        value={privateAddress}
                    />
                </Col>
            </Row>
        </div>
    );
};

//Applicant BioData
export const BioData = ({ renderLabel, firstName, lastName, middleName, phone, secondPhone, gender,
    maritalStatus, dob, privateAddress, stateOfOrigin, ananBranch,
    nationality, isEditing, handleInputChange, handleDateChange, errors }) => {

    return (
        <div>
            <Row>
                <Col sm={12} md={12}>
                    <InputFormComponent
                        placeholder="Last Name"
                        renderLabel={renderLabel}
                        name="lastName"
                        onChange={isEditing ? handleInputChange : null}
                        value={lastName}
                        error={errors ? errors.lastName : null}
                    />
                </Col>
                <Col sm={12} md={12}>
                    <InputFormComponent
                        placeholder="First Name"
                        renderLabel={renderLabel}
                        name="firstName"
                        onChange={isEditing ? handleInputChange : null}
                        value={firstName}
                        error={errors ? errors.firstName : null}
                    />
                </Col>
                <Col sm={12} md={12}>
                    <InputFormComponent
                        placeholder="Middle Name / Initials"
                        renderLabel={renderLabel}
                        name="middleName"
                        onChange={isEditing ? handleInputChange : null}
                        value={middleName}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={6}>
                    <InputFormComponent
                        placeholder="Tel No. / GSM"
                        type="tel"
                        icon="phone"
                        renderLabel={renderLabel}
                        name="phone"
                        onChange={isEditing ? handleInputChange : null}
                        value={phone}
                        error={errors ? errors.phone : null}
                    />
                </Col>
                <Col sm={12} md={6}>
                    <InputFormComponent
                        placeholder="Second Tel No. / GSM"
                        type="tel"
                        icon="phone"
                        renderLabel={renderLabel}
                        name="secondPhone"
                        onChange={isEditing ? handleInputChange : null}
                        value={secondPhone}
                        error={errors ? errors.secondPhone : null}
                    />
                </Col>

            </Row>
            <Row>
                <Col sm={12} md={6}>
                    <InputFormComponent
                        placeholder="Branch (STATE/CITY)"
                        renderLabel={renderLabel}
                        name="ananBranch"
                        onChange={isEditing ? handleInputChange : null}
                        value={ananBranch}
                    />
                </Col>
                <Col sm={12} md={6}>
                    {
                        isEditing ?
                            <DateFormComponent
                                placeholder="Date of Birth"
                                renderLabel={renderLabel}
                                name="dob"
                                onChange={handleDateChange}
                                value={dob}
                            />
                            :
                            <InputFormComponent
                                placeholder="Date of Birth"
                                renderLabel={renderLabel}
                                value={dob.toDateString()}
                                disabled={true}
                            />
                    }
                </Col>

            </Row>
            <Row>
                <Col sm={12} md={6}>
                    <SelectFormComponent
                        values={["Gender", "Male", "Female"]}
                        placeholder="Gender"
                        renderLabel={renderLabel}
                        name="gender"
                        onChange={isEditing ? handleInputChange : null}
                        value={gender}
                        error={errors ? errors.gender : null}
                    />
                </Col>
                <Col sm={12} md={6}>
                    <SelectFormComponent
                        values={["Marital Status", "Single", "Married", "Widowed", "Divorced"]}
                        placeholder="Marital Status"
                        renderLabel={renderLabel}
                        name="maritalStatus"
                        onChange={isEditing ? handleInputChange : null}
                        value={maritalStatus}
                        error={errors ? errors.maritalStatus : null}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={6}>
                    <InputFormComponent
                        placeholder="Nationality"
                        icon="environment"
                        renderLabel={renderLabel}
                        name="nationality"
                        onChange={isEditing ? handleInputChange : null}
                        value={nationality}
                        error={errors ? errors.nationality : null}
                    />
                </Col>
                <Col sm={12} md={6}>
                    <SelectFormComponent
                        values={STATES}
                        placeholder="State of Origin"
                        renderLabel={renderLabel}
                        name="stateOfOrigin"
                        onChange={isEditing ? handleInputChange : null}
                        value={stateOfOrigin}
                        error={errors ? errors.stateOfOrigin : null}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={12}>
                    <InputFormComponent
                        placeholder="Postal/Contact Address"
                        type="textarea"
                        renderLabel={renderLabel}
                        name="privateAddress"
                        onChange={isEditing ? handleInputChange : null}
                        value={privateAddress}
                    />
                </Col>
            </Row>
        </div>
    );
};

export const NextOfKinProfessional = ({ nextOfKinName, nextOfKinPhone, nextOfKinAddress,
    nextOfKinRelationship, tertiaryInstitutionAttended,
    qualificationsWithDates, ananProfessionalExamDate, ananTrainingStartDate,
    ananTrainingEndDate, presentEmployer, employerAddress, positionHeld,
    appointmentDate, isEditing, handleUserApplicationInputChange, handleProfessionalExam,
    handleTrainingEnd, handleTrainingStart, handleAppointmentDate, renderLabel }) => {

    return (
        <div>
            <h4>NEXT OF KIN</h4>
            <Row>
                <Col md={12} sm={12}>
                    <InputFormComponent
                        placeholder="Next of Kin"
                        renderLabel={renderLabel}
                        name="nextOfKinName"
                        value={nextOfKinName}
                        onChange={isEditing ? handleUserApplicationInputChange : null}
                    />
                </Col>
            </Row>
            <Row>
                <Col md={6} sm={12}>
                    <InputFormComponent
                        placeholder="Next of Kin Phone"
                        type="tel"
                        icon="phone"
                        renderLabel={renderLabel}
                        name="nextOfKinPhone"
                        value={nextOfKinPhone}
                        onChange={isEditing ? handleUserApplicationInputChange : null}
                    />
                </Col>
                <Col md={6} sm={12}>
                    <SelectFormComponent
                        values={RELATIONSHIPS}
                        placeholder="Relationship to Applicant"
                        renderLabel={renderLabel}
                        name="nextOfKinRelationship"
                        value={nextOfKinRelationship}
                        onChange={isEditing ? handleUserApplicationInputChange : null}
                    />
                </Col>
            </Row>
            <Row>
                <Col md={12} sm={12}>
                    <InputFormComponent
                        placeholder="Next of Kin Address"
                        type="textarea"
                        renderLabel={renderLabel}
                        name="nextOfKinAddress"
                        value={nextOfKinAddress}
                        onChange={isEditing ? handleUserApplicationInputChange : null}
                    />
                </Col>
            </Row>

            <hr/>
            <h4>EDUCATION BACKGROUND</h4>
            <InputFormComponent
                placeholder="Tertiary Institutions Attended"
                type="textarea"
                renderLabel={renderLabel}
                name="tertiaryInstitutionAttended"
                value={tertiaryInstitutionAttended}
                onChange={isEditing ? handleUserApplicationInputChange : null}
            />
            <InputFormComponent
                placeholder="Qualifications with Dates"
                type="textarea"
                renderLabel={renderLabel}
                name="qualificationsWithDates"
                value={qualificationsWithDates}
                onChange={isEditing ? handleUserApplicationInputChange : null}
            />
            <hr/>
            <h4>ANAN EXAMS</h4>
            <Row>
                <Col md={4} sm={12}>
                    {
                        isEditing ?
                            <DateFormComponent
                                placeholder="Date of Completion of ANAN's Professional Exam at NCA"
                                renderLabel={renderLabel}
                                name="ananProfessionalExamDate"
                                onChange={handleProfessionalExam}
                                value={ananProfessionalExamDate}
                            />
                            :
                            <InputFormComponent
                                placeholder="Date of Completion of ANAN's Professional Exam at NCA"
                                renderLabel={renderLabel}
                                value={ananProfessionalExamDate ? ananProfessionalExamDate.toDateString() : null}
                                disabled={true}
                            />
                    }
                </Col>
                <Col md={4} sm={12}>
                    {
                        isEditing ?
                            <DateFormComponent
                                placeholder="Start Date of Accountant-in-Training Program"
                                renderLabel={renderLabel}
                                name="ananTrainingStartDate"
                                onChange={handleTrainingStart}
                                value={ananTrainingStartDate}
                            />
                            :
                            <InputFormComponent
                                placeholder="Start Date of Accountant-in-Training Program"
                                renderLabel={renderLabel}
                                value={ananTrainingStartDate ? ananTrainingStartDate.toDateString() : null}
                                disabled={true}
                            />
                    }
                </Col>
                <Col md={4} sm={12}>
                    {
                        isEditing ?
                            <DateFormComponent
                                placeholder="End Date of Accountant-in-Training Program"
                                renderLabel={renderLabel}
                                name="ananTrainingEndDate"
                                onChange={handleTrainingEnd}
                                value={ananTrainingEndDate}
                            />
                            :
                            <InputFormComponent
                                placeholder="End Date of Accountant-in-Training Program"
                                renderLabel={renderLabel}
                                value={ananTrainingEndDate ? ananTrainingEndDate.toDateString() : null}
                                disabled={true}
                            />
                    }
                </Col>
            </Row>
            <h4>EMPLOYMENT DATA</h4>
            <Row>
                <Col md={6} sm={12}>
                    <InputFormComponent
                        placeholder="Name of Present Employer"
                        renderLabel={renderLabel}
                        name="presentEmployer"
                        value={presentEmployer}
                        onChange={isEditing ? handleUserApplicationInputChange : null}
                    />
                </Col>
                <Col md={6} sm={12}>
                    {
                        isEditing ?
                            <DateFormComponent
                                placeholder="Date of Appointment to Present Position"
                                renderLabel={renderLabel}
                                name="appointmentDate"
                                onChange={handleAppointmentDate}
                                value={appointmentDate}
                            />
                            :
                            <InputFormComponent
                                placeholder="Date of Appointment to Present Position"
                                renderLabel={renderLabel}
                                value={appointmentDate ? appointmentDate.toDateString() : ''}
                                disabled={true}
                            />
                    }
                </Col>
            </Row>
            <Row>
                <Col md={6} sm={12}>
                    <InputFormComponent
                        placeholder="Employer Address"
                        type="textarea"
                        renderLabel={renderLabel}
                        name="employerAddress"
                        value={employerAddress}
                        onChange={isEditing ? handleUserApplicationInputChange : null}
                    />
                </Col>
                <Col md={6} sm={12}>
                    <InputFormComponent
                        placeholder="Position Held"
                        renderLabel={renderLabel}
                        name="positionHeld"
                        value={positionHeld}
                        onChange={isEditing ? handleUserApplicationInputChange : null}
                    />
                </Col>
            </Row>
        </div>
    );
};

export const DueDiligence = ({ renderLabel, financialMisconduct, financialMisconductDetails,
    courtConviction, courtConvictionDetails, bankruptcy, bankruptcyDetails,
    previousAnanApplication, previousAnanApplicationDetails, isEditing,
    handleUserApplicationInputChange }) => {

  return (
      <div>
          <h4>DUE DILIGENCE</h4>
          <Row>
              <Col sm={12} md={12}>
                  <SelectFormComponent
                      values={["Select Yes/No", "Yes", "No"]}
                      placeholder="Has Any Investigation Panel Found You Guilty of Any Financial Misconduct"
                      renderLabel={renderLabel}
                      name="financialMisconduct"
                      value={financialMisconduct}
                      onChange={isEditing ? handleUserApplicationInputChange : null}
                  />
                  {
                      financialMisconduct === 'Yes'
                      ?
                          <InputFormComponent
                              placeholder="If Yes, Give Details of Offense and Findings"
                              type="textarea"
                              name="financialMisconductDetails"
                              value={financialMisconductDetails}
                              onChange={isEditing ? handleUserApplicationInputChange : null}
                          />
                      : ''
                  }

              </Col>
          </Row>
          <Row>
              <Col sm={12} md={12}>
                  <SelectFormComponent
                      values={["Select Yes/No", "Yes", "No"]}
                      placeholder="Have You Ever Been Convicted By Any Court Of Any Offence Involving Dishonesty Of Reflecting Upon Your Personal Integrity"
                      renderLabel={renderLabel}
                      name="courtConviction"
                      value={courtConviction}
                      onChange={isEditing ? handleUserApplicationInputChange : null}
                  />
                  {
                      courtConviction === 'Yes'
                          ?
                          <InputFormComponent
                              placeholder="If Yes, Give Particulars [Court, Date, Offense, Judgement]"
                              type="textarea"
                              name="courtConvictionDetails"
                              value={courtConvictionDetails}
                              onChange={isEditing ? handleUserApplicationInputChange : null}
                          />
                          : ''
                  }

              </Col>
          </Row>
          <Row>
              <Col sm={12} md={12}>
                  <SelectFormComponent
                      values={["Select Yes/No", "Yes", "No"]}
                      placeholder="Have You Ever Been Adjudged Bankrupt"
                      renderLabel={renderLabel}
                      name="bankruptcy"
                      value={bankruptcy}
                      onChange={isEditing ? handleUserApplicationInputChange : null}
                  />
                  {
                      bankruptcy === 'Yes'
                          ?
                          <InputFormComponent
                              placeholder="If Yes, Give Particulars [Court, Date of Adjudication Order, Date of Discharge]"
                              type="textarea"
                              name="bankruptcyDetails"
                              value={bankruptcyDetails}
                              onChange={isEditing ? handleUserApplicationInputChange : null}
                          />
                          : ''
                  }

              </Col>
          </Row>
          <Row>
              <Col sm={12} md={12}>
                  <SelectFormComponent
                      values={["Select Yes/No", "Yes", "No"]}
                      placeholder="Have You Previously Applied for Membership of this Association"
                      renderLabel={renderLabel}
                      name="previousAnanApplication"
                      value={previousAnanApplication}
                      onChange={isEditing ? handleUserApplicationInputChange : null}
                  />
                  {
                      previousAnanApplication === 'Yes'
                          ?
                          <InputFormComponent
                              placeholder="If Yes, Give Particulars [Date of Application, Association Reference Number]"
                              type="textarea"
                              name="previousAnanApplicationDetails"
                              value={previousAnanApplicationDetails}
                              onChange={isEditing ? handleUserApplicationInputChange : null}
                          />
                          : ''
                  }
              </Col>
          </Row>
      </div>
  );
};


//PAYMENT STATUS
export const Payment = ({ renderLabel, paymentReference, paymentDate, customerName, customerPhone, customerEmail,
    transactionAmount, isEditing, handleInputChange, handleDateChange, errors }) => {

    return (
        <div>
            <Row>
                <Col sm={12} md={12}>
                    <InputFormComponent
                        placeholder="Payment Reference"
                        renderLabel={renderLabel}
                        name="paymentReference"
                        value={paymentReference}
                        disabled={true}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={12}>
                    <InputFormComponent
                        placeholder="Customer Name"
                        renderLabel={renderLabel}
                        name="customerName"
                        value={customerName}
                        disabled={true}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={6}>
                    <InputFormComponent
                        placeholder="Customer Email"
                        renderLabel={renderLabel}
                        name="customerEmail"
                        value={customerEmail}
                        disabled={true}
                    />
                </Col>
                <Col sm={12} md={6}>
                    <InputFormComponent
                        placeholder="Customer Phone"
                        type="tel"
                        icon="phone"
                        renderLabel={renderLabel}
                        name="customerPhone"
                        value={customerPhone}
                        disabled={true}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={6}>
                    <InputFormComponent
                        placeholder="Transaction Amount"
                        renderLabel={renderLabel}
                        name="transactionAmount"
                        value={transactionAmount}
                        disabled={true}
                    />
                </Col>
                <Col sm={12} md={6}>
                    <InputFormComponent
                        placeholder="Payment Date"
                        renderLabel={renderLabel}
                        value={paymentDate}
                        name="paymentAmount"
                        disabled={true}
                    />
                </Col>

            </Row>
        </div>
    );
};