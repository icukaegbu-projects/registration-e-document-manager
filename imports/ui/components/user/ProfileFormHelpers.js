/**
 * Created by ikedi on 02/01/2017.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Card, CardTitle, CardSubtitle, CardText, CardBlock, CardFooter, Form, Button, Badge } from 'reactstrap';
import { Tabs, Tab } from 'react-bootstrap-tabs';
import Collapsible from  'react-collapsible';
import moment from 'moment';
// import { Flipper, Front, Back } from 'react-flipper';

import { BioData, DueDiligence, NextOfKinProfessional, ChangePasswordForm, StaffBioData, Payment } from './RegisterFormHelpers';
import FileUploadForm2 from './FileUploadForm2';
import ChangePassword from '../user/ChangePassword';

const getProfilePicture = ({path, base}) => {
    window.URL = window.URL || window.webkitURL;

    let xhr = new XMLHttpRequest(), blob = null, arrayBufferView = null;
    xhr.responseType = "arraybuffer";

    xhr.onload = function( e ) {
        let img = document.querySelector(`#${base}`),
            urlCreator = window.URL || window.webkitURL,
            imageUrl;

        // If error, show default image.
        if(e.currentTarget.status === 409) {
            img.src = "images/face-0.jpg";
            return
        }

        // Obtain a blob: URL for the image data.
        arrayBufferView = new Uint8Array(this.response);
        blob = new Blob([arrayBufferView], {type: "image/jpeg"});

        imageUrl = urlCreator.createObjectURL( blob );
        img.src = imageUrl;
    }

    xhr.open('POST', 'https://content.dropboxapi.com/2/files/get_thumbnail');
    //xhr.setRequestHeader('Authorization', 'Bearer ' + Meteor.settings.public.DROP_BOX_IJ);
    xhr.setRequestHeader('Authorization', 'Bearer ' + Meteor.settings.public.DROP_BOX_TOKEN);
    xhr.setRequestHeader('Dropbox-API-Arg', JSON.stringify({
        path: path,
        format: "jpeg",
        size: "w64h64"
    }));
    xhr.send();
};

const showProfilePicture = (user, profilePicture) => {
    const { bio } = user;
    let profilePicturePath = "", profilePictureBase = "";

    if (bio) {
        profilePictureBase = `${bio.lastName.toUpperCase()}-${bio.firstName.toUpperCase()}-${user.username.toUpperCase()}`;

        if (bio.ananNumber) {
            profilePicturePath = `/${bio.ananNumber}/Passport.jpg`;
        }else{
            profilePicturePath = `/${profilePictureBase}/Passport.jpg`;
        }
    }

    return (
        profilePicture
            ?
            <div>
                <img
                    className="avatar border-white"
                    id={profilePictureBase}
                    alt="Loading..."/>
                {getProfilePicture({path: profilePicturePath, base: profilePictureBase})}
            </div>
            :
            <img
                className="avatar border-white"
                src="images/face-0.jpg"
                alt="..."/>
    )
};

const ApplicationStatusLine = ({ label, data, icon, color, isButton = false, onClick }) => {
    return (
        <li>
            <Row>
                <Col xs={3} className="col-xs-3">
                    <Button className={`btn btn-sm btn-icon`} style={{background: color, color: '#FFFFFF', borderColor: color }}><i className={`fa ${icon}`}></i></Button>
                </Col>
                <Col xs={7} className="col-xs-7">
                    {!data  ? 'Pending' : data}
                    <br />
                    <span className="text-muted"><small>{label}</small></span>
                </Col>
                {
                    !data || data === 'Pending' || data === 'Not Approved'
                        ? ''
                        :
                        isButton ?
                            <Col xs={2} className="col-xs-2 text-right">
                                <button className="btn btn-fill btn-sm" onClick={onClick}>
                                    <i className="fa fa-check"></i>
                                </button>
                            </Col>
                            :
                            <Col xs={2} className="col-xs-2 text-right">
                                <span style={{color: color, fontSize: 20}}><i className="fa fa-check"></i></span>
                            </Col>
                }
            </Row>
        </li>
    );
};

export const UploadDisplayCard = ({ color, category, title, description, link='#' }) => {
    return (
        <Card
            className="card-just-text card-big-shadow "
            data-background="color" data-color={color}
            data-radius="none"
            style={{height: '180px'}}
        >
            <CardBlock className="content">
                <h6 className="category">{category}</h6>
                <h6 className="title" style={{fontWeight: 'bold'}}><a href={`/${link}`}>{title}</a></h6>
                <p className="description"></p>
                <Button className="btn btn-fill btn-wd" style={{position: 'absolute', top: '125px'}}><i className="fa fa-upload"></i> Upload</Button>
            </CardBlock>
        </Card>
    );
};

export const ProfileUser = ({ user, children, profilePicture = false, deleteUser = null }) => {
    const { bio } = user;
    // let profilePicturePath = "", profilePictureBase = "";
    //
    // if (bio) {
    //     profilePictureBase = `${bio.lastName.toUpperCase()}-${bio.firstName.toUpperCase()}-${user.username.toUpperCase()}`;
    //
    //     if (bio.ananNumber) {
    //         profilePicturePath = `/${bio.ananNumber}/Passport.jpg`;
    //     }else{
    //         profilePicturePath = `/${profilePictureBase}/Passport.jpg`;
    //     }
    // }

    return (
        bio ?
            <Card className="card-user" style={{minHeight: 575, height: 575}}>
                <div className="image">
                    <img src="images/london3.jpg" alt="..." />
                </div>
                <CardText tag="div" className="content">
                    <div className="author">
                        {
                            showProfilePicture(user, profilePicture)
                        }
                        {/*{*/}
                            {/*profilePicture*/}
                            {/*?*/}
                                {/*<div>*/}
                                    {/*<img*/}
                                        {/*className="avatar border-white"*/}
                                        {/*id={profilePictureBase}*/}
                                        {/*alt="Loading..."/>*/}
                                    {/*{getProfilePicture({path: profilePicturePath, base: profilePictureBase})}*/}
                                {/*</div>*/}
                                {/*:*/}
                                {/*<img*/}
                                    {/*className="avatar border-white"*/}
                                    {/*src="images/face-0.jpg"*/}
                                    {/*alt="..."/>*/}

                        {/*}*/}

                        <h4 className="title">{ `${bio.lastName.toUpperCase()},
                                ${bio.firstName.toUpperCase()} ${bio.middleName ? bio.middleName.toUpperCase() : ''} `}<br />
                            <span><small>{user.emails[0].address}</small></span>
                        </h4>
                    </div>
                    <div className="description text-center" style={{margin: 'auto'}}>
                        {
                            children
                            ? children : "New Applicant to ANAN"
                        }
                    </div>
                </CardText>
                <hr/>
                <div className="text-center">
                    <Row className="row">

                        {
                            Roles.userIsInRole(Meteor.userId(), ['admin','Membership']) && deleteUser ?
                                <Col md={{ size:1, offset: 1 }}>
                                    <button
                                        className="btn btn-danger btn-icon"
                                        onClick={deleteUser.bind(null, user)}
                                        style={{marginTop: 5}}
                                    >
                                        <i className="fa fa-trash"></i>
                                    </button>
                                </Col>
                            : <div></div>
                        }
                        <Col md={{ size:2, offset: 1 }}>
                            <h6>{bio.gender || "Unknown"}<br /><small>Gender</small></h6>
                        </Col>
                        <Col md={3}>
                            <h6>{bio.maritalStatus || "Unknown"}<br /><small>Status</small></h6>
                        </Col>
                        <Col md={4}>
                            <h6>{bio.dob && bio.dob.toDateString() ? bio.dob.toDateString() : "Unknown"}<br /><small>DoB</small></h6>
                        </Col>
                    </Row>
                </div>
            </Card>
            : <div></div>
    );
};

export class ProfileUser3 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showCollapsible: false
        };

        this.onShowCollapsible = this.onShowCollapsible.bind(this);
    }

    onShowCollapsible() {
        this.setState({ showCollapsible: !this.state.showCollapsible });
    }

    render() {
        const { user, uploadStatus, children, profilePicture, isStaff, deleteUser } = this.props;
        const { bio } = user;

        return (
            bio ?
                <Card className="card-user" style={{minHeight: 220, height: 220}}>
                    <div style={{padding: 10, height: 160}}>
                        <Row>
                            <Col md={12}>
                                {
                                    !this.state.showCollapsible ?
                                        <Row className="row">
                                            <Col md={5}>
                                                {
                                                    showProfilePicture(user, profilePicture)
                                                }
                                            </Col>
                                            <Col md={7}>
                                                <Row>
                                                    <Col md={12}>
                                                        <h5 className="title">{ `${bio.lastName.toUpperCase()},
                                        ${bio.firstName.toUpperCase()} ${bio.middleName ? bio.middleName.toUpperCase() : ''} `}<br />
                                                            <span><small>{user.emails[0].address}</small></span>
                                                        </h5>
                                                    </Col>
                                                </Row>
                                                <Row className="row">
                                                    {
                                                        isStaff ?
                                                            <div>
                                                                <Col md={3}>
                                                                    <h6>{bio.role.toUpperCase()}<br /><small>Role</small></h6>
                                                                </Col>
                                                                <Col md={6}>
                                                                    <div></div>
                                                                </Col>
                                                            </div>
                                                            :
                                                            <div>
                                                                <Col md={3}>
                                                                    <h6>{bio.gender || "Unknown"}<br /><small>Gender</small></h6>
                                                                </Col>
                                                                <Col md={3}>
                                                                    <h6>{bio.maritalStatus || "Unknown"}<br /><small>Status</small></h6>
                                                                </Col>
                                                                <Col md={3}>
                                                                    <h6>{bio.dob && bio.dob.toDateString() ? moment(bio.dob).format('DD/MM/YY') : "Unknown"}<br /><small>DoB</small></h6>
                                                                </Col>
                                                            </div>
                                                    }
                                                </Row>
                                            </Col>
                                        </Row>
                                        :
                                        <div style={{height: 160}}>
                                            {
                                                children
                                                    ?
                                                    children
                                                    : "New Applicant to ANAN"

                                            }
                                        </div>
                                }
                            </Col>
                        </Row>
                    </div>
                    <hr/>
                    <Row style={{paddingLeft: 15, marginBottom: 10, marginRight: 15}}>
                        <Col sm={{size: 1}} md={{size: 2}}>
                             <Badge style={{marginRight: 5}} className="btn btn-primary btn-fill">
                                <i className="fa fa-upload"></i>
                                <span style={{marginLeft: 3}}>
                                    {uploadStatus.filesUploaded}
                                </span>
                             </Badge>
                        </Col>
                        <Col sm={{size: 1}} md={{size: 2}}>
                             <Badge className="btn btn-info btn-fill">
                                <i className="fa fa-download"></i>
                                <span style={{marginLeft: 3}}>
                                    {uploadStatus.filesOutstanding}
                                </span>
                             </Badge>
                        </Col>
                        <Col  sm={{size: 1, offset: 1}} md={{size: 2, offset: 4}}>
                            {/*<h6 style={{display: 'inline'}}> Click to View</h6>*/}
                            <button
                                onClick={this.onShowCollapsible}
                                className="btn btn-success btn-icon"
                                style={{margin: 5}}>
                                <i className="fa fa-plus"></i> { !this.state.showCollapsible ? 'View' : 'Back' }
                            </button>
                        </Col>
                        {
                            Roles.userIsInRole(Meteor.userId(), ['admin','Membership']) && deleteUser ?
                                <Col sm={2} md={1}>
                                    <button
                                        className="btn btn-danger btn-icon"
                                        onClick={deleteUser.bind(null, user)}
                                        style={{marginTop: 5, marginLeft: 15}}
                                    >
                                        <i className="fa fa-trash"></i>
                                    </button>
                                </Col>
                                : null
                        }
                    </Row>
                </Card>
                : <div></div>
        );
    }
}
ProfileUser3.props = {
    user: PropTypes.object.isRequired,
    profilePicture: PropTypes.string.isRequired,
    isStaff: PropTypes.bool.isRequired,
    deleteUser: PropTypes.func.isRequired
};

ProfileUser3.defaultProps = {
    profilePicture: false,
    isStaff: false,
    deleteUser: null
};

export const ProfileUser2 = ({ user, children, profilePicture = false, isStaff = false, showCollapsible = true, deleteUser = null }) => {
    const { bio } = user;

    const isShowCollapsible = () => {
        return  showCollapsible ?
            <Collapsible
                trigger={
                    <div>
                        <button className="btn btn-success btn-icon" style={{margin: 5, marginLeft: 0}}>
                            <i className="fa fa-plus"></i>
                        </button>
                        <h6 style={{display: 'inline'}}> Click to View</h6>
                    </div>
                }
                accordionPosition="Top"
            >
                {
                    children
                }
            </Collapsible>
            : children
    };

    return (
        bio ?
            <Card className="card-user" style={{minHeight: 220, height: 220}}>
                <div style={{padding: 20}}>
                    <Row className="row">
                        <Col md={4}>
                            {
                                showProfilePicture(user, profilePicture)
                            }
                        </Col>
                        <Col md={8}>
                            <Row>
                                <Col md={12}>
                                    <h5 className="title">{ `${bio.lastName.toUpperCase()},
                                        ${bio.firstName.toUpperCase()} ${bio.middleName ? bio.middleName.toUpperCase() : ''} `}<br />
                                        <span><small>{user.emails[0].address}</small></span>
                                    </h5>
                                </Col>
                            </Row>
                            <Row className="row">
                                {
                                    isStaff ?
                                        <div>
                                            <Col md={3}>
                                                <h6>{bio.role.toUpperCase()}<br /><small>Role</small></h6>
                                            </Col>
                                            <Col md={6}>
                                                <div></div>
                                            </Col>
                                        </div>
                                        :
                                        <div>
                                            <Col md={3}>
                                                <h6>{bio.gender || "Unknown"}<br /><small>Gender</small></h6>
                                            </Col>
                                            <Col md={3}>
                                                <h6>{bio.maritalStatus || "Unknown"}<br /><small>Status</small></h6>
                                            </Col>
                                            <Col md={3}>
                                                <h6>{bio.dob && bio.dob.toDateString() ? moment(bio.dob).format('DD/MM/YY') : "Unknown"}<br /><small>DoB</small></h6>
                                            </Col>
                                        </div>
                                }
                                {
                                    Roles.userIsInRole(Meteor.userId(), ['admin','Membership']) && deleteUser ?
                                        <Col md={1}>
                                            <button
                                                className="btn btn-danger btn-icon"
                                                onClick={deleteUser.bind(null, user)}
                                                style={{marginTop: 5}}
                                            >
                                                <i className="fa fa-trash"></i>
                                            </button>
                                        </Col>
                                        : <div></div>
                                }
                            </Row>
                        </Col>
                    </Row>
                    <hr/>
                    {
                        children
                            ?
                            isShowCollapsible()
                            : "New Applicant to ANAN"

                    }
                </div>
            </Card>
            : <div></div>
    );
};

export const StaffProfileDisplayForm = ({ user }) => {
    return (
        <Row className="content" style={{padding: 20, paddingRight: 35}}>
            <Col lg={4} md={5}>
                <ProfileUser user={user}><div></div></ProfileUser>
                <ChangePassword/>
            </Col>
            <Col lg={8} md={7}>
                <Row>
                    <Card>
                        <CardTitle>
                            <span
                                className="title"
                                style={{paddingLeft: '15px'}}>
                                Data Profile
                            </span>
                        </CardTitle>
                        <CardBlock>
                            <StaffBioData
                                renderLabel={true}
                                firstName={user.bio.firstName}
                                lastName={user.bio.lastName}
                                middleName={user.bio.middleName}
                                phone={user.bio.phone}
                                gender={user.bio.gender}
                                maritalStatus={user.bio.maritalStatus}
                                role={user.bio.role}
                                privateAddress={user.bio.privateAddress}
                                stateOfOrigin={user.bio.stateOfOrigin}

                                isEditing={false}
                                handleInputChange={null}
                            />
                        </CardBlock>
                    </Card>
                </Row>
                {
                    !Roles.userIsInRole(Meteor.userId(), 'admin') ?
                        <Row>
                            <Card>
                                <CardTitle>
                                    <span
                                        className="title"
                                        style={{paddingLeft: '15px'}}>
                                        Applicant Approval Statistics
                                    </span>
                                </CardTitle>
                                <CardBlock>
                                    <div>Will display all approval statistics for staff here</div>
                                </CardBlock>
                            </Card>
                        </Row> : null
                }

            </Col>
        </Row>
    )
}

export const StatusDisplay = ({ title, subtitle = '', values }) => {
    return (
        <Card>
            <CardTitle>
                <span
                    className="title"
                    style={{paddingLeft: '15px'}}>
                    {title}
                    {
                        subtitle !== '' ?
                            <span className="title" style={{paddingLeft: '15px', margin: 20, fontSize: 20, fontWeight: 700}}><br/>{subtitle}</span>
                            : ''
                    }
                </span>
            </CardTitle>
            <CardText
                tag="div"
                className="content"
            >
                <ul className="list-unstyled team-members">
                    {
                        values ?
                            values.map((value) => {
                                return (
                                    <ApplicationStatusLine
                                        key={value.key}
                                        label={value.label}
                                        icon={value.icon}
                                        color={value.color}
                                        data={value.data}
                                        isButton={value.isButton}
                                        onClick={value.onClick}
                                    />
                                );
                            })
                        :
                            <li>No data</li>
                    }
                </ul>
            </CardText>
        </Card>
    )
};

export const ChangePasswordDisplay = ({ oldPassword, newPassword, confirmPassword, handleInputChange, onClick }) => {
    return (
        <Card>
            <CardTitle>
                <span
                    className="title"
                    style={{paddingLeft: '15px'}}>
                    Change Password
                </span>
            </CardTitle>
            <CardText
                tag="div"
                className="content"
            >
                <ChangePasswordForm
                    oldPassword={oldPassword}
                    newPassword={newPassword}
                    confirmPassword={confirmPassword}
                    handleInputChange={handleInputChange}
                    onClick={onClick}
                />
            </CardText>
        </Card>
    );
};

export const CongratulationsDisplay = ({ name, ananNumber }) => {
    return (
        <Card
            style={{
                backgroundColor: '#2196f3',
                color: 'white'
            }}>
            <CardTitle>
                <span
                    className="title"
                    style={{paddingLeft: '15px', color: 'white'}}>
                    Welcome to ANAN.
                </span>
            </CardTitle>
            <CardText
                tag="div"
                className="content"
            >
                <h3>Congratulations <br/>{name}</h3>

                <h4>Your Application has been approved</h4>
                <h4>Your ANAN Number is {ananNumber}</h4>

                <p>
                    Your induction date will be communicated shortly. <br/>
                    Kindly check your email or sms over the next couple of days.
                </p>
            </CardText>
        </Card>
    );
}

export const ArchiveMembersCard = ({ currentUser, onClick }) => {
    const colors = ['#607d8b', '#66bb6a', '#42a5fb', '#ab47bc'];
    let color = colors[Math.floor(Math.random() * colors.length)];
    let { name, ananNumber, phone, email } = currentUser;

    return (
        <Card
            style={{
                backgroundColor: color,
                color: 'white',
                width: '220px'
            }}>
            <CardTitle>
                <span
                    className="title"
                    style={{paddingLeft: '10px', color: 'white'}}>
                    {name}
                </span>
            </CardTitle>
            <CardText
                tag="div"
                className="content"
            >
                <h6>Email: {email}</h6>
                <p>Phone: {phone}</p>
            </CardText>
            <CardFooter style={{padding: 10, color: 'black'}}>
                <span style={{ fontWeight: 600, fontSize: 14, paddingTop: 5, paddingBottom: 5 }}>ANAN #: {ananNumber}</span>
                <Button className="btn btn-danger pull-right" onClick={onClick.bind(null, currentUser)}>View</Button>
            </CardFooter>
        </Card>
    );
}

export const ApplicationStatus = ({ applicationStarted, applicationSubmitted,
                    applicationApproved, isApproved, ananNumber }) => {
    const values = [
        {
            label: "Date Started",
            icon: "fa-envelope",
            color: "#7ACB58",
            data: applicationStarted,
            key: 1
        },
        {
            label: "Date Submitted",
            icon: "fa-bookmark",
            color: "#FF4A00",
            data: applicationSubmitted,
            key: 2
        },
        {
            label: "Date Approved",
            icon: "fa-briefcase",
            color: "#0081E9",
            data: applicationApproved,
            key: 3
        },
        {
            label: "Application Status",
            icon: "fa-battery-full",
            color: "#760076",
            data: isApproved,
            key: 4
        },
        {
            label: "ANAN Number",
            icon: "fa-flag",
            color: "#AD1457",
            data: ananNumber,
            key: 5
        },
    ]
    return (
        <StatusDisplay title="Application Status" values={values} />
    );
};

// export const PaymentCard = ({ user, isEditing, handleUserInputChange, onSubmit }) => {
//     const { lastName, firstName, middleName, phone, dob, gender, maritalStatus } = user.bio;

//     return (
//         <Card
//             style={{
//                 backgroundColor: '#344291',
//                 color: 'white',
//                 width: '220px'
//             }}>
//             <CardTitle>
//                 <span
//                     className="title"
//                     style={{paddingLeft: '10px', color: 'white'}}>
//                     Make Payment
//                 </span>
//             </CardTitle>
//             <CardText
//                 tag="div"
//                 className="content"
//             >
//                 {
//                     user ?
//                         <Payment
//                             renderLabel={true}
//                             customerName={`${lastName}, ${firstName} ${middleName}`}
//                             customerPhone={phone}
//                             customerEmail={user.emails[0].address}
//                             paymentDate={dob}
//                             transactionAmount={gender}
//                             paymentReference={maritalStatus}

//                             isEditing={isEditing}
//                             handleInputChange={handleUserInputChange}
//                         />
//                         :
//                         <div></div>
//                 }
//             </CardText>
//             <CardFooter style={{padding: 10, color: 'black'}}>
//                 <Button className="btn btn-danger pull-right" onClick={onSubmit}>Make Payment</Button>
//             </CardFooter>
//         </Card>
//     );
// }

export const PaymentCard = ({ user, paymentDate, transactionAmount, paymentReference, onSubmit }) => {
    if(user && user.bio) {
        const { lastName, firstName, middleName, phone } = user.bio;

        return (
            <Card block style={{ padding: 10 }}>
                <CardTitle>
                    Registration Payment
                </CardTitle>
                <hr/>
                <CardSubtitle style={{ background: '#f4f3ef', padding: 10, color: 'red' }}>
                    You must make Payment to continue registration<br/>
                    Pay the sum of #5,000.00 to ANAN account
                </CardSubtitle>

                {
                    user ?
                        <Payment
                            renderLabel={true}
                            customerName={`${lastName}, ${firstName} ${middleName}`}
                            customerPhone={phone}
                            customerEmail={user.emails[0].address}
                            paymentDate={paymentDate}
                            transactionAmount={transactionAmount}
                            paymentReference={paymentReference}
                        />
                        :
                        <div></div>
                }
                <CardFooter style={{padding: 10, color: 'black'}}>
                    <Button className="btn btn-default pull-right" onClick={() => onSubmit()}>Make Payment</Button>
                </CardFooter>
            </Card>
        );
    }
    return null;
}

export const FormTabs = (props) => {
    if (props.loading) {
        return <div>Loading...</div>
    } else {
        const bio = props.user.bio;
        const ua = props.userApplication;

        return (
            <Form className="register-form">
                <Tabs onSelect={props.onChangeTab} selected={props.tabIndex}>
                    <Tab label="Bio Data">
                        <Card block style={{padding: 10}}>
                            {
                                bio ?
                                    <BioData
                                        renderLabel={true}
                                        firstName={bio.firstName}
                                        lastName={bio.lastName}
                                        middleName={bio.middleName}
                                        phone={bio.phone}
                                        secondPhone={bio.secondPhone}
                                        ananBranch={bio.ananBranch}
                                        gender={bio.gender}
                                        maritalStatus={bio.maritalStatus}
                                        dob={bio.dob}
                                        privateAddress={bio.privateAddress}
                                        stateOfOrigin={bio.stateOfOrigin}
                                        nationality={bio.nationality}

                                        isEditing={props.isEditing}
                                        handleInputChange={props.handleUserInputChange}
                                    />
                                    :
                                    <div></div>
                            }
                        </Card>
                        <Card block style={{padding: 10}}>
                            {
                                ua ?
                                    <NextOfKinProfessional
                                        renderLabel={true}
                                        nextOfKinName={ua.nextOfKinName}
                                        nextOfKinPhone={ua.nextOfKinPhone}
                                        nextOfKinAddress={ua.nextOfKinAddress}
                                        nextOfKinRelationship={ua.nextOfKinRelationship}

                                        tertiaryInstitutionAttended={ua.tertiaryInstitutionAttended}
                                        qualificationsWithDates={ua.qualificationsWithDates}

                                        presentEmployer={ua.presentEmployer}
                                        employerAddress={ua.employerAddress}
                                        positionHeld={ua.positionHeld}
                                        appointmentDate={ua.appointmentDate}

                                        ananProfessionalExamDate={ua.ananProfessionalExamDate}
                                        ananTrainingStartDate={ua.ananTrainingStartDate}
                                        ananTrainingEndDate={ua.ananTrainingEndDate}

                                        isEditing={props.isEditing}
                                        handleUserApplicationInputChange={props.handleUserApplicationInputChange}

                                        handleProfessionalExam={props.handleProfessionalExam}
                                        handleTrainingEnd={props.handleTrainingEnd}
                                        handleTrainingStart={props.handleTrainingStart}
                                        handleAppointmentDate={props.handleAppointmentDate}
                                    />
                                    :
                                    <div></div>
                            }
                        </Card>
                        <Card block style={{padding: 10}}>
                            {
                                ua ?
                                    <DueDiligence
                                        renderLabel={true}
                                        financialMisconduct={ua.financialMisconduct}
                                        financialMisconductDetails={ua.financialMisconductDetails}
                                        courtConviction={ua.courtConviction}
                                        courtConvictionDetails={ua.courtConvictionDetails}
                                        bankruptcy={ua.bankruptcy}
                                        bankruptcyDetails={ua.bankruptcyDetails}

                                        previousAnanApplication={ua.previousAnanApplication}
                                        previousAnanApplicationDetails={ua.previousAnanApplicationDetails}

                                        isEditing={props.isEditing}
                                        handleUserApplicationInputChange={props.handleUserApplicationInputChange}
                                    />
                                    :
                                    <div></div>
                            }
                        </Card>
                    </Tab>
                    <Tab label="Support Documents">
                        <Card block style={{padding: 10}}>
                            <FileUploadForm2
                                isEditing={props.isEditing}
                                user={props.user}
                                userApplication={props.userApplication}
                            />
                        </Card>
                    </Tab>
                    <Tab label="Payment Details">
                        <Card block style={{padding: 10}}>
                            {
                                bio ?
                                    <Payment
                                        renderLabel={true}
                                        customerName={`${lastName}, ${firstName} ${middleName}`}
                                        customerPhone={phone}
                                        customerEmail={user.emails[0].address}
                                        paymentDate={paymentDate}
                                        transactionAmount={transactionAmount}
                                        paymentReference={paymentReference}
                                    />
                                    :
                                    <div></div>
                            }
                        </Card>
                    </Tab>
                </Tabs>
            </Form>
        );
    }
};