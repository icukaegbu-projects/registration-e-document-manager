import React, {Component} from 'react';
import { Roles } from 'meteor/alanning:roles';

import LoginForm from './LoginForm';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                email: '',
                password: '',
                isLoggingIn: false
            }
        };

        this.loginUser = this.loginUser.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.resetState = this.resetState.bind(this);
    }

    handleInputChange(evt){
        const fields = this.state.fields;
        fields[evt.target.name] = evt.target.value;
        this.setState({ fields })
    }

    resetState(){
        const fields = this.state.fields;

        fields.email = '';
        fields.password = '';
        this.setState({ fields });
    }

    loginUser(e) {
        e.preventDefault();

        //show the pre-loader; set the isLoggingIn field
        const fields = this.state.fields;
        fields.isLoggingIn = !fields.isLoggingIn;
        this.setState({ fields });

        //extract the email and password
        let { email, password } = this.state.fields;

        Meteor.loginWithPassword(email, password, (error) => {
            if (error) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(error.reason, 'Login Error');

                //hide the pre-loader
                fields.isLoggingIn = !fields.isLoggingIn;
                this.setState({ fields });
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Login Successful', 'Success');

                if(Roles.userIsInRole(Meteor.userId(), ['admin', 'Supervisor',
                        'DataCapture', 'Audit', 'Exams', 'Membership', 'Registry'])) {
                    //log staff login

                    Meteor.call('staff.login.audit.trail', (err) =>{
                        if(err) {
                            console.log(err)
                        }
                    });

                    //go to dashboard
                    FlowRouter.go('admin-dashboard')
                }else{
                    FlowRouter.go('profile');
                }
            }
        });

    }

    render() {
        return (
            <LoginForm
                email={this.state.fields.email}
                password={this.state.fields.password}
                loginUser={this.loginUser}
                isLoggingIn={this.state.fields.isLoggingIn}
                handleInputChange={this.handleInputChange}
                resetState={this.resetState}
            />
        )
    }
}

export default Login;