/**
 * Created by ikedi on 07/12/2016.
 */

import React, { Component } from 'react';
import { Row, Col, Card, CardText, CardTitle } from 'reactstrap';
import { LoginFormDetails, StyledDivider, AnanDisplay } from './RegisterFormHelpers';

class LoginForm extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
        <div className="full-screen register background">
            <div className="wrapper">
                <div className="filter-black"></div>
                <Row>
                    <Col
                        xs={{ size: 10, offset: 1 }}
                        sm={{ size: 6, offset: 3 }}
                        md={{ size: 4, offset: 4 }}
                    >
                        <Card className="demo-card">
                            <CardTitle className="title text-center">Login</CardTitle>
                            <AnanDisplay />

                            <CardText tag="div">
                                <StyledDivider />

                                <LoginFormDetails
                                    email={this.props.email}
                                    password={this.props.password}
                                    loginUser={this.props.loginUser}
                                    isLoggingIn={this.props.isLoggingIn}
                                    handleInputChange={this.props.handleInputChange}
                                    resetState={this.props.resetState}
                                />
                            </CardText>
                        </Card>
                    </Col>
                </Row>
            </div>
        </div>
        );
    }
}

export default LoginForm;