/**
 * Created by ikedi on 19/01/2017.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Meteor} from 'meteor/meteor';
import {_} from 'meteor/underscore';
import Dropzone from 'react-dropzone';
import jsPDF from 'jspdf';

import { uploadUserDocument_V2, updateUserDocumentInfo_V2, deleteUserDocument_V2 } from '../../../api/users/DropboxMethods_V2';

import { Card, CardBlock, Row, Col, Button, Alert, Progress } from 'reactstrap';

class FileUploadCard2 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            uploading: [],
            progress: 0,
            inProgress: false,
            uploaded: false,
            checkFileSizeError: false,
            checkFileTypeError: false,
            checkFileNameError: false
        };

        this.showUploadProgress = this.showUploadProgress.bind(this);
        this.removeFile = this.removeFile.bind(this);
        this.onDrop = this.onDrop.bind(this);
    }

    getDataUri (url, callback) {
        var image = new Image();

        image.onload = function () {
            var canvas = document.createElement('canvas');
            canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
            canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

            canvas.getContext('2d').drawImage(this, 0, 0);

            // Get raw image data
            //callback(canvas.toDataURL('image/jpg').replace(/^data:image\/(png|jpg);base64,/, ''));

            // ... or get as Data URI
            callback(canvas.toDataURL('image/jpg'));
        };

        image.src = url;
    }

    onDrop (files) {
        if(files && files[0]) {
            let file = files[0];

            let {lastName, firstName} = this.props.user.bio;
            let {username} = this.props.user;
            let path = this.props.user.bio && this.props.user.bio.ananNumber ?
                `/${this.props.user.bio.ananNumber}`
                : `/${lastName.toUpperCase()}-${firstName.toUpperCase()}-${username.toUpperCase()}`;

            if (file) {
                let newFileName = this.props.nameFormat;

                if (file.type === 'image/jpg' || file.type === 'image/jpeg') {
                    //if it is not the passport and it is an image, convert it to PDF
                    if(newFileName !== "Passport.jpg" ) {
                        //create a document
                        let doc = new jsPDF();

                        this.getDataUri(file, function(dataUri) {
                            doc.addImage(dataUri,15, 40, 180, 160)
                        });

                        //write the new pdf to file
                        file = new File([doc], newFileName, { type: 'application/pdf' });
                    }
                } else if ( file.type === 'application/pdf' ) {
                    //just rename the file if it is a PDF
                    file = new File([file], newFileName, { type: file.type });
                } else {
                    toastr.options = {"positionClass": 'toast-top-left'};
                    toastr.error('File could not be converted', 'Failed File Conversion');
                    this.setState({
                        checkFileNameError: true
                    })
                    return;
                }
                path += `/${newFileName}`;

                // //check size does not exceed 10mb
                if (file.size > 10240000) {
                    toastr.options = {"positionClass": 'toast-top-left'};
                    toastr.error('File size greater than 10MB', 'Large File Size');
                    this.setState({
                        checkFileSizeError: true
                    })
                    return;
                }

                //split nameFormat and attach file type ending
                // let tempName = this.props.nameFormat.split('.')[0];
                // let newFileName = '';

                // console.log(file)

                // //rename file
                // if(file.type === 'image/jpg' || file.type === 'image/jpeg') {
                //     newFileName = `${tempName}.jpg`;
                // }else if(file.type === 'application/pdf'){
                //     newFileName = `${tempName}.pdf`;
                // }else{
                //     toastr.options = {"positionClass": 'toast-top-left'};
                //     toastr.error('File could not be renamed correctly', 'File Renaming Failed');
                //     this.setState({
                //         checkFileNameError: true
                //     });
                //     return;
                // }
                // file = new File([file], newFileName, { type: file.type })
                // path += `/${newFileName}`;

                // console.log(file)
                // console.log(path)

                // //check size does not exceed 10mb
                // if (file.size > 10240000) {
                //     toastr.options = {"positionClass": 'toast-top-left'};
                //     toastr.error('File size greater than 10MB', 'Large File Size');
                //     this.setState({
                //         checkFileSizeError: true
                //     })
                //     return;
                // }


                //path += "/" + this.props.nameFormat;
                // if (file.type === 'image/jpg' || file.type === 'image/jpeg') {
                //     //check file type is only PDF
                //     if(this.props.nameFormat !== "Passport.jpg" ) {
                //         toastr.options = {"positionClass": 'toast-top-left'};
                //         toastr.error('Passport must be named Passport.jpg and in .jpg format', 'Wrong File Type');
                //         this.setState({
                //             checkFileTypeError: true
                //         })
                //         return;
                //     }
                // } else if ( file.type === 'application/pdf' ) {
                //     //check file size does not exceed 1MB
                //     if (file.size > 10240000) {
                //         toastr.options = {"positionClass": 'toast-top-left'};
                //         toastr.error('File size greater than 10MB', 'Large File Size');
                //         this.setState({
                //             checkFileSizeError: true
                //         })
                //         return;
                //     }

                //     //check current file name matches approved filename as passed in via props
                //     //if (file.name !== this.props.nameFormat) {
                //     if (file.name.toUpperCase() !== this.props.nameFormat.toUpperCase()) {
                //         toastr.options = {"positionClass": 'toast-top-left'};
                //         toastr.error('File name has spaces or is not formatted correctly', 'Wrong Name Format');
                //         this.setState({
                //             checkFileNameError: true
                //         });
                //         return;
                //     }
                // } else {
                //     toastr.options = {"positionClass": 'toast-top-left'};
                //     toastr.error('File type must be PDF', 'Wrong File Type');
                //     this.setState({
                //         checkFileSizeError: true
                //     })
                //     return;
                // }

                //start uploading, display
                this.setState({
                    uploading: file,
                    inProgress: true
                });

                //OPTION 3
                var xhr = new XMLHttpRequest();

                xhr.upload.onprogress = (evt) => {
                    var percentComplete = parseInt(100.0 * evt.loaded / evt.total);

                    // Upload in progress. Do something here with the percent complete.
                    this.setState({
                        progress: percentComplete
                    })
                };

                xhr.onload = () => {
                    if (xhr.status === 200) {
                        let fileInfo = JSON.parse(xhr.response);

                        updateUserDocumentInfo_V2.call({
                            path: path,
                            userId: this.props.user._id
                        }, (err, result) => {
                            if (err) {
                                //console.log(err);
                                this.setState({
                                    inProgress: false
                                });
                            }
                            else {
                                //console.log(result)
                                this.setState({
                                    uploading: [],
                                    progress: 0,
                                    inProgress: false,
                                    checkFileSize: false,
                                    checkFileType: false,
                                    checkFileName: false
                                });

                                toastr.options = {"positionClass":'toast-top-left'};
                                toastr.success('File saved successfully', 'File Saved');
                            }
                        });
                    }
                    else {
                        var errorMessage = xhr.response || 'Unable to upload file';

                        // Upload failed. Do something here with the error.
                        toastr.options = {"positionClass":'toast-top-left'};
                        toastr.error(errorMessage, 'Error in saving file');

                        this.setState({
                            inProgress: false
                        });
                    }
                };

                xhr.open('POST', 'https://content.dropboxapi.com/2/files/upload');
                //xhr.setRequestHeader('Authorization', 'Bearer ' + Meteor.settings.public.DROP_BOX_IJ );
                xhr.setRequestHeader('Authorization', 'Bearer ' + Meteor.settings.public.DROP_BOX_TOKEN );
                xhr.setRequestHeader('Content-Type', 'application/octet-stream');
                xhr.setRequestHeader('Dropbox-API-Arg', JSON.stringify({
                    path: path,
                    mode: 'add',
                    autorename: true,
                    mute: false
                }));

                xhr.send(file);
            }
        }
    }

    removeFile(e) {
        e.preventDefault();
        let conf = confirm('Are you sure you want to delete this file?') || false;

        //do something with confirm
        if(conf) {
            this.setState({
                inProgress: true
            });

            deleteUserDocument_V2.call({ path: this.props.dropboxPath, userId: this.props.user._id }, Meteor.bindEnvironment((err, result) => {
                if(err) {
                    toastr.options = {"positionClass":'toast-top-left'};
                    toastr.error(err.error, 'Error Deleting File');
                    this.setState({
                        inProgress: false
                    });
                }
                else {
                    toastr.options = {"positionClass":'toast-top-left'};
                    toastr.success('File Deleted Successfully', 'Delete Successful');
                    this.setState({
                        uploading: [],
                        uploaded: false,
                        progress: 0,
                        inProgress: false,
                        checkFileSize: false,
                        checkFileType: false,
                        checkFileName: false
                    });
                }
            }));
        }
    }

    showUploadProgress() {
        if (this.state.inProgress) {
            return (
                <div>
                    <Progress
                        animated
                        color="success"
                        value={this.state.progress}
                        aria-valuemax="100"
                        aria-valuemin="0"
                        aria-valuenow={this.state.progress || 0}
                    >
                        <span className="sr-only">{this.state.progress}% Complete (success)</span>
                        <span>{this.state.progress}%</span>
                    </Progress>

                    <div className="loader2">Loading...</div>
                </div>
            );
        }
    }

    render() {
        return (
            <Card
                className="card-just-text card-big-shadow "
                data-background="color" data-color={this.props.color}
                data-radius="none"
                style={{height: '300px'}}
            >
                <CardBlock className="content">
                    <h6 className="category">{this.props.title}</h6>
                    <h6 className="title" style={{fontWeight: 700}}>File Name Format:<br/>{this.props.nameFormat}</h6>
                    <h6 className="title" style={{fontWeight: 700}}>File Size: Should not exceed 10MB</h6>
                    <div className="description">
                        {this.showUploadProgress()}
                    </div>
                    {
                        this.props.isUploaded && !this.state.inProgress ?
                            <div>
                                <Row>
                                    <Col md={4} sm={6}>
                                        <Button
                                            className="btn btn-danger btn-fill"
                                            onClick={this.removeFile}
                                        >
                                            <i className="fa fa-remove"></i> Delete
                                        </Button>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12} sm={12}>
                                        <Alert
                                            color="info"
                                            isOpen={this.props.isUploaded}
                                            style={{
                                                marginTop: 40
                                            }}
                                        >
                                            <strong>Completed:</strong><br/>File uploaded
                                        </Alert>
                                    </Col>
                                </Row>
                            </div>
                        :
                            <Row>
                                <Col md={12} sm={12}>
                                    {
                                        !this.state.inProgress ?
                                           <Dropzone
                                               onDrop={this.onDrop}
                                               multiple={false}
                                               className="btn btn-primary"
                                           >
                                               Drag file on or Click to Upload
                                           </Dropzone>
                                        : <div></div>
                                    }

                                </Col>
                            </Row>
                    }
                    <Row>
                        <Col md={11} sm={11}>
                            <Alert color="danger" isOpen={this.state.checkFileSizeError} toggle={() => this.setState({ checkFileSizeError: false })}>
                                <strong>Large File Size:</strong><br/>File size greater than 1MB
                            </Alert>
                        </Col>
                        <Col md={11} sm={11}>
                            <Alert color="danger" isOpen={this.state.checkFileTypeError} toggle={() => this.setState({ checkFileTypeError: false })}>
                                <strong>Wrong File Type:</strong><br/>File can only be PDF format
                            </Alert>
                        </Col>
                        <Col md={11} sm={11}>
                            <Alert color="danger" isOpen={this.state.checkFileNameError} toggle={() => this.setState({ checkFileNameError: false })}>
                                <strong>Wrong name format:</strong><br/>File name has spaces or is not formatted correctly
                            </Alert>
                        </Col>
                    </Row>
                </CardBlock>
            </Card>
        )
    }
}

FileUploadCard2.propTypes = {
    user: PropTypes.object.isRequired,
    nameFormat: PropTypes.string,
    dropboxPath: PropTypes.string,
    color: PropTypes.string,
    title: PropTypes.string,
    isUploaded: PropTypes.bool
};

FileUploadCard2.defaultProps = {
    isEditing: false,
    isNew: false,
    color: '#333333'
}

export default FileUploadCard2;