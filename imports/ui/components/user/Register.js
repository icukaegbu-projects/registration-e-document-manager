import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { userSignup } from '../../../api/users/methods';
import regex_validations from '../../../api/regex_validations';
import RegisterForm from './RegisterForm';
import moment from 'moment';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                acceptDeclaration: '',
                username: '',
                email: '',
                password: '',
                confirmPassword: '',

                firstName: '',
                middleName: '',
                lastName: '',
                phone: '',
                secondPhone: '',
                ananBranch: '',
                gender: '',
                maritalStatus: '',
                dob: moment(),
                privateAddress: '',
                stateOfOrigin: '',
                nationality: '',
                isRegistering: false
            },
            errors: {}
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleRegistration = this.handleRegistration.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.resetState = this.resetState.bind(this);
    }

    handleInputChange(evt)
    {
        const fields = this.state.fields;
        fields[evt.target.name] = evt.target.value;
        this.setState({ fields });
    }

    handleDateChange(date)
    {
        const fields = this.state.fields;
        fields.dob = date.toDate();
        this.setState({ fields });
    }

    handleRegistration(e)
    {
        e.preventDefault();

        //validate the form
        if(!this.validateForm()){
            console.log('Form invalid')
            console.log(this.state.errors);
            return;
        }else{
            //set the isRegistering field
            const fields = this.state.fields;

            fields.isRegistering = !fields.isRegistering;
            this.setState({ fields });

            let userData = this.state.fields;

            //call signup method
            let emails = [];
            let user_email = { address: userData.email, verified: false };
            emails.push(user_email);
            userData.emails = emails;

            //exclude isRegistering from fields submitted to server
            let { isRegistering, ...userDataSubmit } = userData;

            userSignup.call(userDataSubmit,  (message, user) => {
                if (message) {
                    if(message.reason === 'Email already exists' || message.reason === 'Phone number already exists'){
                        this.resetState();
                    }

                    toastr.options = {"positionClass":'toast-top-left'};
                    toastr.error(message.reason, 'Signup Error');

                    //hide the pre-loader
                    fields.isRegistering = !fields.isRegistering;
                    this.setState({ fields });
                    return;
                } else {
                    if (this.props.registerNew) {
                        Meteor.loginWithPassword(userData.emails[0].address, userData.password, function(error) {
                            if (error) {
                                toastr.options = {"positionClass":'toast-top-left'};
                                toastr.error(error.reason, 'Login Error')
                            } else {
                                toastr.options = {"positionClass":'toast-top-left'};
                                toastr.success('Login Successful', 'Success');

                                FlowRouter.go('profile');
                            }
                        });
                    }else{
                        this.resetState();
                    }

                    return;
                }
            });
        }
    }

    validateForm(){
        //clear the errors object to reset it at the beginning
        this.setState({ errors: null });

        //if form validation fails, set errors to the errors and display them
        let { ck_name, ck_username, ck_email, ck_password, ck_phone,  } = regex_validations;

        let { username, firstName, lastName, email, phone, secondPhone, password, confirmPassword,
            gender, maritalStatus, stateOfOrigin, nationality
        } = this.state.fields;
        let errors = {};
        

        if ( email === '' || password === '') {
            errors.error = 'Email and/or password cannot be null';
        }

        if ( password !== confirmPassword ) {
            errors.confirmPassword = 'Passwords do not match';
        }

        //if above validations passed,
        // clear errors before checking regular expressions
        //validate regular expressions
        if (!ck_name.test(firstName)) {
            errors.firstName =  "Enter a valid First Name";
        }
        if (!ck_name.test(lastName)) {
            errors.lastName =  "Enter a valid Last Name";
        }
        if (!ck_username.test(username)) {
            errors.username =  "Enter a valid Username (3 - 20 letters)";
        }
        if (!ck_email.test(email)) {
            errors.email = "Enter a valid Email Address";
        }
        if (!ck_password.test(password)) {
            errors.password = "Enter a valid Password";
        }
        if (!ck_phone.test(phone)) {
            errors.phone = "11 Digits";
        }
        if (secondPhone && !ck_phone.test(secondPhone)) {
            errors.secondPhone = "11 Digits";
        }
        if (!ck_name.test(gender)) {
            errors.gender =  "**";
        }
        if (!ck_name.test(maritalStatus)) {
            errors.maritalStatus =  "**";
        }
        if (!ck_name.test(stateOfOrigin)) {
            errors.stateOfOrigin =  "**";
        }
        if (!ck_name.test(nationality)) {
            errors.nationality =  "**";
        }

        //if errors is not null, clear the state and return
        if(Object.keys(errors).length > 0 ){
            this.setState({ errors });
            return false;
        }

        this.setState({ errors: {} });
        return true;
    }

    resetState() {
        this.setState({
            fields: {
                acceptDeclaration: '',
                username: '',
                email: '',
                password: '',
                confirmPassword: '',

                firstName: '',
                middleName: '',
                lastName: '',
                phone: '',
                secondPhone: '',
                ananBranch: '',
                gender: '',
                maritalStatus: '',
                dob: moment(),
                privateAddress: '',
                stateOfOrigin: '',
                nationality: '',
                isRegistering: false
            }
        });
    }

    render() {
        return (
            <RegisterForm
                renderNew={this.props.registerNew}
                acceptDeclaration={this.state.fields.acceptDeclaration}
                username={this.state.fields.username}
                email={this.state.fields.email}
                password={this.state.fields.password}
                confirmPassword={this.state.fields.confirmPassword}

                firstName={this.state.fields.firstName}
                lastName={this.state.fields.lastName}
                middleName={this.state.fields.middleName}
                phone={this.state.fields.phone}
                secondPhone={this.state.fields.secondPhone}
                ananBranch={this.state.fields.ananBranch}
                gender={this.state.fields.gender}
                maritalStatus={this.state.fields.maritalStatus}
                dob={this.state.fields.dob}
                privateAddress={this.state.fields.privateAddress}
                stateOfOrigin={this.state.fields.stateOfOrigin}
                nationality={this.state.fields.nationality}

                isRegistering={this.state.fields.isRegistering}
                handleInputChange={this.handleInputChange}
                handleDateChange={this.handleDateChange}
                handleRegistration={this.handleRegistration}

                errors={this.state.errors}
            />
        )
    }
}

Register.propTypes = {
    registerNew: PropTypes.bool.isRequired,
    registeredUser: PropTypes.object,
    setRegisteredUser: PropTypes.func
};

Register.defaultProps = {
    registerNew: true
};

export default Register;