/**
 * Created by ikedi on 14/12/2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col, Card, CardTitle, CardBlock } from 'reactstrap';
import Collapsible from  'react-collapsible';

import Sidebar from '../Sidebar';
import DashboardNavbar from '../DashboardNavbar';
import Footer from '../Footer';

import { ProfileUser, ApplicationStatus, CongratulationsDisplay, FormTabs, PaymentCard } from './ProfileFormHelpers';
import ChangePassword from './ChangePassword';
import { userApplication, userPayment } from '../../../api/users/methods';
import { deleteUser_V2 } from '../../../api/users/DropboxMethods_V2';

import { Accounts } from 'meteor/accounts-base';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import uuidv4 from 'uuid/v4';

class ProfileForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            userApplication: {},
            userPayment: {},
            paymentReference: null,
            paymentDate: null,
            isEditing: false,
            isSubmitted: false,
            tabIndex: 0,
        };

        this.renderInterface = this.renderInterface.bind(this);
        this.displayComment = this.displayComment.bind(this);
        this.displayInstructions = this.displayInstructions.bind(this);
        this.formatDate = this.formatDate.bind(this);
        this.onEditChange = this.onEditChange.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onChangeTab = this.onChangeTab.bind(this);
        this.saveUserApplication = this.saveUserApplication.bind(this);
        this.handleApplicationSave = this.handleApplicationSave.bind(this);
        this.handleApplicationSubmit = this.handleApplicationSubmit.bind(this);
        this.handleUserInputChange = this.handleUserInputChange.bind(this);
        this.handleUserApplicationInputChange = this.handleUserApplicationInputChange.bind(this);
        this.handleProfessionalExam = this.handleProfessionalExam.bind(this);
        this.handleTrainingEnd = this.handleTrainingEnd.bind(this);
        this.handleTrainingStart = this.handleTrainingStart.bind(this);
        this.handleAppointmentDate = this.handleAppointmentDate.bind(this);
        this.saveUserPayment = this.saveUserPayment.bind(this);
        //this.isEmailVerified = this.isEmailVerified.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps && nextProps.user && nextProps.userApplication && nextProps.userPayment) {
            this.setState({
                user: nextProps.user,
                userApplication: nextProps.userApplication,
                userPayment: nextProps.userPayment,
                isSubmitted: nextProps.userApplication.isSubmitted
            });
        }
    }

    onChangeTab(idx, label) {
        this.setState({
            tabIndex: idx
        })
    }

    onDelete() {
        console.log("Delete this user from the Database")
        let isDelete = confirm("Delete your account?");
        if(isDelete) {
            //delete his folder from Dropbox
            //delete his records from Meteor.users and UserApplication
            //log the user out and route back to register page
            //send him a mail to notify him of the deletion
            let userData = this.state.user,
                userId = userData._id,
                path = `/${userData.bio.lastName.toUpperCase()}-${userData.bio.firstName.toUpperCase()}-${userData.username.toUpperCase()}`

            deleteUser_V2.call({ path, userId }, (err, result) => {
                if(err) {
                    console.log(err);
                    toastr.options = {"positionClass":'toast-top-right'};
                    toastr.error(err.reason, 'Logout Error');
                }else{
                    Meteor.logout((error) => {
                        if (error) {
                            toastr.options = {"positionClass":'toast-top-right'};
                            toastr.error(error.reason, 'Logout Error');
                        } else {
                            toastr.options = {"positionClass":'toast-top-right'};
                            toastr.warning('Account Deleted Successful', 'Success');
                            FlowRouter.go('register');
                        }
                    })
                }
            });
        }
    }

    generatePaymentReference() {
        //return uuidv4();
        // this.setState({
        //     paymentReference: Date.now(),
        //     paymentDate: new Date().toISOString().split('T')[0],
        // })
        return {
            //paymentReference: Date.now(),
            paymentReference: Date.now().toString(),
            paymentDate: new Date().toISOString().split('T')[0],
            transactionAmount: 5000
        };
    }

    onEditChange() {
        this.setState({
            isEditing: !this.state.isEditing
        })
    }

    formatDate(dateObj) {
        return (
            dateObj ? dateObj.toDateString() : 'Pending'
        );
    }

    handleProfessionalExam(date)
    {
        const ua = this.state.userApplication;
        ua.ananProfessionalExamDate = date.toDate();

        this.setState({ ua });
    }

    handleTrainingStart(date)
    {
        const ua = this.state.userApplication;
        ua.ananTrainingStartDate = date.toDate();

        this.setState({ ua });
    }

    handleTrainingEnd(date)
    {
        const ua = this.state.userApplication;
        ua.ananTrainingEndDate = date.toDate();

        this.setState({ ua });
    }

    handleAppointmentDate(date)
    {
        const ua = this.state.userApplication;
        ua.appointmentDate = date.toDate();

        this.setState({ ua });
    }

    handleUserInputChange(evt)
    {
        const user = this.state.user;
        user[evt.target.name] = evt.target.value;

        this.setState({ user });
    }

    handleUserApplicationInputChange(evt)
    {
        const userApplication = this.state.userApplication;
        userApplication[evt.target.name] = evt.target.value;

        this.setState({ userApplication });
    }

    saveUserApplication(userData, saveMessage, isEditing, isSubmitted=false) {
        userApplication.call(userData, (message) => {
            if (message) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(message.reason, 'Update Error');
                return;
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success(saveMessage, 'Success');

                //disable forms
                this.setState({
                    isEditing: isEditing,
                    isSubmitted: isSubmitted
                });
            }
        });
    }

    saveUserPayment(pay) {
        const { user } = this.state;
        const { paymentReference, paymentDate, transactionAmount } = pay;
        const { lastName, firstName, middleName, phone } = user.bio;

        const product = {
            ItemName: 'BRANCH REGISTRATION FEE',
            ItemCode: 'GL10142',
            ItemAmount: transactionAmount
        };

        //make call to api to save payment

        const data = {
            PaymentReference: paymentReference,
            PaymentDate: paymentDate,
            CustReference: user._id,
            CustomerName: `${lastName}, ${firstName} ${middleName}`,
            CustomerEmail: user.emails[0].address,
            CustomerPhoneNo: phone,
            amount: transactionAmount,
            product,
            products: [product],
            TotalAmount: transactionAmount,
        };

        userPayment.call(data, (err, resp) => {
            if (err) {
                console.log(err)
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(err, 'Update Error');
                return;
            } else {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Payment successful', 'Success');

                //disable forms
                // this.setState({
                //     isEditing: isEditing,
                //     isSubmitted: isSubmitted
                // });
            }
        });
    }

    displayComment() {
        let { comment, dateCommented } = this.props.approvalComment.membership;
        //if membership, display the comments
        if (this.props.approvalComment && this.props.approvalComment.membership) {
            return (
                <div className="panel-group">
                    <h5>Comments</h5>
                    {
                        <Collapsible
                            trigger={`+ Membership --- Click to Expand`}
                            classParentString="panel panel-default"
                        >
                            <div>{comment}</div>
                            <div>{dateCommented.toDateString()}</div>
                        </Collapsible>
                    }
                </div>
            );
        }
    }

    displayInstructions() {
        return (
            <Card>
                <CardTitle>
                    <span
                        className="title"
                        style={{paddingLeft: '15px'}}>
                        Instructions
                    </span>
                </CardTitle>
                <CardBlock>
                    <Collapsible
                        trigger={`+ Instructions --- Click to Expand`}
                        classParentString="panel panel-default"
                    >
                        <ol>
                            <li>Click on Edit to fill in your details</li><br/>
                            <li>Completely fill out your personal details and click “Save” to preserve it</li><br/>
                            <li>Click on the Documents tab, and upload corresponding documents</li><br/>
                            <li>
                                Make sure your documents have the following name format
                                <ol>
                                    <li>Passport / Photograph (in .jpg only): Passport.jpg</li><br/>
                                    <li>Declaration (Signed by Commissioner of Oaths): Declaration_Signed_by_Commissioner_of_Oaths.pdf</li><br/>
                                    <li>Attestation Letter from Employer: Attestation_Letter_from_Employer.pdf</li><br/>
                                    <li>Letter of Promotion: Letter_of_Promotion.pdf</li><br/>
                                    <li>Letter of Appointment: Letter_of_Appointment.pdf</li><br/>
                                    <li>BSc/HND Certificate or Statement of Result: BSc_HND_Certificate_or_Statement_of_Result.pdf</li><br/>
                                    <li>NYSC Discharge Certificate / Exemption: NYSC_Discharge_Certificate_or_Exemption.pdf</li><br/>
                                    <li>NCA Statement of Result: NCA_Statement_of_Result.pdf</li><br/>
                                    <li>NCA Conversion of Result (if applicable): NCA_Conversion_of_Result.pdf</li><br/>
                                    <li>A.I.T Workshop: AIT_Workshop.pdf</li><br/>
                                    <li>Evidence of Change of Name (if Applicable): Evidence_of_Change_of_Name.pdf</li><br/>
                                    <li>Certificate of Sponsor with Branch Endorsement: Certificate_of_Sponsor_with_Branch_Endorsement.pdf</li><br/>
                                    <li>Scanned Signature: Scanned_Signature.pdf</li><br/>
                                </ol>
                            </li>
                            <li>After uploading your documents, click on “Submit” to submit your application. Once submitted, you can’t change your application again</li><br/>
                            <li>To delete you application, click on Delete. You can only delete your application if it is not been submitted.</li>
                        </ol>
                    </Collapsible>
                </CardBlock>
            </Card>
        );
    }



    renderInterface() {
        if(this.state.user && this.state.userApplication && this.state.userPayment) {
            const { user, userApplication, userPayment, isEditing, isSubmitted, tabIndex } = this.state;
            let pay = null;
            //if payment has not been made, generate a reference number
            if(userPayment && !userPayment.isPaymentMade) {
                pay = this.generatePaymentReference();
                console.log(pay)
            }

            return (
                <div>
                    <Row>
                        <DashboardNavbar
                            user={user}
                            isEditing={isEditing}
                            isSubmitted={isSubmitted}
                            onEditChange={this.onEditChange}
                            onDelete={this.onDelete}
                            changePassword={this.changePassword}
                            handleApplicationSave={this.handleApplicationSave}
                            handleApplicationSubmit={this.handleApplicationSubmit}
                        />
                    </Row>

                    <Row className="content">
                        <div>
                            <Col lg={4} md={5}>
                                <ProfileUser
                                    user={user}
                                    profilePicture={userApplication.files && userApplication.files[0].isUploaded}
                                >
                                    {
                                        this.props.approvalComment ?
                                            this.displayComment() : <div></div>
                                    }
                                </ProfileUser>

                                <ApplicationStatus
                                    applicationStarted={ this.formatDate(userApplication.applicationStarted) }
                                    applicationSubmitted={ this.formatDate(userApplication.applicationSubmitted) }
                                    applicationApproved={ this.formatDate(userApplication.applicationApproved) }
                                    isApproved={userApplication.isApproved}
                                    ananNumber={userApplication.ananNumber}
                                />

                                {
                                    userApplication.isApproved && userApplication.ananNumber ?
                                        <CongratulationsDisplay
                                            name={`${user.bio.lastName.toUpperCase()}, ${user.bio.firstName} ${user.bio.middleName}`}
                                            ananNumber={userApplication.ananNumber}
                                        />
                                        : <span></span>
                                }

                                <ChangePassword />
                            </Col>
                            <Col lg={8} md={7}>
                                <Row>
                                    {this.displayInstructions()}
                                </Row>
                                {
                                    userPayment && !userPayment.isPaymentMade
                                    ? (
                                        <Row>
                                            <PaymentCard
                                                user={user}
                                                paymentReference={pay.paymentReference}
                                                transactionAmount={pay.transactionAmount}
                                                paymentDate={pay.paymentDate}
                                                onSubmit={this.saveUserPayment.bind(null, pay)}
                                            />
                                        </Row>
                                    ) : (
                                        <Row>
                                            <Card>
                                                <CardTitle>
                                                    <span
                                                        className="title"
                                                        style={{paddingLeft: '15px'}}>
                                                        Data Profile
                                                    </span>
                                                </CardTitle>
                                                <CardBlock style={{padding: 10}}>
                                                    <FormTabs
                                                        user={user}
                                                        userApplication={userApplication}
                                                        isEditing={isEditing}
                                                        tabIndex={tabIndex}
                                                        onChangeTab={this.onChangeTab}
                                                        handleApplicationSave={this.handleApplicationSave}
                                                        handleUserInputChange={this.handleUserInputChange}
                                                        handleUserApplicationInputChange={this.handleUserApplicationInputChange}
                                                        handleProfessionalExam={this.handleProfessionalExam}
                                                        handleTrainingEnd={this.handleTrainingEnd}
                                                        handleTrainingStart={this.handleTrainingStart}
                                                        handleAppointmentDate={this.handleAppointmentDate}
                                                    />
                                                </CardBlock>
                                            </Card>
                                        </Row>
                                    )
                                }
                            </Col>
                        </div>
                    </Row>
                </div>
            )
        }
        return <div></div>
    }

    handleApplicationSubmit(e) {
        e.preventDefault();
        let userData = this.state.userApplication;

        //form not yet submitted
        userData.isSubmitted = true;
        this.setState({ userApplication: userData });

        this.saveUserApplication(userData, 'Application submitted', false, true);
    }

    handleApplicationSave(e){
        e.preventDefault();
        let userData = this.state.userApplication,
            user = this.state.user;

        let email = user.emails[0].address;

        //form not yet submitted
        userData.isSubmitted = this.state.isSubmitted;

        //validate the form
        this.saveUserApplication(userData, 'Application Saved', false);
    }

    // isEmailVerified() {
    //     if (this.props.user.emails[0].verified) {
    //         this.renderInterface()
    //     }else{
    //         return (
    //             <div>Kindly verify your email</div>
    //         )
    //     }
    // }

    render() {
        return (
            <div>
                <div className="wrapper">
                    <Sidebar />

                    <Container fluid={true} className="main-panel">
                        {
                            this.props.loading
                            ?
                                <div className="loading">Loading...</div>
                            :
                                this.renderInterface()
                        }

                        <Footer />
                    </Container>
                </div>
            </div>
        );
    }
}

ProfileForm.propTypes = {
    loading: PropTypes.bool,
    user: PropTypes.object,
    userApplication: PropTypes.object,
    approvalComment: PropTypes.object,
};

export default ProfileForm;


// renderInterface() {
//     if(this.state.user && this.state.userApplication) {
//         const { user, userApplication, isEditing, isSubmitted, tabIndex } = this.state;
//         return (
//             <div>
//                 <Row>
//                     <DashboardNavbar
//                         user={user}
//                         isEditing={isEditing}
//                         isSubmitted={isSubmitted}
//                         onEditChange={this.onEditChange}
//                         onDelete={this.onDelete}
//                         changePassword={this.changePassword}
//                         handleApplicationSave={this.handleApplicationSave}
//                         handleApplicationSubmit={this.handleApplicationSubmit}
//                     />
//                 </Row>
//
//                 <Row className="content">
//                     {
//                         user.emails[0].verified ?
//                             <div>
//                                 <Col lg={4} md={5}>
//                                     <ProfileUser
//                                         user={user}
//                                         profilePicture={userApplication.files && userApplication.files[0].isUploaded}
//                                     >
//                                         {
//                                             this.props.approvalComment ?
//                                                 this.displayComment() : <div></div>
//                                         }
//                                     </ProfileUser>
//
//                                     <ApplicationStatus
//                                         applicationStarted={ this.formatDate(userApplication.applicationStarted) }
//                                         applicationSubmitted={ this.formatDate(userApplication.applicationSubmitted) }
//                                         applicationApproved={ this.formatDate(userApplication.applicationApproved) }
//                                         isApproved={userApplication.isApproved}
//                                         ananNumber={userApplication.ananNumber}
//                                     />
//
//                                     {
//                                         userApplication.isApproved && userApplication.ananNumber ?
//                                             <CongratulationsDisplay
//                                                 name={`${user.bio.lastName.toUpperCase()}, ${user.bio.firstName} ${user.bio.middleName}`}
//                                                 ananNumber={userApplication.ananNumber}
//                                             />
//                                             : <span></span>
//                                     }
//
//                                     <ChangePasswordDisplay
//                                         oldPassword={this.state.password.oldPassword}
//                                         newPassword={this.state.password.newPassword}
//                                         confirmPassword={this.state.password.confirmPassword}
//                                         handleInputChange={this.handlePasswordChange}
//                                         onClick={this.changePassword}
//                                     />
//                                 </Col>
//                                 <Col lg={8} md={7}>
//                                     <Row>
//                                         {this.displayInstructions()}
//                                     </Row>
//                                     <Row>
//                                         <Card>
//                                             <CardTitle>
//                                         <span
//                                             className="title"
//                                             style={{paddingLeft: '15px'}}>
//                                             Data Profile
//                                         </span>
//                                             </CardTitle>
//                                             <CardBlock>
//                                                 <FormTabs
//                                                     user={user}
//                                                     userApplication={userApplication}
//                                                     isEditing={isEditing}
//                                                     tabIndex={tabIndex}
//                                                     onChangeTab={this.onChangeTab}
//                                                     handleApplicationSave={this.handleApplicationSave}
//                                                     handleUserInputChange={this.handleUserInputChange}
//                                                     handleUserApplicationInputChange={this.handleUserApplicationInputChange}
//                                                     handleProfessionalExam={this.handleProfessionalExam}
//                                                     handleTrainingEnd={this.handleTrainingEnd}
//                                                     handleTrainingStart={this.handleTrainingStart}
//                                                     handleAppointmentDate={this.handleAppointmentDate}
//                                                 />
//                                             </CardBlock>
//                                         </Card>
//                                     </Row>
//                                 </Col>
//                             </div>
//                             :
//                             <div>
//                                 Kindly verify your email
//                             </div>
//                     }
//                 </Row>
//             </div>
//         )
//     }
//     return <div></div>
// }