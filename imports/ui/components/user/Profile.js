import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { UserApplication, ApprovalComments, UserPayment } from '../../../api/users/collections';

import ProfileForm from './ProfileForm';

export default Profile = createContainer(({ params }) => {
    const usersHandle = Meteor.subscribe('users.one', Meteor.userId());
    const userApplicationHandle = Meteor.subscribe('userApplication.one', Meteor.userId());
    const userPaymentHandle = Meteor.subscribe('userPayment.one', Meteor.userId());
    const userApprovalCommentsHandle = Meteor.subscribe('approval.comments', Meteor.userId());
    const loading = !usersHandle.ready() && !userApplicationHandle.ready() && !userApprovalCommentsHandle.ready() && !userPaymentHandle.ready();
    const user = Meteor.users.findOne({
        _id: Meteor.userId()
    }, {
        fields: {
            "roles": 0,
            "createdAt": 0
        }
    });

    const userApplication = UserApplication.findOne({ userId: Meteor.userId() });
    const approvalComment = ApprovalComments.findOne({ applicantId: Meteor.userId() },{
        fields: {
            "audit": 0,
            "exam": 0,
            "registry": 0
        }
    });
    const userPayment = UserPayment.findOne({ custReference: Meteor.userId() });

    return {
        loading,
        user,
        userApplication,
        approvalComment,
        userPayment
    };
}, ProfileForm);