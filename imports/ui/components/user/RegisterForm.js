/**
 * Created by ikedi on 07/12/2016.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col,
    Card, CardText, CardTitle, Button } from 'reactstrap';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

import { LoginDetails, Declaration, BioData, StyledDivider, AnanDisplay } from './RegisterFormHelpers';

class RegisterForm extends Component {
    constructor(props) {
        super(props);

        this.renderNewApplicant = this.renderNewApplicant.bind(this);
        this.renderArchivedApplicant = this.renderArchivedApplicant.bind(this);
    }

    renderNewApplicant() {
        return (
            <div className="full-screen register background">
                <div className="wrapper">
                    <div className="filter-black"></div>
                    <Row>
                        <Col xs={12} sm={7} md={{ size: 5, offset: 1 }}>
                            <Declaration
                                acceptDeclaration={this.props.acceptDeclaration}
                                handleInputChange={this.props.handleInputChange}
                            />
                        </Col>
                        <Col xs={12} sm={5} md={6}>
                            <Card className="demo-card">
                                <CardTitle className="title text-center">Register</CardTitle>
                                <AnanDisplay />

                                {
                                    this.props.acceptDeclaration === 'I agree'
                                        ?
                                        <CardText tag="div">
                                            <StyledDivider/>
                                            <Form>
                                                <Row>
                                                    {this.props.isRegistering ? <div className="loader">Loading...</div> : '' }
                                                </Row>

                                                <LoginDetails
                                                    username={this.props.username}
                                                    email={this.props.email}
                                                    password={this.props.password}
                                                    confirmPassword={this.props.confirmPassword}
                                                    handleInputChange={this.props.handleInputChange}

                                                    errors={this.props.errors}
                                                />

                                                <StyledDivider/>

                                                <BioData
                                                    firstName={this.props.firstName}
                                                    lastName={this.props.lastName}
                                                    middleName={this.props.middleName}
                                                    phone={this.props.phone}
                                                    secondPhone={this.props.secondPhone}
                                                    ananBranch={this.props.ananBranch}
                                                    gender={this.props.gender}
                                                    maritalStatus={this.props.maritalStatus}
                                                    dob={this.props.dob}
                                                    privateAddress={this.props.privateAddress}
                                                    stateOfOrigin={this.props.stateOfOrigin}
                                                    nationality={this.props.nationality}
                                                    handleInputChange={this.props.handleInputChange}
                                                    handleDateChange={this.props.handleDateChange}
                                                    isEditing={true}

                                                    errors={this.props.errors}
                                                />

                                                <Button
                                                    className="btn btn-fill btn-block"
                                                    onClick={this.props.handleRegistration}
                                                    disabled={this.props.isRegistering}
                                                >
                                                    Register
                                                </Button>
                                            </Form>
                                            <div className="login">
                                                <p>Already have an account? <a href={FlowRouter.path('login')}>Log in</a>.</p>
                                            </div>
                                        </CardText>
                                        :
                                        <CardText tag="div">
                                            <h3 style={{
                                                fontWeight: '900',
                                                fontSize: 30,
                                                color: 'white',
                                                textAlign: 'center'
                                            }}>
                                                Accept Declaration to Register
                                            </h3>
                                        </CardText>
                                }
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }

    renderArchivedApplicant() {
        return (
            <div className="full-screen register background">
                <div className="wrapper">
                    <div className="filter-black"></div>
                        <Card className="demo-card">
                            <CardTitle className="title text-center">Register</CardTitle>
                            <AnanDisplay />

                            <CardText tag="div">
                                <StyledDivider/>

                                <Form className="register-form">
                                    <Row>
                                        {this.props.isRegistering ? <div className="loader">Loading...</div> : '' }
                                    </Row>

                                    <LoginDetails
                                        username={this.props.username}
                                        email={this.props.email}
                                        password={this.props.password}
                                        confirmPassword={this.props.confirmPassword}
                                        handleInputChange={this.props.handleInputChange}
                                    />

                                    <StyledDivider/>

                                    <BioData
                                        firstName={this.props.firstName}
                                        lastName={this.props.lastName}
                                        middleName={this.props.middleName}
                                        phone={this.props.phone}
                                        secondPhone={this.props.secondPhone}
                                        ananBranch={this.props.ananBranch}
                                        gender={this.props.gender}
                                        maritalStatus={this.props.maritalStatus}
                                        dob={this.props.dob}
                                        privateAddress={this.props.privateAddress}
                                        stateOfOrigin={this.props.stateOfOrigin}
                                        nationality={this.props.nationality}
                                        handleInputChange={this.props.handleInputChange}
                                        handleDateChange={this.props.handleDateChange}
                                        isEditing={true}
                                    />

                                    <Button
                                        className="btn btn-fill btn-block"
                                        onClick={this.props.handleRegistration}
                                        disabled={this.props.isRegistering}
                                    >
                                        Register
                                    </Button>
                                </Form>
                            </CardText>
                        </Card>
                </div>
            </div>
        );
    }

    render() {
        return (
            this.props.renderNew ?
                this.renderNewApplicant()
            :
                this.renderArchivedApplicant()
        );
    }
}

RegisterForm.propTypes = {
    renderNew: PropTypes.bool.isRequired
};

RegisterForm.defaultProps = {
    renderNew: true
};

export default RegisterForm;