/**
 * Created by ic on 7/17/17.
 */
import React, { Component } from 'react';
import { ChangePasswordDisplay } from '../user/ProfileFormHelpers';

import { Accounts } from 'meteor/accounts-base';

export default class ChangePassword extends Component {
    constructor(props) {
        super(props);

        this.state = {
            password: {
                oldPassword: "",
                newPassword: "",
                confirmPassword: ""
            }
        };

        this.changePassword = this.changePassword.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    changePassword() {
        let { oldPassword, newPassword, confirmPassword } = this.state.password;

        if (confirmPassword !== newPassword) {
            toastr.options = {"positionClass":'toast-top-left'};
            toastr.error("New passwords do not match", 'Password Error');

            let password = {
                oldPassword: "",
                newPassword: "",
                confirmPassword: "",
            };

            this.setState({
                password
            });

            return ;
        }

        Accounts.changePassword(oldPassword, newPassword, (err) => {
            if(err) {
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.error(err.reason, 'Password Error');
            }else{
                toastr.options = {"positionClass":'toast-top-left'};
                toastr.success('Password change successful', 'Success');

                let password = {
                    oldPassword: "",
                    newPassword: "",
                    confirmPassword: "",
                };

                this.setState({
                    password
                });
            }
        });
    }

    handlePasswordChange(evt) {
        const password = this.state.password;
        password[evt.target.name] = evt.target.value;

        this.setState({ password });
    }

    render() {
        const { password } = this.state;

        return (
            <ChangePasswordDisplay
                oldPassword={password.oldPassword}
                newPassword={password.newPassword}
                confirmPassword={password.confirmPassword}
                handleInputChange={this.handlePasswordChange}
                onClick={this.changePassword}
            />
        );
    }
}