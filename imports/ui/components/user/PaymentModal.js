import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import './bootstrap.min.css';
import './App.css';
class App extends Component {
    state = {
        title:"Form title",
        PaymentReference:""
    };
    componentDidMount(){
        this.setState({
            PaymentReference:Date.now(),
            PaymentDate:new Date().toISOString().split('T')[0],

        })
    }
    render() {
        let {title,PaymentReference,PaymentDate} = this.state;
        return (
            <div style={{margin:15}}>
                <div>
                    <div className="panel-heading">
                        <div className="panel-title text-center">
                            <h1 className="title">{title}</h1>
                            <hr/>
                        </div>
                    </div>
                    <div className="main-login main-center">
                        <form className="form-horizontal" method="post" action="processPayment.php">
                            <div className="form-group">
                                <label className="cols-sm-2 control-label">RRN</label>
                                <div className="cols-sm-10">
                                    <div className="input-group">
                                        <span className="input-group-addon"><i className="fa fa-lock fa" aria-hidden="true"/></span>
                                        <input type="text" className="form-control"  readOnly name="PaymentReference" placeholder="RRN" value={PaymentReference}/>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="cols-sm-2 control-label">Payment Date</label>
                                <div className="cols-sm-10">
                                    <div className="input-group">
                                        <span className="input-group-addon"><i className="fa fa-calendar" aria-hidden="true"/></span>
                                        <input type="text" className="form-control" readOnly name="PaymentDate" placeholder="Payment Date" value={PaymentDate} />
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label  className="cols-sm-2 control-label">Customer Name</label>
                                <div className="cols-sm-10">
                                    <div className="input-group">
                                        <span className="input-group-addon"><i className="fa fa-user fa" aria-hidden="true"/></span>
                                        <input type="text" name="CustomerName" className="form-control" placeholder="Customer Name" required="required" />
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="cols-sm-2 control-label">Customer Phone Number</label>
                                <div className="cols-sm-10">
                                    <div className="input-group">
                                        <span className="input-group-addon"><i className="fa fa-phone fa-lg" aria-hidden="true"/></span>
                                        <input type="text" name="CustomerPhoneNo" className="form-control" placeholder="Customer Phone Number" required="required" />
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="cols-sm-2 control-label">Customer Email</label>
                                <div className="cols-sm-10">
                                    <div className="input-group">
                                        <span className="input-group-addon"><i className="fa fa-envelope-o fa" aria-hidden="true"/></span>
                                        <input type="email" name="CustomerEmail" className="form-control" placeholder="Customer Email" required="required" />
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="cols-sm-2 control-label">Transaction Amount</label>
                                <div className="cols-sm-10">
                                    <div className="input-group">
                                        <span className="input-group-addon"><i className="fa fa-money fa-lg" aria-hidden="true"/></span>
                                        <input type="number" className="form-control" name="amount" placeholder="Transaction Amount" required="required" />
                                    </div>
                                </div>
                            </div>
                            <div className="form-group ">
                                <Button block type="submit">Submit</Button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default App