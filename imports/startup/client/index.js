import './routes';
import moment from 'moment';
import 'moment/locale/en-gb';
import 'react-toggle/style.css';
import '/node_modules/tether/dist/js/tether.min';
import '/node_modules/tether/dist/css/tether.min.css';

// import 'bootstrap' // js-only!
// import '/node_modules/bootstrap/dist/css/bootstrap.min.css';
// import '/node_modules/font-awesome/css/font-awesome.css';
// import '../../../public/css/paper-dashboard.css';
// import '../../../public/css/styles.css';
// import './css/paper-dashboard.css';
// import './css/styles.css';


moment.locale('en-gb');

//code to make FlowRouter to wait
//FlowRouter.wait();

// Tracker.autorun(() => {
//     if(Roles.subscription.ready() && !FlowRouter._initialized) {
//         FlowRouter.initialize();
//     }
// });