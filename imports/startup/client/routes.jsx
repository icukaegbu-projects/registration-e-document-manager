import React from 'react';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import { Roles } from 'meteor/alanning:roles';
import { mount } from 'react-mounter';

import AppLayout from '../../ui/layouts/AppLayout';
import DashboardLayout from '../../ui/layouts/DashboardLayout';
// import UserLayout from '../../ui/layouts/UserLayout';
import Register from '../../ui/components/user/Register';
import Profile from '../../ui/components/user/Profile';
import Login from '../../ui/components/user/Login';
import LandingPage from '../../ui/components/user/LandingPage';
//import Members from '../../ui/components/admin/Members';

import Admin from '../../ui/components/admin/Admin';
import ArchivedApplicantForm from '../../ui/components/admin/ArchivedApplicantForm';
import ViewArchivedApplicants from '../../ui/components/admin/ViewArchivedApplicants';
import Applicants from '../../ui/components/admin/ApplicantsForm';
import UploadCSV from '../../ui/components/admin/UploadCSV';
import AdminAuditTrail from '../../ui/components/admin/AdminAuditTrail';
import AdminManageUsersForm2 from '../../ui/components/admin/AdminManageUsersForm2';
import ViewMembers from '../../ui/components/admin/ViewMembers';
import StaffProfile from '../../ui/components/admin/StaffProfile';


// function checkLoggedIn(ctx, redirect){
//     if(!Meteor.userId()){
//         //console.log('not logged in, redirecting')
//         redirect('/')
//     }
// }
//
// function loggedIn(ctx, redirect){
//     if(Meteor.userId()){
//         //console.log('logged in, redirecting')
//         redirect('/user')
//         //FlowRouter.go('user')
//     }
// }
//
function checkAdminLoggedIn(ctx, redirect){
    if(!Roles.userIsInRole(Meteor.userId(), 'admin')){
        console.log(Roles.userIsInRole(Meteor.userId(), 'admin'))
        redirect('profile')
    }
}

let adminRoutes = FlowRouter.group({
    prefix: '/admin',
    name: 'admin'
    // triggersEnter: [checkAdminLoggedIn]
});

FlowRouter.route('/', {
    name: 'index',
    action(){
        mount(AppLayout, {
            content: <LandingPage />,
            renderWithMenu: true
        });
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action(){
        mount(AppLayout, {
            content: <Login />,
            renderWithMenu: true
        });
    }
});

FlowRouter.route('/register', {
    name: 'register',
    action(){
        mount(AppLayout, {
            content: <Register />,
            renderWithMenu: true
        });
    }
});

// FlowRouter.route('/members', {
//     name: 'members',
//     action(){
//         mount(UserLayout, {
//             content: <Members />
//         });
//     }
// });
//
FlowRouter.route('/profile', {
    name: 'profile',
    action(){
        mount(DashboardLayout, {
            content: <Profile />
        });
    }
});

//ADMIN ROUTES
FlowRouter.route('/archived', {
    name: 'archived-members',
    action() {
        mount(DashboardLayout, {
            content: <ArchivedApplicantForm />
        });
    },
    // triggersEnter: [checkAdminLoggedIn]
});

FlowRouter.route('/members', {
    name: 'members',
    action() {
        mount(DashboardLayout, {
            // content: <ViewArchivedApplicants />
            content: <ViewMembers />
        });
    },
    // triggersEnter: [checkAdminLoggedIn]
});

FlowRouter.route('/applicants', {
    name: 'applicants',
    action() {
        mount(DashboardLayout, {
            content: <Applicants />
        });
    }
});

FlowRouter.route('/staff', {
    name: 'staff',
    action() {
        mount(DashboardLayout, {
            content: <AdminManageUsersForm2 />
        });
    }
});

// FlowRouter.route('/staff', {
//     name: 'staff',
//     action() {
//         mount(DashboardLayout, {
//             content: <AdminManageUsersForm />
//         });
//     }
// });

FlowRouter.route('/upload-csv', {
    name: 'upload-csv',
    action() {
        mount(DashboardLayout, {
            content: <UploadCSV />
        });
    }
});

FlowRouter.route('/staff-profile', {
    name: 'staff-profile',
    action() {
        mount(DashboardLayout, {
            content: <StaffProfile />
        });
    }
});

FlowRouter.route('/audit-trail', {
    name: 'audit-trail',
    action() {
        mount(DashboardLayout, {
            content: <AdminAuditTrail />
        });
    }
});

// FlowRouter.route('/dashboard', {
//     name: 'admin-dashboard',
//     action() {
//         mount(DashboardLayout, {
//             content: <Admin />
//         });
//     }
// });

adminRoutes.route('/', {
    name: 'admin-dashboard',
    action() {
        mount(DashboardLayout, {
            content: <Admin />
        });
    }
});